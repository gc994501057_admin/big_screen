package com.service.screenWeb.service.impl;

import com.service.config.utils.IdUtil;
import com.service.config.utils.ModelMapperUtil;
import com.service.config.utils.R;
import com.service.screenWeb.dao.ReplyInfoDao;
import com.service.screenWeb.dto.LargeQaAndReplyDto;
import com.service.screenWeb.dto.ReplyInfoDto;
import com.service.screenWeb.entity.LargeQa;
import com.service.screenWeb.dao.LargeQaDao;
import com.service.screenWeb.entity.ReplyInfo;
import com.service.screenWeb.service.LargeQaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 意见、建议、问题反馈表(LargeQa)表服务实现类
 *
 * @author makejava
 * @since 2021-05-07 15:00:53
 */
@Service("largeQaService")
public class LargeQaServiceImpl implements LargeQaService {
    @Resource
    private LargeQaDao largeQaDao;
    @Resource
    private ReplyInfoDao replyInfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public LargeQa queryById(String id) {
        return this.largeQaDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<LargeQa> queryAllByLimit(int offset, int limit) {
        return this.largeQaDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param largeQa 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public LargeQa insert(LargeQa largeQa) {
        this.largeQaDao.insert(largeQa);
        return largeQa;
    }

    /**
     * 修改数据
     *
     * @param largeQa 实例对象
     * @return 实例对象
     */
    @Override
    public LargeQa update(LargeQa largeQa) {
        this.largeQaDao.update(largeQa);
        return this.queryById(largeQa.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.largeQaDao.deleteById(id) > 0;
    }

    @Override
    @Transactional
    public void addAndUpdateLargeQa(LargeQa largeQa) {
        if (largeQa.getId() !=null && !largeQa.getId().equals("")) {
            largeQa.setUpdateTime(new Date());
            largeQaDao.update(largeQa);
        }else { // 新增记录
            largeQa.setId(IdUtil.getStringId());
            largeQa.setCommitDate(new Date());
            largeQa.setStatus(false);
            largeQa.setType("0");
            largeQa.setUpdateTime(new Date());
            largeQa.setOrderNo(largeQa.getId());
            largeQaDao.insert(largeQa);
        }
    }

    /**
     * getHistory 不存在或者存在且为false：用户获取未处理的意见、建议和问题反馈
     * getHistory 存在且true：用户获取已处理的意见、建议和问题反馈
     * @param largeQa
     * @param getHistory
     * @return
     */
    @Override
    public List<LargeQaAndReplyDto> queryAll(LargeQa largeQa, Boolean getHistory) {
        if (getHistory != null) {
            largeQa.setStatus(getHistory);
            List<ReplyInfo> replyInfos =  replyInfoDao.queryAll(new ReplyInfo());
            largeQa.setStatus(getHistory);
            List<LargeQa> largeQaList = largeQaDao.queryAll(largeQa);
            /* 根据提交日期升序排序 */
            return getLargeQaAndReplyDtos(replyInfos, largeQaList);

        }
        List<ReplyInfo> replyInfos =  replyInfoDao.queryAll(new ReplyInfo());
        largeQa.setStatus(false);
        List<LargeQa> largeQaList = largeQaDao.queryAll(largeQa);
        return getLargeQaAndReplyDtos(replyInfos,largeQaList);
    }

    private List<LargeQaAndReplyDto> getLargeQaAndReplyDtos(List<ReplyInfo> replyInfos, List<LargeQa> largeQaList) {
        largeQaList = largeQaList.stream().sorted(Comparator.comparing(LargeQa::getCommitDate)).collect(Collectors.toList());
        List<LargeQaAndReplyDto> largeQaAndReplyDtoList = ModelMapperUtil.strictMapList(largeQaList, LargeQaAndReplyDto.class);
        largeQaAndReplyDtoList.stream().forEach(largeQaAndReplyDto -> {
            List<ReplyInfo> collect = replyInfos.stream().filter(replyInfo -> replyInfo.getQaId().equals(largeQaAndReplyDto.getId())).collect(Collectors.toList());
            List<ReplyInfoDto> replyInfoDtoList = ModelMapperUtil.strictMapList(collect, ReplyInfoDto.class);
            largeQaAndReplyDto.setReplyInfoList(replyInfoDtoList.stream().sorted(Comparator.comparing(ReplyInfoDto::getDate)).collect(Collectors.toList()));
        });
        return largeQaAndReplyDtoList;
    }
}
