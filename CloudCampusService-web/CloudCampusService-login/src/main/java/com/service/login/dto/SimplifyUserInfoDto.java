package com.service.login.dto;

import com.service.login.entity.UserExtend;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户表(UserInfo)实体类
 *
 * @author makejava
 * @since 2021-03-30 11:37:21
 */
@Data
public class SimplifyUserInfoDto implements Serializable {

    /**
     * 账号
     */
    private String apiUsername;
    /**
     * 密码
     */
    private String apiPassword;
    /**
     * api账号域名
     */
    private String apiAddress;


}
