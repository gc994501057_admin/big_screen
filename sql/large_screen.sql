/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50637
 Source Host           : localhost:3306
 Source Schema         : large_screen

 Target Server Type    : MySQL
 Target Server Version : 50637
 File Encoding         : 65001

 Date: 13/07/2021 15:09:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assembly
-- ----------------------------
DROP TABLE IF EXISTS `assembly`;
CREATE TABLE `assembly`  (
  `id` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `assembly_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件名称',
  `assembly_x` double(25, 0) NULL DEFAULT NULL COMMENT '组件描述',
  `assembly_y` double(25, 0) NULL DEFAULT NULL COMMENT '组件标题',
  `width` double NULL DEFAULT NULL COMMENT '宽',
  `height` double NULL DEFAULT NULL COMMENT '高',
  `number` int(11) NULL DEFAULT NULL COMMENT '序号',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `assembly_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组件' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for assembly_configure
-- ----------------------------
DROP TABLE IF EXISTS `assembly_configure`;
CREATE TABLE `assembly_configure`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `assembly_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件id',
  `schedule` int(50) NULL DEFAULT NULL COMMENT '组件自定义刷新时间- 单位分钟',
  `background_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色配置',
  `site_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '站点id--根据站点id进行站点拓扑',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `programme_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方案表id',
  `auth_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'url昵称',
  `api_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口url',
  `auth_username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证账号',
  `auth_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组件单独配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for assembly_info
-- ----------------------------
DROP TABLE IF EXISTS `assembly_info`;
CREATE TABLE `assembly_info`  (
  `id` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `assembly_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件名称',
  `number` int(11) NULL DEFAULT NULL COMMENT '序号',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'vue路由组件',
  `assembly_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件引用的图片地址',
  `type` int(11) NULL DEFAULT NULL COMMENT '组件类型 0--内置组件 1--第三方组件',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组件信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for customer_service_info
-- ----------------------------
DROP TABLE IF EXISTS `customer_service_info`;
CREATE TABLE `customer_service_info`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统运维人员信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for device_distribution_gis
-- ----------------------------
DROP TABLE IF EXISTS `device_distribution_gis`;
CREATE TABLE `device_distribution_gis`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `device_count` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `device_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备类型',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `site_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '站点id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '站点分布GIS图-设备表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for industry_info
-- ----------------------------
DROP TABLE IF EXISTS `industry_info`;
CREATE TABLE `industry_info`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `industry_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行业名称',
  `industry_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行业代码',
  `industry_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行业范围简要说明',
  `industry_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级行业id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'L2行业信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for large_qa
-- ----------------------------
DROP TABLE IF EXISTS `large_qa`;
CREATE TABLE `large_qa`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `question_feedback` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '意见、建议、问题反馈',
  `commit_date` datetime(0) NULL DEFAULT NULL COMMENT '初次提交时间',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态 0--false（还未处理） 1--true（已处理）',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '文本类型 0--意见 1--建议 2--问题反馈',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '内容变更时间',
  `order_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '意见、建议、问题反馈表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for layout
-- ----------------------------
DROP TABLE IF EXISTS `layout`;
CREATE TABLE `layout`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `assembly_x` double(25, 0) NULL DEFAULT NULL COMMENT '组件x',
  `assembly_y` double(25, 0) NULL DEFAULT NULL COMMENT '组件y',
  `width` double NULL DEFAULT NULL COMMENT '宽',
  `height` double NULL DEFAULT NULL COMMENT '高',
  `assembly_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件信息id',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `programme_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方案表id',
  `number` int(11) NULL DEFAULT NULL COMMENT '组件序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组件布局表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for model_assembly
-- ----------------------------
DROP TABLE IF EXISTS `model_assembly`;
CREATE TABLE `model_assembly`  (
  `model_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板id',
  `assembly_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for model_info
-- ----------------------------
DROP TABLE IF EXISTS `model_info`;
CREATE TABLE `model_info`  (
  `assembly_x` double(25, 0) NULL DEFAULT NULL COMMENT '组件x',
  `assembly_y` double(25, 0) NULL DEFAULT NULL COMMENT '组件y',
  `width` double NULL DEFAULT NULL COMMENT '宽',
  `height` double NULL DEFAULT NULL COMMENT '高',
  `model_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板id',
  `assembly_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件信息id',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '布局表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for programme_info
-- ----------------------------
DROP TABLE IF EXISTS `programme_info`;
CREATE TABLE `programme_info`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `programme_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方案名称',
  `is_banner` bit(1) NULL DEFAULT NULL COMMENT '是否条幅 0--false 1--true',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '大屏条幅标题',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `layout_info` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '布局信息（json数据）',
  `status` bit(1) NULL DEFAULT NULL COMMENT '是否当前 0--false 1--true',
  `file_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传文件id',
  `type` bit(1) NULL DEFAULT NULL COMMENT '方案类型 0--false（不是默认方案） 1--true（默认方案）',
  `programme_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方案预览图地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '方案信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for purchase_charge
-- ----------------------------
DROP TABLE IF EXISTS `purchase_charge`;
CREATE TABLE `purchase_charge`  (
  `id` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id ',
  `user_id` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户购买计费表（字段待补充）' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for reply_info
-- ----------------------------
DROP TABLE IF EXISTS `reply_info`;
CREATE TABLE `reply_info`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `qa_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '意见、建议和问题反馈表id',
  `customer_service_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客服id',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `date` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `reply_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复id',
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份 0--客户 1--客服',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '回复表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for screen_model
-- ----------------------------
DROP TABLE IF EXISTS `screen_model`;
CREATE TABLE `screen_model`  (
  `id` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `model_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板Id',
  `model_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板名称',
  `model_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板描述',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '模板（视图）' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for screen_user
-- ----------------------------
DROP TABLE IF EXISTS `screen_user`;
CREATE TABLE `screen_user`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '大屏用户id',
  `tenant_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户账号',
  `tenant_pwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户密码',
  `telephone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `model_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板id',
  `model_color` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板背景色',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `assembly_info` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件信息',
  `domain` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '域名（nass,nass1,nass2,nass3）',
  `status` bit(1) NULL DEFAULT NULL COMMENT '账号状态',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '大屏用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for service_info
-- ----------------------------
DROP TABLE IF EXISTS `service_info`;
CREATE TABLE `service_info`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `service_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务名字',
  `status` bit(1) NULL DEFAULT NULL COMMENT '0--已禁用 1--正常',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `service_name`(`service_name`) USING BTREE COMMENT '唯一索引  服务名'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_distribution_gis
-- ----------------------------
DROP TABLE IF EXISTS `site_distribution_gis`;
CREATE TABLE `site_distribution_gis`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `site_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '站点id',
  `site_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '站点名称',
  `latitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '维度',
  `longtitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经度',
  `screen_user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '大屏用户id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '站点GIS分布-站点表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for third_assembly
-- ----------------------------
DROP TABLE IF EXISTS `third_assembly`;
CREATE TABLE `third_assembly`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `assembly_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件名称',
  `assembly_x` double(25, 0) NULL DEFAULT NULL COMMENT '组件描述',
  `assembly_y` double(25, 0) NULL DEFAULT NULL COMMENT '组件标题',
  `width` double NULL DEFAULT NULL COMMENT '宽',
  `height` double NULL DEFAULT NULL COMMENT '高',
  `number` int(11) NULL DEFAULT NULL COMMENT '序号',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `assembly_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `auth_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'url昵称',
  `api_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口url',
  `auth_username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证账号',
  `auth_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证密码',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '第三方组件' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_extend
-- ----------------------------
DROP TABLE IF EXISTS `user_extend`;
CREATE TABLE `user_extend`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱 （必填）',
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司/企业/学校名称',
  `occupation` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '职业',
  `company_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司/企业/学校网址',
  `company_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司/企业/学校地址',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域（地址）',
  `send_allow` bit(1) NULL DEFAULT NULL COMMENT '是否接受营销信息',
  `parent_company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '母公司/集团名称',
  `parent_company_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '母公司/集团网址',
  `industry` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行业',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息扩展' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `telephone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `add_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `date` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `count` int(11) NULL DEFAULT 1 COMMENT '登录次数，新注册用户次数为1',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号状态0--在线 1--下线 2--封号 3--销号（不可恢复）',
  `off_time` datetime(0) NULL DEFAULT NULL COMMENT '下线时间',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  `online_time` int(11) NULL DEFAULT NULL COMMENT '在线时长',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_login_record
-- ----------------------------
DROP TABLE IF EXISTS `user_login_record`;
CREATE TABLE `user_login_record`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '记录id',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `telephone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `add_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `date` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `count` int(11) NULL DEFAULT 1 COMMENT '第几次登录',
  `off_time` datetime(0) NULL DEFAULT NULL COMMENT '下线时间',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  `online_time` int(10) NULL DEFAULT NULL COMMENT '在线时长',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户登录记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_service_info
-- ----------------------------
DROP TABLE IF EXISTS `user_service_info`;
CREATE TABLE `user_service_info`  (
  `service_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '云服务id',
  `user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `assembly_info` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '组件信息',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '大屏标题'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与服务关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for v_file_physics_info
-- ----------------------------
DROP TABLE IF EXISTS `v_file_physics_info`;
CREATE TABLE `v_file_physics_info`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `filename` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名称（重命名后）',
  `filerealname` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件真实名称',
  `fileseverip` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件存储服务器IP',
  `fileseverdomain` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件存储服务器域名',
  `filedic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物理文件存储目录',
  `filedicdomain` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物理文件存储目录对应网络访问路径',
  `filesize` bigint(20) NULL DEFAULT NULL COMMENT '文件大小(b)',
  `filetype` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件类型(文件后缀)',
  `filemd5` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件md5码',
  `filefromtype` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件来源方式',
  `filebz` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件备注',
  `fileaddr` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件创建人',
  `fileaddrrealname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件创建人昵称',
  `status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '状态 0 停用 1 启用',
  `delflag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标记 0 未删除 1 已删除',
  `addr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `addtime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `updtime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '物理文件消息表' ROW_FORMAT = Compact;

-- ----------------------------
-- View structure for programme_info_view
-- ----------------------------
DROP VIEW IF EXISTS `programme_info_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `programme_info_view` AS select `p`.`id` AS `id`,`p`.`user_id` AS `user_id`,`p`.`programme_name` AS `programme_name`,`p`.`is_banner` AS `is_banner`,`p`.`name` AS `name`,`p`.`file_id` AS `file_id`,`p`.`layout_info` AS `layout_info`,`p`.`status` AS `status`,`p`.`programme_img` AS `programme_img`,`l`.`id` AS `layout_id`,`l`.`assembly_x` AS `assembly_x`,`l`.`assembly_y` AS `assembly_y`,`l`.`height` AS `height`,`l`.`width` AS `width`,`a`.`id` AS `assembly_id`,`a`.`assembly_img` AS `assembly_img`,`a`.`assembly_name` AS `assembly_name`,`a`.`component` AS `component`,`a`.`number` AS `number`,`a`.`type` AS `type`,`c`.`id` AS `assembly_configure_id`,`c`.`site_id` AS `site_id`,`c`.`auth_url` AS `auth_url`,`c`.`api_url` AS `api_url`,`c`.`auth_password` AS `auth_password`,`c`.`auth_username` AS `auth_username`,`c`.`background_color` AS `background_color`,`c`.`schedule` AS `schedule` from (((`programme_info` `p` left join `layout` `l` on((`p`.`id` = `l`.`programme_id`))) left join `assembly_info` `a` on((`l`.`assembly_id` = `a`.`id`))) left join `assembly_configure` `c` on(((`c`.`assembly_id` = `a`.`id`) and (`c`.`programme_id` = `p`.`id`))));

-- ----------------------------
-- View structure for user_service_info_view
-- ----------------------------
DROP VIEW IF EXISTS `user_service_info_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `user_service_info_view` AS select `u`.`id` AS `id`,`u`.`username` AS `username`,`u`.`password` AS `password`,`us`.`assembly_info` AS `assembly_info`,`s`.`id` AS `service_id`,`s`.`service_name` AS `service_name` from ((`user_info` `u` left join `user_service_info` `us` on((`u`.`id` = `us`.`user_id`))) left join `service_info` `s` on((`us`.`service_id` = `s`.`id`))) where ((`us`.`user_id` is not null) and (`us`.`service_id` is not null));

SET FOREIGN_KEY_CHECKS = 1;
