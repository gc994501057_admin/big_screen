/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50637
 Source Host           : localhost:3306
 Source Schema         : large_screen

 Target Server Type    : MySQL
 Target Server Version : 50637
 File Encoding         : 65001

 Date: 19/08/2021 16:34:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assembly_configure
-- ----------------------------
DROP TABLE IF EXISTS `assembly_configure`;
CREATE TABLE `assembly_configure`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `assembly_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件id',
  `schedule` int(50) NULL DEFAULT NULL COMMENT '组件自定义刷新时间- 单位分钟',
  `background_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色配置',
  `site_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '站点id--根据站点id进行站点拓扑',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `programme_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方案表id',
  `auth_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'url昵称',
  `api_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口url',
  `auth_username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证账号',
  `auth_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组件单独配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of assembly_configure
-- ----------------------------
INSERT INTO `assembly_configure` VALUES ('1', '1', 10, 'white', NULL, NULL, '1', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('10', '2', 10, 'white', NULL, NULL, '4', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('100', '9', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('101', '10', 10, 'white', NULL, NULL, '14', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('102', '11', 10, 'white', NULL, NULL, '14', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('103', '12', 10, 'white', NULL, NULL, '14', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('104', '13', 10, 'white', NULL, NULL, '14', '欢迎使用', 'https://www.vmall.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('105', '14', 10, 'white', NULL, NULL, '14', '欢迎使用', 'https://cloudcampusapps.top/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('106', '1', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('107', '2', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('108', '3', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('109', '4', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('11', '2', 10, 'white', NULL, NULL, '5', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('110', '5', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('111', '6', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('112', '7', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('113', '8', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('114', '9', 10, 'white', NULL, NULL, '15', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('115', '10', 10, 'white', NULL, NULL, '15', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('116', '11', 10, 'white', NULL, NULL, '15', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('117', '12', 10, 'white', NULL, NULL, '15', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('118', '13', 10, 'white', NULL, NULL, '15', '欢迎使用', 'https://www.vmall.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('119', '14', 10, 'white', NULL, NULL, '15', '欢迎使用', 'https://cloudcampusapps.top/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('12', '3', 10, 'white', NULL, NULL, '5', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('120', '15', 10, 'white', NULL, NULL, '15', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('121', '1', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('122', '2', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('123', '3', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('124', '4', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('125', '5', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('126', '6', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('127', '7', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('128', '8', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('129', '9', 10, 'white', NULL, NULL, '16', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('13', '4', 10, 'white', NULL, NULL, '5', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('130', '10', 10, 'white', NULL, NULL, '16', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('131', '11', 10, 'white', NULL, NULL, '16', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('132', '12', 10, 'white', NULL, NULL, '16', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('133', '13', 10, 'white', NULL, NULL, '16', '欢迎使用', 'https://www.vmall.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('134', '14', 10, 'white', NULL, NULL, '16', '欢迎使用', 'https://cloudcampusapps.top/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('135', '15', 10, 'white', NULL, NULL, '16', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('136', '16', 10, 'white', NULL, NULL, '16', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('137', '1', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('138', '2', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('139', '3', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('14', '5', 10, 'white', NULL, NULL, '5', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('140', '4', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('141', '5', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('142', '6', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('143', '7', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('144', '8', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('145', '9', 10, 'white', NULL, NULL, '17', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('146', '10', 10, 'white', NULL, NULL, '17', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('147', '11', 10, 'white', NULL, NULL, '17', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('148', '12', 10, 'white', NULL, NULL, '17', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('149', '13', 10, 'white', NULL, NULL, '17', '欢迎使用', 'https://www.vmall.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('15', '1', 10, 'white', NULL, NULL, '5', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('150', '14', 10, 'white', NULL, NULL, '17', '欢迎使用', 'https://cloudcampusapps.top/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('151', '15', 10, 'white', NULL, NULL, '17', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('152', '16', 10, 'white', NULL, NULL, '17', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('153', '17', 10, 'white', NULL, NULL, '17', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('154', '2', 10, 'white', NULL, NULL, '18', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('155', '3', 10, 'white', NULL, NULL, '18', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('156', '4', 10, 'white', NULL, NULL, '18', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('157', '5', 10, 'white', NULL, NULL, '18', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('158', '6', 10, 'white', NULL, NULL, '18', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('159', '7', 10, 'white', NULL, NULL, '18', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('16', '1', 10, 'white', NULL, NULL, '6', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('160', '8', 10, 'white', NULL, NULL, '18', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('161', '9', 10, 'white', NULL, NULL, '18', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('162', '10', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('163', '11', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('164', '12', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('165', '13', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://www.vmall.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('166', '14', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://cloudcampusapps.top/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('167', '15', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('168', '16', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('169', '17', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('17', '2', 10, 'white', NULL, NULL, '6', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('170', '18', 10, 'white', NULL, NULL, '18', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('171', '1', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('172', '3', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('173', '4', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('174', '5', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('175', '6', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('176', '7', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('178', '8', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('179', '2', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('18', '3', 10, 'white', NULL, NULL, '6', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('180', '9', 10, 'white', NULL, NULL, '19', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('181', '10', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('182', '11', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('183', '12', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('184', '13', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://www.vmall.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('185', '14', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://cloudcampusapps.top/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('186', '15', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('187', '16', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('188', '17', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('189', '18', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('19', '4', 10, 'white', NULL, NULL, '6', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('190', '19', 10, 'white', NULL, NULL, '19', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('191', '2', 10, 'white', NULL, NULL, '20', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('192', '3', 10, 'white', NULL, NULL, '20', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('193', '4', 10, 'white', NULL, NULL, '20', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('194', '5', 10, 'white', NULL, NULL, '20', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('195', '6', 10, 'white', NULL, NULL, '20', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('196', '7', 10, 'white', NULL, NULL, '20', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('197', '1', 10, 'white', NULL, NULL, '20', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('198', '9', 10, 'white', NULL, NULL, '20', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('199', '10', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('2', '1', 10, 'white', NULL, NULL, '2', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('20', '5', 10, 'white', NULL, NULL, '6', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('200', '11', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('201', '12', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('202', '13', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://www.vmall.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('203', '14', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://cloudcampusapps.top/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('204', '15', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('205', '16', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('206', '17', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('207', '18', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('208', '19', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('209', '20', 10, 'white', NULL, NULL, '20', '欢迎使用', 'https://www.baidu.com/?tn=88093251_95_hao_pg', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('21', '6', 10, 'white', NULL, NULL, '6', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('22', '1', 10, 'white', NULL, NULL, '7', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('221', '1', 10, 'white', NULL, NULL, '18', NULL, '', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('222', '8', 10, 'white', NULL, NULL, '20', '', '', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('23', '2', 10, 'white', NULL, NULL, '7', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('24', '3', 10, 'white', NULL, NULL, '7', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('25', '4', 10, 'white', NULL, NULL, '7', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('26', '5', 10, 'white', NULL, NULL, '7', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('27', '6', 10, 'white', NULL, NULL, '7', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('28', '7', 10, 'white', NULL, NULL, '7', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('29', '1', 10, 'white', NULL, NULL, '8', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('3', '2', 10, 'white', NULL, NULL, '2', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('30', '2', 10, 'white', NULL, NULL, '8', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('31', '3', 10, 'white', NULL, NULL, '8', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('32', '4', 10, 'white', NULL, NULL, '8', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('33', '5', 10, 'white', NULL, NULL, '8', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('34', '6', 10, 'white', NULL, NULL, '8', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('35', '7', 10, 'white', NULL, NULL, '8', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('36', '8', 10, 'white', NULL, NULL, '8', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('37', '1', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('38', '2', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('39', '3', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('4', '1', 10, 'white', NULL, NULL, '3', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('40', '4', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('41', '5', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('42', '6', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('43', '7', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('44', '8', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('45', '9', 10, 'white', NULL, NULL, '9', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('46', '1', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('47', '2', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('48', '3', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('49', '4', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('5', '2', 10, 'white', NULL, NULL, '3', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('50', '5', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('51', '6', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('52', '7', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('53', '8', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('54', '9', 10, 'white', NULL, NULL, '10', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('55', '10', 10, 'white', NULL, NULL, '10', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('56', '1', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('57', '2', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('58', '3', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('59', '4', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('6', '3', 10, 'white', NULL, NULL, '3', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('60', '5', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('61', '6', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('62', '7', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('63', '8', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('64', '9', 10, 'white', NULL, NULL, '11', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('65', '10', 10, 'white', NULL, NULL, '11', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('66', '11', 10, 'white', NULL, NULL, '11', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('67', '1', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('68', '2', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('69', '3', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('7', '1', 10, 'white', NULL, NULL, '4', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('70', '4', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('71', '5', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('72', '6', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('73', '7', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('74', '8', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('75', '9', 10, 'white', NULL, NULL, '12', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('76', '10', 10, 'white', NULL, NULL, '12', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('77', '11', 10, 'white', NULL, NULL, '12', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('78', '12', 10, 'white', NULL, NULL, '12', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('79', '1', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('8', '3', 10, 'white', NULL, NULL, '4', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('80', '2', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('81', '3', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('82', '4', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('83', '5', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('84', '6', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('85', '7', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('86', '8', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('87', '9', 10, 'white', NULL, NULL, '13', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('88', '10', 10, 'white', NULL, NULL, '13', '欢迎使用', 'https://map.baidu.com/@1269781.952205323,2574148.570000000,15.55z', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('89', '11', 10, 'white', NULL, NULL, '13', '欢迎使用', 'https://www.huaweicloud.com/product/cmn.html', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('9', '4', 10, 'white', NULL, NULL, '4', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('90', '12', 10, 'white', NULL, NULL, '13', '欢迎使用', 'https://naas.huaweicloud.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('91', '13', 10, 'white', NULL, NULL, '13', '欢迎使用', 'https://www.vmall.com/', NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('92', '1', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('93', '2', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('94', '3', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('95', '4', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('96', '5', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('97', '6', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('98', '7', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);
INSERT INTO `assembly_configure` VALUES ('99', '8', 10, 'white', NULL, NULL, '14', NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
