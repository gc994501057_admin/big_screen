package com.service.screenWeb.dao;

import com.service.screenWeb.entity.AssemblyConfigure;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 组件单独配置表(AssemblyConfigure)表数据库访问层
 *
 * @author makejava
 * @since 2021-05-18 19:56:29
 */
public interface AssemblyConfigureDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssemblyConfigure queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<AssemblyConfigure> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param assemblyConfigure 实例对象
     * @return 对象列表
     */
    List<AssemblyConfigure> queryAll(AssemblyConfigure assemblyConfigure);

    /**
     * 新增数据
     *
     * @param assemblyConfigure 实例对象
     * @return 影响行数
     */
    int insert(AssemblyConfigure assemblyConfigure);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<AssemblyConfigure> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<AssemblyConfigure> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<AssemblyConfigure> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<AssemblyConfigure> entities);

    /**
     * 修改数据
     *
     * @param assemblyConfigure 实例对象
     * @return 影响行数
     */
    int update(AssemblyConfigure assemblyConfigure);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    List<AssemblyConfigure> queryDefault();

    void deleteByProgrammeId(String id);
}

