package com.service.login.dao;



import com.service.login.dto.ScreenUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 大屏用户表(ScreenUser)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-29 15:42:46
 */
public interface ScreenUserDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ScreenUser queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ScreenUser> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param screenUser 实例对象
     * @return 对象列表
     */
    List<ScreenUser> queryAll(ScreenUser screenUser);
    List<ScreenUser> queryByUserId(String userId);

    /**
     * 新增数据
     *
     * @param screenUser 实例对象
     * @return 影响行数
     */
    int insert(ScreenUser screenUser);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<ScreenUser> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<ScreenUser> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<ScreenUser> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<ScreenUser> entities);

    /**
     * 修改数据
     *
     * @param screenUser 实例对象
     * @return 影响行数
     */
    int update(ScreenUser screenUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);
}

