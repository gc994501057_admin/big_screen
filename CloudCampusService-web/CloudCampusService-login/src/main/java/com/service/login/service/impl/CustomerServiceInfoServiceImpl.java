package com.service.login.service.impl;

import com.service.login.annotation.DataSource;
import com.service.login.entity.CustomerServiceInfo;
import com.service.login.dao.CustomerServiceInfoDao;
import com.service.login.enums.DataSourceType;
import com.service.login.service.CustomerServiceInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统运维人员信息表(CustomerServiceInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-05-08 17:36:16
 */
@Service("customerServiceInfoService")
public class CustomerServiceInfoServiceImpl implements CustomerServiceInfoService {
    @Resource
    private CustomerServiceInfoDao customerServiceInfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public CustomerServiceInfo queryById(String id) {
        return this.customerServiceInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<CustomerServiceInfo> queryAllByLimit(int offset, int limit) {
        return this.customerServiceInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param customerServiceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public CustomerServiceInfo insert(CustomerServiceInfo customerServiceInfo) {
        this.customerServiceInfoDao.insert(customerServiceInfo);
        return customerServiceInfo;
    }

    /**
     * 修改数据
     *
     * @param customerServiceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public CustomerServiceInfo update(CustomerServiceInfo customerServiceInfo) {
        this.customerServiceInfoDao.update(customerServiceInfo);
        return this.queryById(customerServiceInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.customerServiceInfoDao.deleteById(id) > 0;
    }

    @Override
    @DataSource(DataSourceType.MASTER)
    public CustomerServiceInfo queryAllByNameAndPassword(CustomerServiceInfo customerServiceInfo) {
        customerServiceInfo = customerServiceInfoDao.queryAllByNameAndPassword(customerServiceInfo.getUsername(),customerServiceInfo.getPassword());
        return customerServiceInfo;
    }
}
