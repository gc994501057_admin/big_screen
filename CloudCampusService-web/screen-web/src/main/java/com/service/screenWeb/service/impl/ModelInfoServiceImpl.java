package com.service.screenWeb.service.impl;

import com.service.screenWeb.entity.ModelInfo;
import com.service.screenWeb.dao.ModelInfoDao;
import com.service.screenWeb.service.ModelInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ModelInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-05-10 14:56:08
 */
@Service("modelInfoService")
public class ModelInfoServiceImpl implements ModelInfoService {
    @Resource
    private ModelInfoDao modelInfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ModelInfo queryById(String id) {
        return this.modelInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ModelInfo> queryAllByLimit(int offset, int limit) {
        return this.modelInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param modelInfo 实例对象
     * @return 实例对象
     */
    @Override
    public ModelInfo insert(ModelInfo modelInfo) {
        this.modelInfoDao.insert(modelInfo);
        return modelInfo;
    }

    /**
     * 修改数据
     *
     * @param modelInfo 实例对象
     * @return 实例对象
     */
    @Override
    public ModelInfo update(ModelInfo modelInfo) {
        this.modelInfoDao.update(modelInfo);
        return this.queryById(modelInfo.getAssemblyId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.modelInfoDao.deleteById(id) > 0;
    }
}
