package com.service.screenReception.service;



import com.service.screenReception.dto.UserServiceInfoDto;
import com.service.screenReception.entity.UserServiceInfo;

import java.util.List;

/**
 * 用户与服务关联表(UserServiceInfo)表服务接口
 *
 * @author makejava
 * @since 2021-04-22 09:50:41
 */
public interface UserServiceInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id
     * @return 实例对象
     */
    UserServiceInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserServiceInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param userServiceInfo 实例对象
     * @return 实例对象
     */
    UserServiceInfo insert(UserServiceInfo userServiceInfo);

    /**
     * 修改数据
     *
     * @param userServiceInfo 实例对象
     * @return 实例对象
     */
    UserServiceInfo update(UserServiceInfo userServiceInfo);

    /**
     * 通过主键删除数据
     *
     * @param id
     * @return 是否成功
     */
    boolean deleteById(String id);

    UserServiceInfoDto queryAll(String serviceId, String userId) throws Exception;
}
