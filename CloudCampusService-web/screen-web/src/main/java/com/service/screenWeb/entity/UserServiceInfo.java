package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * 用户与服务关联表(UserServiceInfo)实体类
 *
 * @author makejava
 * @since 2021-04-22 09:50:37
 */
public class UserServiceInfo implements Serializable {
    private static final long serialVersionUID = -54159755953866898L;
    /**
     * 云服务id
     */
    private String serviceId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 组件信息
     */
    private String assemblyInfo;
    /**
     * 组件信息
     */
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAssemblyInfo() {
        return assemblyInfo;
    }

    public void setAssemblyInfo(String assemblyInfo) {
        this.assemblyInfo = assemblyInfo;
    }

}
