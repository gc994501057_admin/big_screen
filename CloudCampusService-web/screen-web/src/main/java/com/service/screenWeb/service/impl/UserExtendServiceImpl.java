package com.service.screenWeb.service.impl;

import com.service.screenWeb.dao.UserExtendDao;
import com.service.screenWeb.entity.UserExtend;
import com.service.screenWeb.service.UserExtendService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户信息扩展(UserExtend)表服务实现类
 *
 * @author makejava
 * @since 2021-03-29 15:42:50
 */
@Service("userExtendService")
public class UserExtendServiceImpl implements UserExtendService {
    @Resource
    private UserExtendDao userExtendDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public UserExtend queryById(String id) {
        return this.userExtendDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<UserExtend> queryAllByLimit(int offset, int limit) {
        return this.userExtendDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param userExtend 实例对象
     * @return 实例对象
     */
    @Override
    public UserExtend insert(UserExtend userExtend) {
        this.userExtendDao.insert(userExtend);
        return userExtend;
    }

    /**
     * 修改数据
     *
     * @param userExtend 实例对象
     * @return 实例对象
     */
    @Override
    public UserExtend update(UserExtend userExtend) {
        this.userExtendDao.update(userExtend);
        return this.queryById(userExtend.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.userExtendDao.deleteById(id) > 0;
    }
}
