package com.service.screenWeb.service;

import com.service.screenWeb.entity.ModelInfo;

import java.util.List;

/**
 * (ModelInfo)表服务接口
 *
 * @author makejava
 * @since 2021-05-10 14:56:08
 */
public interface ModelInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ModelInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ModelInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param modelInfo 实例对象
     * @return 实例对象
     */
    ModelInfo insert(ModelInfo modelInfo);

    /**
     * 修改数据
     *
     * @param modelInfo 实例对象
     * @return 实例对象
     */
    ModelInfo update(ModelInfo modelInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}
