package com.service.screenWeb.dao;

import com.service.screenWeb.entity.LargeQa;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 意见、建议、问题反馈表(LargeQa)表数据库访问层
 *
 * @author makejava
 * @since 2021-05-07 15:00:52
 */
public interface LargeQaDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    LargeQa queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<LargeQa> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param largeQa 实例对象
     * @return 对象列表
     */
    List<LargeQa> queryAll(LargeQa largeQa);

    /**
     * 新增数据
     *
     * @param largeQa 实例对象
     * @return 影响行数
     */
    int insert(LargeQa largeQa);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<LargeQa> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<LargeQa> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<LargeQa> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<LargeQa> entities);

    /**
     * 修改数据
     *
     * @param largeQa 实例对象
     * @return 影响行数
     */
    int update(LargeQa largeQa);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}

