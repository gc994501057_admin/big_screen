package com.service.screenWeb.service;

import com.service.screenWeb.entity.AssemblyInfo;
import com.service.screenWeb.entity.ThirdAssembly;

import java.util.List;

/**
 * 组件信息表(AssemblyInfo)表服务接口
 *
 * @author makejava
 * @since 2021-05-18 19:56:28
 */
public interface AssemblyInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssemblyInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<AssemblyInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param assemblyInfo 实例对象
     * @return 实例对象
     */
    AssemblyInfo insert(AssemblyInfo assemblyInfo);

    /**
     * 修改数据
     *
     * @param assemblyInfo 实例对象
     * @return 实例对象
     */
    AssemblyInfo update(AssemblyInfo assemblyInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    List<ThirdAssembly> queryAll(AssemblyInfo assemblyInfo);

    List<AssemblyInfo> queryAllNoThirdAssembly();
}
