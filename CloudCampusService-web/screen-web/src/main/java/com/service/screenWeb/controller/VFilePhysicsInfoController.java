package com.service.screenWeb.controller;

import com.service.config.annotation.LoginUser;
import com.service.config.constant.Constant;
import com.service.config.enums.BusinessType;
import com.service.config.enums.OperatorType;
import com.service.config.utils.RedisUtil;
import com.service.config.utils.Result;
import com.service.config.utils.UserTokenManager;
import com.service.screenWeb.entity.VFilePhysicsInfo;
import com.service.screenWeb.service.VFilePhysicsInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 物理文件消息表(VFilePhysicsInfo)表控制层
 *
 * @author makejava
 * @since 2021-05-18 19:56:30
 */
@RestController
@RequestMapping("vFilePhysicsInfo")
@Api(value = "上传logo",produces = "上传logo")
public class VFilePhysicsInfoController {
    /**
     * 服务对象
     */
    @Resource
    private VFilePhysicsInfoService vFilePhysicsInfoService;
    @Resource
    private RedisUtil redisUtil;

    /**
     * 客户方案logo上传
     * @param cloudAuthToken
     * @param file
     * @param programmeId
     * @return
     * @throws Exception
     */
    @ApiOperation(value = " 客户方案logo上传")
    @RequestMapping(value = "uploadServiceLogo", method = RequestMethod.POST)
    @ResponseBody
    public Result uploadServiceLogo(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                    @RequestParam("file") MultipartFile file,
                                    @RequestParam("programmeId") String programmeId) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (!redisUtil.exist(cloudAuthToken + userId)) {
                return Result.failure(10001, "未登录或者用户不存在，请重新登录");
            }
            vFilePhysicsInfoService.uploadServiceLogo(file,programmeId);
            return Result.ok();
        }
        return Result.ok();
    }
}
