package com.service.screenWeb.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * 意见、建议、问题反馈表(LargeQa)实体类
 *
 * @author makejava
 * @since 2021-05-07 15:00:52
 */
public class LargeQaDto implements Serializable {
    private static final long serialVersionUID = -32445803362696167L;
    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private String id;
    /**
     * 用户id
     */
    @ApiModelProperty("主键id")
    private String userId;
    /**
     * 意见、建议、问题反馈
     */
    @ApiModelProperty("意见、建议、问题反馈内容")
    private String questionFeedback;
    /**
     * 提交时间
     */
    @ApiModelProperty("提交时间")
    private Date commitDate;
    /**
     * 状态 0--false（还未处理） 1--true（已处理）
     */
    @ApiModelProperty("状态 0--false（还未处理） 1--true（已处理）")
    private Boolean status;
    /**
     * 文本类型 0--意见 1--建议 2--问题反馈
     */
    @ApiModelProperty("文本类型 0--意见 1--建议 2--问题反馈")
    private String type;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQuestionFeedback() {
        return questionFeedback;
    }

    public void setQuestionFeedback(String questionFeedback) {
        this.questionFeedback = questionFeedback;
    }

    public Date getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(Date commitDate) {
        this.commitDate = commitDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
