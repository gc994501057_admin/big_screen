package com.service.screenWeb.service;



import com.service.screenWeb.dto.CheckTenantDto;
import com.service.screenWeb.dto.ScreenUserDto;
import com.service.screenWeb.entity.ScreenUser;

import java.util.List;
import java.util.Map;

/**
 * 大屏用户表(ScreenUser)表服务接口
 *
 * @author makejava
 * @since 2021-03-29 15:42:46
 */
public interface ScreenUserService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ScreenUser queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ScreenUser> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param screenUser 实例对象
     * @return 实例对象
     */
    ScreenUser insert(ScreenUser screenUser);

    /**
     * 修改数据
     *
     * @param screenUser 实例对象
     * @return 实例对象
     */
    ScreenUser update(ScreenUser screenUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    Map check(CheckTenantDto checkTenantDto, String userId);

    List<ScreenUser> insertOrUpdate(ScreenUser strictMap, String userId) throws Exception;

    void serviceStatusBind(String screen_user_id, Boolean status, String userI,String domain);

    void updateApiAccount(ScreenUser strictMap);

    List<ScreenUserDto> queryAll(String userId);

    void updateName(String userId, String name);
}
