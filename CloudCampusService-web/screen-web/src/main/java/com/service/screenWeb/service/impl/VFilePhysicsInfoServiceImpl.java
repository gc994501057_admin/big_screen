package com.service.screenWeb.service.impl;

import com.service.config.utils.IdUtil;
import com.service.config.utils.ModelMapperUtil;
import com.service.config.utils.ScanFilesWithRecursionUtils;
import com.service.screenWeb.dao.ProgrammeInfoDao;
import com.service.screenWeb.entity.ProgrammeInfo;
import com.service.screenWeb.entity.VFilePhysicsInfo;
import com.service.screenWeb.dao.VFilePhysicsInfoDao;
import com.service.screenWeb.service.VFilePhysicsInfoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 物理文件消息表(VFilePhysicsInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-05-18 19:56:30
 */
@Service("vFilePhysicsInfoService")
public class VFilePhysicsInfoServiceImpl implements VFilePhysicsInfoService {
    @Resource
    private VFilePhysicsInfoDao vFilePhysicsInfoDao;
    @Value("${spring.fileSeverIp}")
    private String systemIp;

    @Value("${spring.fileSeverDomain}")
    private String systemDomain;

    @Value("${spring.fileDic}")
    private String systemDic;

    @Value("${spring.fileDicDomain}")
    private String systemDicDomain;

    @Value("${spring.imageUrl}")
    private String imageUrl;
    @Resource
    private ProgrammeInfoDao programmeInfoDao;
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public VFilePhysicsInfo queryById(String id) {
        return this.vFilePhysicsInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<VFilePhysicsInfo> queryAllByLimit(int offset, int limit) {
        return this.vFilePhysicsInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param vFilePhysicsInfo 实例对象
     * @return 实例对象
     */
    @Override
    public VFilePhysicsInfo insert(VFilePhysicsInfo vFilePhysicsInfo) {
        this.vFilePhysicsInfoDao.insert(vFilePhysicsInfo);
        return vFilePhysicsInfo;
    }

    /**
     * 修改数据
     *
     * @param vFilePhysicsInfo 实例对象
     * @return 实例对象
     */
    @Override
    public VFilePhysicsInfo update(VFilePhysicsInfo vFilePhysicsInfo) {
        this.vFilePhysicsInfoDao.update(vFilePhysicsInfo);
        return this.queryById(vFilePhysicsInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.vFilePhysicsInfoDao.deleteById(id) > 0;
    }

    @Override
    public void uploadServiceLogo(MultipartFile file, String programmeId) throws Exception {
        Map duplicate = duplicate(file);
        VFilePhysicsInfo vFilePhysicsInfo = ModelMapperUtil.strictMap(duplicate.get("vFilePhysicsInfo"), VFilePhysicsInfo.class);
        Boolean.parseBoolean(duplicate.get("boolean").toString());
        if (programmeId != null && !programmeId.equals("")) {
            uploadLogo(vFilePhysicsInfo,file);
        }
        ProgrammeInfo programmeInfo = new ProgrammeInfo();
        programmeInfo.setFileId(vFilePhysicsInfo.getId());
        programmeInfo.setId(programmeId);
        programmeInfoDao.update(programmeInfo);
    }
    /**
     * 上传logo
     * @param vFilePhysicsInfo
     * @throws Exception
     */
    @Transactional
    public void uploadLogo(VFilePhysicsInfo vFilePhysicsInfo, MultipartFile file) throws IOException {
        vFilePhysicsInfo.setFiledic(imageUrl);
        vFilePhysicsInfo.setFiledicdomain("large_screen/logo");
        File file1 = createFile(vFilePhysicsInfo.getFiledic() +"/"+ vFilePhysicsInfo.getFilename());
        createFile(file,file1);
        vFilePhysicsInfo.setFilemd5(ScanFilesWithRecursionUtils.getJdkFileMD5(file1.getPath()));
        vFilePhysicsInfoDao.insert(vFilePhysicsInfo);
    }
    private File createFile(String s) {
        File file = new File(s);
        return file;
    }
    private void createFile(MultipartFile file,File file1) throws IOException {
        file.transferTo(file1);
    }
    /**
     * 抽取共同代码
     * @param file
     * @return
     * @throws Exception
     */
    private Map duplicate(MultipartFile file) throws Exception {
        Map map = new HashMap();
        VFilePhysicsInfo vFilePhysicsInfo = new VFilePhysicsInfo();
        vFilePhysicsInfo.setId(IdUtil.getStringId());
        String originalFilename = file.getOriginalFilename();// 文件原始名称
        vFilePhysicsInfo.setFilerealname(originalFilename); // 设置文件真实名字
        int i = originalFilename.lastIndexOf(".");
        String substring = originalFilename.substring(i + 1);
        boolean b1 = judgeFileType(substring);// 判断文件类型
        vFilePhysicsInfo.setFiletype(substring);
        vFilePhysicsInfo.setFilename(UUID.randomUUID().toString().replace("-","")+"."+substring); // 对文件重命名
        vFilePhysicsInfo.setFileseverip(systemIp);// 服务器ip
        vFilePhysicsInfo.setFileseverdomain(systemDomain); // 服务器域名
        vFilePhysicsInfo.setStatus("1");
        vFilePhysicsInfo.setDelflag("0");
        map.put("vFilePhysicsInfo",vFilePhysicsInfo);
        map.put("boolean",b1);
        return map;
    }
    /**
     * 判断文件类型
     * @param substring
     * @return
     */
    private boolean judgeFileType(String substring) {
        if (substring.equals("gif")) {
            return true;
        }
        if (substring.equals("jpg")) {
            return true;
        }
        if (substring.equals("jpeg")) {
            return true;
        }
        if (substring.equals("png")) {
            return true;
        }
        if (substring.equals("GIF")) {
            return true;
        }
        if (substring.equals("JPG")) {
            return true;
        }
        if (substring.equals("JPEG")) {
            return true;
        }
        if (substring.equals("PNG")) {
            return true;
        }
        return false;
    }
}
