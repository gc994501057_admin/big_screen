package com.service.login.datasource;

import com.service.config.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * @author
 * @version 1.0
 * @date 2021/4/28 14:43
 */
public class DynamicDataSourceContextHolder {
    public static final Logger log = LoggerFactory.getLogger(DynamicDataSourceContextHolder.class);
    private static final ThreadLocal<String> contextHolder = new ThreadLocal();

    public static String get() {
        return contextHolder.get();
    }

    public static void set(String dataSourceType) {
        contextHolder.set(dataSourceType);
    }

    public static void clear() {
        contextHolder.remove();
    }
}
