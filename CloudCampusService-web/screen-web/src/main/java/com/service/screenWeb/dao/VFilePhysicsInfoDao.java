package com.service.screenWeb.dao;

import com.service.screenWeb.entity.VFilePhysicsInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 物理文件消息表(VFilePhysicsInfo)表数据库访问层
 *
 * @author makejava
 * @since 2021-05-18 19:56:30
 */
public interface VFilePhysicsInfoDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    VFilePhysicsInfo queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<VFilePhysicsInfo> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param vFilePhysicsInfo 实例对象
     * @return 对象列表
     */
    List<VFilePhysicsInfo> queryAll(VFilePhysicsInfo vFilePhysicsInfo);

    /**
     * 新增数据
     *
     * @param vFilePhysicsInfo 实例对象
     * @return 影响行数
     */
    int insert(VFilePhysicsInfo vFilePhysicsInfo);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<VFilePhysicsInfo> 实例对象列表
     * @return 影响行数
     */
//    int insertBatch(@Param("entities") List<VFilePhysicsInfo> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<VFilePhysicsInfo> 实例对象列表
     * @return 影响行数
     */
//    int insertOrUpdateBatch(@Param("entities") List<VFilePhysicsInfo> entities);

    /**
     * 修改数据
     *
     * @param vFilePhysicsInfo 实例对象
     * @return 影响行数
     */
    int update(VFilePhysicsInfo vFilePhysicsInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}

