package com.service.screenReception.entity;

import java.io.Serializable;

/**
 * 大屏用户表(ScreenUser)实体类
 *
 * @author makejava
 * @since 2021-04-14 09:29:43
 */
public class ScreenUser implements Serializable {
    private static final long serialVersionUID = 284159235750833179L;
    /**
     * 大屏用户id
     */
    private String id;
    /**
     * 租户账号
     */
    private String tenantName;
    /**
     * 租户密码
     */
    private String tenantPwd;
    /**
     * 手机号码
     */
    private String telephone;
    /**
     * 模板id
     */
    private String modelId;
    /**
     * 模板背景色
     */
    private String modelColor;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 组件信息
     */
    private String assemblyInfo;

    /**
     * 域名 naas naas1 naas2 naas3
     * @return
     */
    private String domain;
    private Boolean status;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantPwd() {
        return tenantPwd;
    }

    public void setTenantPwd(String tenantPwd) {
        this.tenantPwd = tenantPwd;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelColor() {
        return modelColor;
    }

    public void setModelColor(String modelColor) {
        this.modelColor = modelColor;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAssemblyInfo() {
        return assemblyInfo;
    }

    public void setAssemblyInfo(String assemblyInfo) {
        this.assemblyInfo = assemblyInfo;
    }

}
