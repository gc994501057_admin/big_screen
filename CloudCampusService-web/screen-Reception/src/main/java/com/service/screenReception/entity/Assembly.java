package com.service.screenReception.entity;

import java.io.Serializable;

/**
 * 组件(Assembly)实体类
 *
 * @author makejava
 * @since 2021-04-14 09:29:29
 */
public class Assembly implements Serializable {
    private static final long serialVersionUID = 130595826242408359L;
    /**
     * id
     */
    private String id;
    /**
     * 组件名称
     */
    private String assemblyName;
    /**
     * 组件描述
     */
    private Double assemblyX;
    /**
     * 组件标题
     */
    private Double assemblyY;
    /**
     * 宽
     */
    private Double width;
    /**
     * 高
     */
    private Double height;
    /**
     * 序号
     */
    private Integer number;

    private String component;

    private String assemblyImg;


    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getAssemblyImg() {
        return assemblyImg;
    }

    public void setAssemblyImg(String assemblyImg) {
        this.assemblyImg = assemblyImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public void setAssemblyName(String assemblyName) {
        this.assemblyName = assemblyName;
    }

    public Double getAssemblyX() {
        return assemblyX;
    }

    public void setAssemblyX(Double assemblyX) {
        this.assemblyX = assemblyX;
    }

    public Double getAssemblyY() {
        return assemblyY;
    }

    public void setAssemblyY(Double assemblyY) {
        this.assemblyY = assemblyY;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

}
