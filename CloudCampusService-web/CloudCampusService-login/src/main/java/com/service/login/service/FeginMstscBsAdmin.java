package com.service.login.service;


import com.service.config.utils.Result;
import com.service.login.dto.ScreenUserDto;
import com.service.login.service.impl.UserServerFailBack;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author
 * @version 1.0
 * @date 2020/6/18 14:54
 */
@FeignClient(value = "service-screen",configuration = UserServerFailBack.class)
public interface FeginMstscBsAdmin {


    /**
     * 配置账号远程接口
     * @return
     */
    @RequestMapping(value = "/screenModel/queryInsideModelList",method = RequestMethod.GET)
    public Result addApiAccount( @RequestParam(value = "userId", required = false) @ApiParam("服务id存在则去查询用户自定义否则获取内置的模板列表")String userId);
}
