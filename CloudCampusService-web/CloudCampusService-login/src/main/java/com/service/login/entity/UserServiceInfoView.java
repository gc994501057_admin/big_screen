package com.service.login.entity;

import java.io.Serializable;

/**
 * (UserServiceInfoView)实体类
 *
 * @author makejava
 * @since 2021-04-22 10:05:08
 */
public class UserServiceInfoView implements Serializable {
    private static final long serialVersionUID = 475836939303849781L;
    /**
     * 用户id
     */
    private String id;
    /**
     * 账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 组件信息
     */
    private String assemblyInfo;
    /**
     * 主键id
     */
    private String serviceId;
    /**
     * 服务名字
     */
    private String serviceName;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAssemblyInfo() {
        return assemblyInfo;
    }

    public void setAssemblyInfo(String assemblyInfo) {
        this.assemblyInfo = assemblyInfo;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

}
