package com.service.screenWeb.dao;

import com.service.screenWeb.entity.ProgrammeInfo;
import com.service.screenWeb.entity.ProgrammeInfoView;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (ProgrammeInfoView)表数据库访问层
 *
 * @author makejava
 * @since 2021-05-19 09:46:52
 */
public interface ProgrammeInfoViewDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id
     * @return 实例对象
     */
    ProgrammeInfoView queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ProgrammeInfoView> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param programmeInfoView 实例对象
     * @return 对象列表
     */
    List<ProgrammeInfoView> queryAll(ProgrammeInfoView programmeInfoView);

    /**
     * 新增数据
     *
     * @param programmeInfoView 实例对象
     * @return 影响行数
     */
    int insert(ProgrammeInfoView programmeInfoView);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<ProgrammeInfoView> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<ProgrammeInfoView> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<ProgrammeInfoView> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<ProgrammeInfoView> entities);

    /**
     * 修改数据
     *
     * @param programmeInfoView 实例对象
     * @return 影响行数
     */
    int update(ProgrammeInfoView programmeInfoView);

    /**
     * 通过主键删除数据
     *
     * @param id
     * @return 影响行数
     */
    int deleteById(String id);

    List<ProgrammeInfoView> queryDefault();

    List<ProgrammeInfoView> queryByUserId(String userId);
}

