package com.service.login.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author
 * @version 1.0
 * @date 2021/3/30 14:33
 */
@Setter
@Getter
public class LoginUserDto {

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("强制登录标识符")
    private Boolean isLogin;
}
