package com.service.screenWeb.dto;

import lombok.Data;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-04-02 10:22
 **/
@Data
public class DeviceTrafficDto {
    private String siteId;
    private String deviceId;
    private String deviceName;
    private Double upTraffic;
    private Double downTraffic;
    private String unit;
}
