package com.service.login.service;

import com.service.config.utils.Result;
import com.service.login.dto.LoginUserDto;
import com.service.login.dto.SimplifyUserInfoDto;
import com.service.login.dto.UserInfoDto;
import com.service.login.entity.UserInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * 用户表(UserInfo)表服务接口
 *
 * @author makejava
 * @since 2021-03-30 11:37:21
 */
public interface UserInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param userInfo 实例对象
     * @return 实例对象
     */
    UserInfo insert(UserInfo userInfo);

    /**
     * 修改数据
     *
     * @param userInfo 实例对象
     * @return 实例对象
     */
    UserInfo update(UserInfo userInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    UserInfo queryAll(LoginUserDto loginUserDto);

    UserInfo register(UserInfoDto userInfoDto, String realIp);

    UserInfo queryByUserName(UserInfoDto username) throws Exception;

    void sendMail(UserInfoDto userInfoDto);

    UserInfo queryAlls(LoginUserDto loginUserDto);

    UserInfo mspRegister(UserInfoDto userInfoDto, String realIp);

    void updateMsp(UserInfo strictMap);

    UserInfo queryByMspUserName(UserInfoDto userInfoDto) throws Exception;

    HashMap getToken(LoginUserDto loginUserDto, String serviceId,HttpServletRequest httpServletRequest) throws Exception;

    Result simplifyRegister(SimplifyUserInfoDto simplifyUserInfoDto, String realIp) throws Exception;

    Result MspSimplifyRegister(SimplifyUserInfoDto simplifyUserInfoDto, String realIp) throws Exception;
}
