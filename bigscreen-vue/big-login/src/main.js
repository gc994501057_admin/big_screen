import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
Vue.use(ElementUI, {
    size: 'small'
});
import "./assets/css/main.css";
import axios from 'axios'
Vue.prototype.$axios = axios
axios.defaults.baseURL = '/flag' //地址替换

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
