package com.service.screenWeb.controller;

import com.alibaba.fastjson.JSONArray;
import com.service.config.utils.*;
import com.service.screenWeb.dto.*;
import com.service.screenWeb.entity.Assembly;
import com.service.screenWeb.entity.LargeQa;
import com.service.screenWeb.service.FeginMstsc;
import com.service.screenWeb.service.LargeQaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static com.service.config.constant.Constant.*;

/**
 * 意见、建议、问题反馈表(LargeQa)表控制层
 *
 * @author makejava
 * @since 2021-05-07 15:00:53
 */
@RestController
@RequestMapping("largeQa")
@Api(value = "意见、建议和问题反馈",description = "意见、建议和问题反馈")
public class LargeQaController {
    /**
     * 服务对象
     */
    @Resource
    private LargeQaService largeQaService;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    FeginMstsc feginMstsc;
    /**
     * 增加或者更新意见、建议和问题反馈
     * @param largeQaDto
     */
    @PostMapping("addAndUpdateLargeQa")
    @ApiOperation(value = "增加或者更新意见、建议和问题反馈")
    @ResponseBody
    public Result addAndUpdateLargeQa(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                      @RequestBody LargeQaDto largeQaDto) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                LargeQa largeQa = ModelMapperUtil.strictMap(largeQaDto, LargeQa.class);
                largeQa.setUserId(userId);
                this.largeQaService.addAndUpdateLargeQa(largeQa);//意见、建议和问题反馈
                return Result.ok();
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
    /**
     * 用户更新意见、建议和问题反馈的状态
     * @param largeQaStatusDto
     */
    @PostMapping("updateLargeQaStatus")
    @ApiOperation(value = "用户更新意见、建议和问题反馈的状态")
    @ResponseBody
    public Result updateLargeQaStatus(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                      @RequestBody LargeQaStatusDto largeQaStatusDto) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                LargeQa largeQa = ModelMapperUtil.strictMap(largeQaStatusDto, LargeQa.class);
                largeQa.setUpdateTime(new Date());
                this.largeQaService.update(largeQa); //用户更新意见、建议和问题反馈的状态
                return Result.ok();
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
    /**
     * 用户查询意见、建议和问题反馈列表
     * isKeFu为true客服查询所有问题
     */
    @GetMapping("queryLargeQa")
    @ApiOperation(value = "用户查询意见、建议和问题反馈列表")
    @ResponseBody
    public Result queryLargeQa(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                               @RequestParam(value = "getHistory",required = false) Boolean getHistory,
                               @RequestParam(value = "isKeFu") @ApiParam("true客服查询所有问题")Boolean isKeFu) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            // 客服查询所有问题
            if (isKeFu) {
                if (redisUtil.exist(cloudAuthToken)) {
                    LargeQa largeQa = new LargeQa();
                    List<LargeQaAndReplyDto> largeQaList = largeQaService.queryAll(largeQa, getHistory);
                    Result s = feginMstsc.s();
                    List<UserDto> userDtoList = JSONArray.parseArray(JsonXMLUtils.obj2json(s.getData()), UserDto.class);
                    largeQaList.stream().forEach(largeQaAndReplyDto -> {
                        List<UserDto> collect = userDtoList.stream().filter(userDto -> userDto.getId().equals(largeQaAndReplyDto.getUserId())).collect(Collectors.toList());
                        if (collect.size() > 0) {
                            UserDto userDto = collect.get(0);
                            largeQaAndReplyDto.setUsername(userDto.getUsername());
                        }
                    });

                    if (largeQaList.size() > 0) {
                        List<LargeQaAndReplyDto> collect = largeQaList.stream().filter(llargeQaAndReplyDto
                                -> llargeQaAndReplyDto.getUsername() != null && !llargeQaAndReplyDto.equals("")).collect(Collectors.toList());
                        return Result.ok().setData(collect.stream().sorted(Comparator.comparing(LargeQaAndReplyDto::getUsername)).collect(Collectors.toList()));
                    }else {
                        return Result.ok();
                    }
                }
            }
            if (redisUtil.exist(cloudAuthToken + userId)) {
                LargeQa largeQa = new LargeQa();
                largeQa.setUserId(userId);
                List<LargeQaAndReplyDto> largeQaList = largeQaService.queryAll(largeQa,getHistory);
                return Result.ok().setData(largeQaList);
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
}
