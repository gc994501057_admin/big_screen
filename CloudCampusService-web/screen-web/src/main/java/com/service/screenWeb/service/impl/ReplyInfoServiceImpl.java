package com.service.screenWeb.service.impl;

import com.service.screenWeb.entity.ReplyInfo;
import com.service.screenWeb.dao.ReplyInfoDao;
import com.service.screenWeb.service.ReplyInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 回复表(ReplyInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-05-07 15:00:54
 */
@Service("replyInfoService")
public class ReplyInfoServiceImpl implements ReplyInfoService {
    @Resource
    private ReplyInfoDao replyInfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ReplyInfo queryById(String id) {
        return this.replyInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ReplyInfo> queryAllByLimit(int offset, int limit) {
        return this.replyInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param replyInfo 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public ReplyInfo insert(ReplyInfo replyInfo) {
        this.replyInfoDao.insert(replyInfo);
        return replyInfo;
    }

    /**
     * 修改数据
     *
     * @param replyInfo 实例对象
     * @return 实例对象
     */
    @Override
    public ReplyInfo update(ReplyInfo replyInfo) {
        this.replyInfoDao.update(replyInfo);
        return this.queryById(replyInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.replyInfoDao.deleteById(id) > 0;
    }
}
