package com.service.screenReception.dto;

import lombok.Data;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-06-10 09:35
 **/
@Data
public class HealthDto {
    private String name;
    private Integer value;
}
