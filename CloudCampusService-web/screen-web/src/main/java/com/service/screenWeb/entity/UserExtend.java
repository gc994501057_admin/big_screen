package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * 用户信息扩展(UserExtend)实体类
 *
 * @author makejava
 * @since 2021-03-29 15:42:50
 */
public class UserExtend implements Serializable {
    private static final long serialVersionUID = -33839337434466227L;
    /**
     * 用户信息扩展表
     */
    private String id;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 区域（地址）
     */
    private String address;
    /**
     * 职业
     */
    private String occupation;
    /**
     * 是否接受营销信息
     */
    private Boolean sendAllow;
    /**
     * 邮箱
     */
    private String email;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Boolean getSendAllow() {
        return sendAllow;
    }

    public void setSendAllow(Boolean sendAllow) {
        this.sendAllow = sendAllow;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
