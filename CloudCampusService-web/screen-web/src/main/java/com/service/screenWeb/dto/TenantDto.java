package com.service.screenWeb.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author
 * @version 1.0
 * @date 2021/4/16 15:23
 */
@Data
public class TenantDto {
    /**
     * 大屏用户id
     */
    @ApiModelProperty("大屏用户id")
    private String id;

    /**
     * 租户账号
     */
    @ApiModelProperty("租户账号")
    private String tenantName;
    /**
     * 租户密码
     */
    @ApiModelProperty("租户密码")
    private String tenantPwd;
    @ApiModelProperty("域名 naas naas1 naas2 naas3")
    private String domain;
}
