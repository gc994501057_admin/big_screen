package com.service.screenWeb.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.service.screenWeb.entity.Assembly;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @program: CloudCampusService
 * @description: 大屏用户
 * @author: rui
 * @create: 2021-03-31 15:07
 **/
@Data
public class ScreenUserDto {
    private static final long serialVersionUID = 475824938111398685L;
    /**
     * 大屏用户id
     */
    @ApiModelProperty("大屏用户id")
    private String id;
    /**
     * 租户账号
     */
    @ApiModelProperty("租户账号")
    private String tenantName;
    /**
     * 租户密码
     */
    @ApiModelProperty("租户密码")
    @JsonIgnore
    private String tenantPwd;
    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    private String telephone;
    /**
     * 模板id
     */
    @ApiModelProperty("模板id")
    private String modelId;
    /**
     * 模板背景色
     */
    @ApiModelProperty("模板背景色")
    private String modelColor;
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private String userId;
    /**
     * 域名 naas naas1 naas2 naas3
     */
    @ApiModelProperty("域名 naas naas1 naas2 naas3")
    private String domain;
    /**
     * 组件信息
     */
    @ApiModelProperty("组件信息")
    @JsonIgnore
    private List<Assembly> assemblyInfo;
    @ApiModelProperty("大屏名字")
    private String name;
    @ApiModelProperty("账号状态")
    private Boolean status;
}
