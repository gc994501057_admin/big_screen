package com.service.login.service;

import com.service.login.entity.UserLoginRecord;

import java.util.List;

/**
 * 用户表(UserLoginRecord)表服务接口
 *
 * @author makejava
 * @since 2021-05-11 11:34:15
 */
public interface UserLoginRecordService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserLoginRecord queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserLoginRecord> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param userLoginRecord 实例对象
     * @return 实例对象
     */
    UserLoginRecord insert(UserLoginRecord userLoginRecord);

    /**
     * 修改数据
     *
     * @param userLoginRecord 实例对象
     * @return 实例对象
     */
    UserLoginRecord update(UserLoginRecord userLoginRecord);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    void insertBatch(List<UserLoginRecord> userLoginRecordList);

    List<UserLoginRecord> queryAll(UserLoginRecord userLoginRecord);

    List<UserLoginRecord> queryAllMsp(UserLoginRecord userLoginRecord);

    void insertOrUpdateBatch(List<UserLoginRecord> userLoginRecordList);
}
