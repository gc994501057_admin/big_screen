package com.service.login.controller;

import com.service.config.utils.RedisUtil;
import com.service.config.utils.Result;
import com.service.config.utils.UserTokenManager;

import com.service.login.entity.UserServiceInfoView;
import com.service.login.service.UserServiceInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.List;


import static com.service.config.constant.Constant.*;

/**
 * 用户与服务关联表(UserServiceInfo)表控制层
 *
 * @author makejava
 * @since 2021-03-31 14:38:24
 */
@CrossOrigin
@RestController
@RequestMapping("userServiceInfo")
public class UserServiceInfoController {
    /**
     * 服务对象
     */
    @Resource
    private UserServiceInfoService userServiceInfoService;
    @Resource
    private RedisUtil redisUtil;

    /**
     * 用户获取已添加的服务
     *
     * @param
     * @return
     */
    @PostMapping(value = "getServices",produces="application/json")
    @ApiOperation(value = "用户获取已添加的服务")
    @ResponseBody
    public Result getServices(@RequestHeader(value = "cloud-Auth-Token") String cloudAuthToken) throws Exception {
        if(cloudAuthToken != null && !cloudAuthToken.equals("")){
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken+userId)) {
                List<UserServiceInfoView> userServiceInfos = userServiceInfoService.queryAll(userId);
                return  Result.ok().setData(userServiceInfos);
            }
            return Result.failure(USERNAME_Off_SITE,usernameOffSite);
        }else {
            return Result.failure(USER_NOT_LOGIN,"用户未登录");
        }
    }
}
