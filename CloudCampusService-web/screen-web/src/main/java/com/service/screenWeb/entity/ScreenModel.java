package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * 模板（视图）(ScreenModel)实体类
 *
 * @author makejava
 * @since 2021-04-14 09:29:38
 */
public class ScreenModel implements Serializable {
    private static final long serialVersionUID = 748783059353382915L;
    /**
     * id
     */
    private String id;
    /**
     * 模板Id
     */
    private String modelId;
    /**
     * 模板名称
     */
    private String modelName;
    /**
     * 模板描述
     */
    private String modelDesc;
    /**
     * 模板描述
     */
    private String name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelDesc() {
        return modelDesc;
    }

    public void setModelDesc(String modelDesc) {
        this.modelDesc = modelDesc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
