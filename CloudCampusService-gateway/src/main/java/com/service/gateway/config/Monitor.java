package com.service.gateway.config;

/**
 * @author
 * @version 1.0
 * @date 2020/6/12 15:49
 */



import com.service.config.utils.JsonXMLUtils;
import com.service.config.utils.RedisUtil;
import com.service.gateway.dynamicRouteService.DynamicRouteServiceImpl;
import com.service.gateway.model.GatewayFilterDefinition;
import com.service.gateway.model.GatewayPredicateDefinition;
import com.service.gateway.model.GatewayRouteDefinition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Resource;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Scope("singleton")
@Component
public class Monitor implements ApplicationRunner {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    RedisUtil redisUtil;
    @Resource
    private DynamicRouteServiceImpl dynamicRouteService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Set<String> service = redisUtil.values("service");
        for (String s : service){
            GatewayRouteDefinition gatewayRouteDefinition = JsonXMLUtils.json2obj(s,GatewayRouteDefinition.class);
            RouteDefinition definition = assembleRouteDefinition(gatewayRouteDefinition);
            logger.info("route info --"+definition);
            this.dynamicRouteService.update(definition);
        }
    }
    //把传递进来的参数转换成路由对象
    public RouteDefinition assembleRouteDefinition(GatewayRouteDefinition gwdefinition) {
        RouteDefinition definition = new RouteDefinition();
        definition.setId(gwdefinition.getId());
        definition.setOrder(gwdefinition.getOrder());

        //设置断言
        List<PredicateDefinition> pdList=new ArrayList<>();
        List<GatewayPredicateDefinition> gatewayPredicateDefinitionList=gwdefinition.getPredicates();
        for (GatewayPredicateDefinition gpDefinition: gatewayPredicateDefinitionList) {
            PredicateDefinition predicate = new PredicateDefinition();
            predicate.setArgs(gpDefinition.getArgs());
            predicate.setName(gpDefinition.getName());
            pdList.add(predicate);
        }
        definition.setPredicates(pdList);

        //设置过滤器
        ArrayList<FilterDefinition> filters = new ArrayList<FilterDefinition>();
        List<GatewayFilterDefinition> gatewayFilters = gwdefinition.getFilters();
        for(GatewayFilterDefinition filterDefinition : gatewayFilters){
            FilterDefinition filter = new FilterDefinition();
            filter.setName(filterDefinition.getName());
            filter.setArgs(filterDefinition.getArgs());
            filters.add(filter);
        }
        definition.setFilters(filters);

        URI uri = null;
        if(gwdefinition.getUri().startsWith("http")){
            uri = UriComponentsBuilder.fromHttpUrl(gwdefinition.getUri()).build().toUri();
        }else{
            // uri为 lb://consumer-service 时使用下面的方法
            uri = URI.create(gwdefinition.getUri());
        }
        definition.setUri(uri);
        return definition;
    }
}
