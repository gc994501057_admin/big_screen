package com.service.login.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 用户表(UserInfo)实体类
 *
 * @author makejava
 * @since 2021-05-09 09:41:05
 */
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 960216171896668892L;
    /**
     * 用户id
     */
    private String id;
    /**
     * 账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 手机号码
     */
    private String telephone;
    /**
     * 注册时间
     */
    private Date addTime;
    /**
     * 登录时间
     */
    private Date date;
    /**
     * 登录次数，新注册用户次数为1
     */
    private Integer count;
    /**
     * 账号状态0--在线 1--下线 2--封号 3--销号（不可恢复）
     */
    private String status;
    /**
     * 下线时间
     */
    private Date offTime;
    /**
     * 登录ip
     */
    private String ip;
    private long onlineTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getOffTime() {
        return offTime;
    }

    public void setOffTime(Date offTime) {
        this.offTime = offTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public long getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(long onlineTime) {
        this.onlineTime = onlineTime;
    }
}
