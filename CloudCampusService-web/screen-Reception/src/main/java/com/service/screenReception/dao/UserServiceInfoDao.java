package com.service.screenReception.dao;


import com.service.screenReception.entity.UserServiceInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户与服务关联表(UserServiceInfo)表数据库访问层
 *
 * @author makejava
 * @since 2021-04-22 09:50:40
 */
public interface UserServiceInfoDao {

    /**
     * 通过ID查询单条数据
     *
     * @param userId
     * @return 实例对象
     */
    UserServiceInfo queryById(String userId);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserServiceInfo> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param userServiceInfo 实例对象
     * @return 对象列表
     */
    List<UserServiceInfo> queryAll(UserServiceInfo userServiceInfo);

    /**
     * 新增数据
     *
     * @param userServiceInfo 实例对象
     * @return 影响行数
     */
    int insert(UserServiceInfo userServiceInfo);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserServiceInfo> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<UserServiceInfo> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserServiceInfo> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<UserServiceInfo> entities);

    /**
     * 修改数据
     *
     * @param userServiceInfo 实例对象
     * @return 影响行数
     */
    int update(UserServiceInfo userServiceInfo);
}

