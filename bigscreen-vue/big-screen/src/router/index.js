import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: '/bs/',
    routes: [
        {
            path: '/',
            component: () => import('../views/home.vue'),
            meta: { title: '首页' }
        },
        {
            path: '/test',
            component: () => import('../views/test.vue'),
            meta: { title: '测试' }
        },
        {
            path: '/anylink',
            component: () => import('../views/anylink.vue'),
            meta: { title: 'anylink' }
        },
        {
            path: '*',
            component: () => import('../views/home.vue'),
            meta: { title: '首页' }
        },{
            path: '/map',
            component: () => import('../components/mapGIS/index.vue'),
            meta: { title: '地图' }
        },{
            path: '/compared',
            component: () => import('../components/compared/index.vue'),
            meta: { title: '分类型的设备数量' }
        },{
            path: '/overview',
            component: () => import('../components/overview/index.vue'),
            meta: { title: '在线总人数' }
        },{
            path: '/networkTraffic',
            component: () => import('../components/networkTraffic/index.vue'),
            meta: { title: '今日网络流量' }
        },{
            path: '/talent',
            component: () => import('../components/talent/index.vue'),
            meta: { title: 'Top流量应用' }
        },{
            path: '/business',
            component: () => import('../components/business/index.vue'),
            meta: { title: '设备总数' }
        },{
            path: '/seamless',
            component: () => import('../components/seamless/index.vue'),
            meta: { title: '终端用户在线时长' }
        },{
            path: '/accessTerminal',
            component: () => import('../components/accessTerminal/index.vue'),
            meta: { title: '有线/无线接入终端数' }
        },{
            path: '/deviceStatus',
            component: () => import('../components/deviceStatus/index.vue'),
            meta: { title: '设备状态总览' }
        },{
            path: '/networkTrafficMonth',
            component: () => import('../components/networkTrafficMonth/index.vue'),
            meta: { title: '本月网络流量' }
        },{
            path: '/networkTrafficYear',
            component: () => import('../components/networkTrafficYear/index.vue'),
            meta: { title: '今年网络流量' }
        },{
            path: '/terminalTraffic',
            component: () => import('../components/terminalTraffic/index.vue'),
            meta: { title: '终端总计流量列表' }
        }
        ,{
            path: '/topology',
            component: () => import('../components/topology/index.vue'),
            meta: { title: '站点拓扑图' }
        }
        ,{
            path: '/deviceHealth',
            component: () => import('../components/Health/deviceHealth.vue'),
            meta: { title: '设备健康度' }
        },{
            path: '/deviceApHealth',
            component: () => import('../components/Health/deviceApHealth.vue'),
            meta: { title: 'AP设备健康度' }
        },{
            path: '/radioHealth',
            component: () => import('../components/Health/radioHealth.vue'),
            meta: { title: '射频健康度' }
        },{
            path: '/siteHealth',
            component: () => import('../components/Health/siteHealth.vue'),
            meta: { title: '站点健康度' }
        }
    ]
});
