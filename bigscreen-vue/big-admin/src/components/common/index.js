import compared from './compared'; // 3-分类型的设备数量-饼
import overview from './overview'; // 4-在线总人数-柱
import mapGIS from './mapGIS'; // 1号站点的地图分布-map
import networkTraffic from './networkTraffic'; // 5号网络流量-柱
import talent from './talent' // 6-TopN流量应用-环
import business from './business' // 2-设备总数-柱
import seamless from './seamless' // 终端用户在线时长  滚动图
import accessTerminal from './accessTerminal' // 有线/无线接入终端数  环
import deviceStatus from './deviceStatus' // 设备状态总览  数字

import networkTrafficMonth from './networkTrafficMonth'; // 本月网络流量-柱
import networkTrafficYear from './networkTrafficYear'; // 今年网络流量-柱
import terminalTraffic from './terminalTraffic'; // 终端流量列表- 滚动图
// import topology from './topology'; // 站点拓扑图- 滚动图
import deviceHealth from './Health/deviceHealth'; // 设备健康度
import deviceApHealth from './Health/deviceApHealth'; // AP设备健康度
import radioHealth from './Health/radioHealth'; // 射频健康度
import siteHealth from './Health/siteHealth'; // 站点健康度

import iframeCustomize1 from './iframeCustomize1' // 第三方接口1 
import iframeCustomize2 from './iframeCustomize2' // 第三方接口2 
import iframeCustomize3 from './iframeCustomize3' // 第三方接口3 
import iframeCustomize4 from './iframeCustomize4' // 第三方接口4 
import iframeCustomize5 from './iframeCustomize5' // 第三方接口5

const components = {
  compared, 
  overview, 
  mapGIS, 
  networkTraffic,
  talent,
  business,
  seamless,
  accessTerminal,
  deviceStatus,
  networkTrafficMonth,
  networkTrafficYear,
  terminalTraffic,
  // topology,
  deviceHealth,
  deviceApHealth,
  radioHealth,
  siteHealth,

  iframeCustomize1,
  iframeCustomize2,
  iframeCustomize3,
  iframeCustomize4,
  iframeCustomize5,
  
};

const install = (Vue = {}) => {
  if (install.installed) return;
  Object.keys(components).forEach(component => {
    Vue.component(components[component].name, components[component]);
  });

  install.installed = true;
};

install.installed = false;

if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue);
  install.installed = true;
}

const Vcomp = {
  ...components,
  install
};


export default Vcomp
