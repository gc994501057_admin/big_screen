package com.service.uniqueSequence;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * @author
 * @version 1.0
 * @date 2021/3/30 17:00
 */
//指定aop事务执行顺序，已保证在切换数据源的后面
//排除数据源自动配置
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})

@EnableDiscoveryClient
@EnableAsync
@EnableScheduling
@EnableFeignClients
public class UniqueApplication {
    public static void main(String[] args) {
        SpringApplication.run(UniqueApplication.class, args);
    }
}
