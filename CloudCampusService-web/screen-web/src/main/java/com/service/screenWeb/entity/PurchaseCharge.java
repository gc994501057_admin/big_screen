package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * 用户购买计费表（字段待补充）(PurchaseCharge)实体类
 *
 * @author makejava
 * @since 2021-03-29 15:42:32
 */
public class PurchaseCharge implements Serializable {
    private static final long serialVersionUID = 163837256320544212L;
    /**
     * id
     */
    private String id;
    /**
     * 用户id
     */
    private String userId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
