package com.service.login.dao;

import com.service.login.entity.IndustryInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * L2行业信息表(IndustryInfo)表数据库访问层
 *
 * @author makejava
 * @since 2021-05-09 11:34:25
 */
public interface IndustryInfoDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    IndustryInfo queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<IndustryInfo> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param industryInfo 实例对象
     * @return 对象列表
     */
    List<IndustryInfo> queryAll(IndustryInfo industryInfo);

    /**
     * 新增数据
     *
     * @param industryInfo 实例对象
     * @return 影响行数
     */
    int insert(IndustryInfo industryInfo);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<IndustryInfo> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<IndustryInfo> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<IndustryInfo> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<IndustryInfo> entities);

    /**
     * 修改数据
     *
     * @param industryInfo 实例对象
     * @return 影响行数
     */
    int update(IndustryInfo industryInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}

