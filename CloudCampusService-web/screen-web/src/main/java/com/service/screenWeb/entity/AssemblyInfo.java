package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * 组件信息表(AssemblyInfo)实体类
 *
 * @author makejava
 * @since 2021-05-18 19:56:26
 */
public class AssemblyInfo implements Serializable {
    private static final long serialVersionUID = 381543463989349998L;
    /**
     * id
     */
    private String id;
    /**
     * 组件名称
     */
    private String assemblyName;
    /**
     * 序号
     */
    private Integer number;
    /**
     * vue路由组件
     */
    private String component;
    /**
     * 组件引用的图片地址
     */
    private String assemblyImg;
    /**
     * 组件类型 0--内置组件 1--第三方组件
     */
    private Integer type;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public void setAssemblyName(String assemblyName) {
        this.assemblyName = assemblyName;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getAssemblyImg() {
        return assemblyImg;
    }

    public void setAssemblyImg(String assemblyImg) {
        this.assemblyImg = assemblyImg;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
