package com.service.login.dto;

import com.service.login.entity.UserExtend;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户表(UserInfo)实体类
 *
 * @author makejava
 * @since 2021-03-30 11:37:21
 */
@Data
public class UserInfoDto implements Serializable {
    /**
     * 用户id
     */
    private String id;
    /**
     * 账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 手机号码
     */
    private String telephone;
    private UserExtend userExtend;

}
