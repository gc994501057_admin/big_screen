package com.service.screenReception.service;

import com.service.screenReception.dto.ScreenUserDto;
import com.service.screenReception.entity.ScreenUser;

import java.util.List;

/**
 * 大屏用户表(ScreenUser)表服务接口
 *
 * @author makejava
 * @since 2021-04-14 09:29:45
 */
public interface ScreenUserService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ScreenUser queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ScreenUser> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param screenUser 实例对象
     * @return 实例对象
     */
    ScreenUser insert(ScreenUser screenUser);

    /**
     * 修改数据
     *
     * @param screenUser 实例对象
     * @return 实例对象
     */
    ScreenUser update(ScreenUser screenUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    ScreenUser checkTenant(ScreenUserDto screenUserDto, String userId);

    ScreenUser insideAddApiAccount(ScreenUserDto screenUserDto, String userId);
}
