package com.service.config.constant;

/**
 * @ClassName ApiUrlConstants
 * @Description TODO
 * @Author guochuang
 * @Date 2021/03/29 11:21
 * @Version 1.0
 */
public interface ApiUrlConstants {
    //顾客端AppId xxxx 测试用 xxxx  正式 xxxx
    public static final String CUSTOMER_APP_ID = "xxxx";
    //顾客端密钥 xxxx 测试用 xxxx
    // 正式 xxxx
    public static final String CUSTOMER_SECRET = "xxxx";
    //老板端AppId xxxx
    public static final String BOSS_APP_ID= "xxxx";
    //老板端密钥 xxxx
    public static final String BOSS_SECRET = "xxxx";

    public static final String URL_FIRST = "https://";

    // 获取云管理token
    public static final String CLOUD_CAMPUS_TOKEN_URL = ".huaweicloud.com:18002/controller/v2/tokens";

    // 获取站点
    public static final String SITE_URL = ".huaweicloud.com:18002/controller/campus/v3/sites?pageSize=100&pageIndex=";

    // 根据siteId获取SSID配置
    public static final String SS_ID_URL = ".huaweicloud.com:18002/controller/campus/v3/networkconfig/site/";

    // 创建指定站点的SSID配置
    public static final String SS_ID_ADD_URL = ".huaweicloud.com:18002/controller/campus/v3/networkconfig/site/";

    // 修改指定站点的SSID配置
    public static final String SS_ID_UPDATE_URL = ".huaweicloud.com:18002/controller/campus/v3/networkconfig/site/";

    // 删除指定站点的SSID配置
    public static final String SS_ID_DEL_URL = ".huaweicloud.com:18002/controller/campus/v3/networkconfig/site/";

    // 站点下的设备
    public static final String DEVICES_URL = ".huaweicloud.com:18002/controller/campus/v3/devices";

    // 设备维度下用户信息
    public static final String DEVICE_USER_INFO = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/station/client/device/";

    // 站点维度下用户信息
    public static final String SITE_USER_INFO = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/station/client/site/";

    //Portal在线用户查询
    public static final String PROTAL_ONLINE_USER = ".huaweicloud.com:18002/controller/campus/v1/accountservice/onlineusers?";

    //获取查询终端TopN应用流量
    public static final String APP_TRAFFIC = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/application/apptraffic/topapp?top=5&timeDimension=day&appDimension=apptype&siteId=";

    //查询站点维度TopN设备或者所有设备的上行流量、下行流量
    public static final String DEVICE_TRAFFIC = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/basicperformance/devicetraffic/statistic/site/";

    //查询实时接入客户数量
    public static final String REAL_TIME_FLOW = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/endpointbehavior/realtimeflow?siteId=";

    //终端用户列表信息查询
    public static final String TERMINAL_USER_LIST = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/station/client/site/";


    //查询TOP N SSID流量和最近在线用户数
    public static final String TOPN_SSID_TRAFFIC = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/basicperformance/topnssidtraffic/";

    //基于站点的站点健康度查询
    public static final String SITE_HEALTH = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/basicperformance/siteshealth/";

    //查询云管设备健康度趋势
    public static final String CLOUD_DEVICE_HEALTH = ".huaweicloud.com:18002/controller/campus/v1/performanceservice/basicperformance/trend/";
    //get请求 微信登录凭证校验接口 appid=APPID&secret=SECRET&
    public static final String WEI_XIN_CODE2_SESSION = "https://api.weixin.qq.com/sns/jscode2session?grant_type=authorization_code&appid=";
    public static final String secret = "&secret=";
    public static final String js_code = "&js_code=";
    //getAccessToken 获取小程序全局唯一后台接口调用凭据（access_token）。调用绝大多数后台接口时都需使用 access_token，开发者需要进行妥善保存
    public static final String WEI_XIN_GET_ACCESS_TOKEN =  "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=";

    // 生成二维码 getUnlimited POST
    public static final String WEI_XIN_GET_UNLIMITED = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=";
    // 获取 scene 值
    public static final String WEI_XIN_scene = "&scene=";

    //查询网络链路列表信息
    public static final String LINK_AGGREGATION = ".huaweicloud.com:18002/rest/openapi/network/link";

    //查询内联链路
    public static final String SITE_LINK_AGGREGATION = ".huaweicloud.com:18002/controller/campus/v1/sdwan/net/lag-ports";


}


