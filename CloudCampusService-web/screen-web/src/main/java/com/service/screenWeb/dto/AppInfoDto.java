package com.service.screenWeb.dto;

import lombok.Data;

/**
 * @program: CloudCampusService
 * @description: TopNapp流量
 * @author: rui
 * @create: 2021-04-01 11:06
 **/
@Data
public class AppInfoDto {

    private String siteId;

    private String name;
    //流量占总流量的比值。
    private String percent;
    //应用或者应用类型上行流量与下行流量总和大小。
    private String traffic;
    //流量单位。
    private String unit;

}
