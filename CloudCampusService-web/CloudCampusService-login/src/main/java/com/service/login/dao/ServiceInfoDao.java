package com.service.login.dao;

import com.service.login.entity.ServiceInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (ServiceInfo)表数据库访问层
 *
 * @author makejava
 * @since 2021-04-22 09:50:52
 */
public interface ServiceInfoDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ServiceInfo queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ServiceInfo> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param serviceInfo 实例对象
     * @return 对象列表
     */
    List<ServiceInfo> queryAll(ServiceInfo serviceInfo);

    /**
     * 新增数据
     *
     * @param serviceInfo 实例对象
     * @return 影响行数
     */
    int insert(ServiceInfo serviceInfo);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<ServiceInfo> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<ServiceInfo> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<ServiceInfo> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<ServiceInfo> entities);

    /**
     * 修改数据
     *
     * @param serviceInfo 实例对象
     * @return 影响行数
     */
    int update(ServiceInfo serviceInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}

