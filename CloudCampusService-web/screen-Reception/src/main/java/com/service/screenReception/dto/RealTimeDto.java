package com.service.screenReception.dto;

import lombok.Data;

import java.util.List;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-04-02 14:36
 **/
@Data
public class RealTimeDto {
    private String siteId;
    private String name;
    private Integer value;
}
