package com.service.login.dao;

import com.service.login.entity.UserServiceInfoView;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (UserServiceInfoView)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-31 14:54:03
 */
public interface UserServiceInfoViewDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserServiceInfoView queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserServiceInfoView> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param userServiceInfoView 实例对象
     * @return 对象列表
     */
    List<UserServiceInfoView> queryAll(UserServiceInfoView userServiceInfoView);

    /**
     * 新增数据
     *
     * @param userServiceInfoView 实例对象
     * @return 影响行数
     */
    int insert(UserServiceInfoView userServiceInfoView);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserServiceInfoView> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<UserServiceInfoView> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserServiceInfoView> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<UserServiceInfoView> entities);

    /**
     * 修改数据
     *
     * @param userServiceInfoView 实例对象
     * @return 影响行数
     */
    int update(UserServiceInfoView userServiceInfoView);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}

