package com.service.login.service.impl;

import com.service.config.utils.IdUtil;
import com.service.config.utils.ModelMapperUtil;
import com.service.config.utils.Result;
import com.service.login.annotation.DataSource;
import com.service.login.bean.MailBean;
import com.service.login.dto.UserExtendDto;
import com.service.login.entity.UserExtend;
import com.service.login.dao.UserExtendDao;
import com.service.login.entity.UserInfo;
import com.service.login.enums.DataSourceType;
import com.service.login.service.FeginMstscUnique;
import com.service.login.service.UserExtendService;
import com.service.login.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户信息扩展(UserExtend)表服务实现类
 *
 * @author makejava
 * @since 2021-03-30 11:39:11
 */
@Service("userExtendService")
public class UserExtendServiceImpl implements UserExtendService {
    @Resource
    private UserExtendDao userExtendDao;
    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserExtendService userExtendService;
    @Autowired
    RedisTemplate redisTemplate;
    @Resource
    private FeginMstscUnique feginMstscUnique;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    @DataSource(DataSourceType.MASTER)
    public UserExtend queryById(String id) {
        return this.userExtendDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    @DataSource(DataSourceType.MASTER)
    public List<UserExtend> queryAllByLimit(int offset, int limit) {
        return this.userExtendDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param userExtend 实例对象
     * @return 实例对象
     */
    @Override
    @DataSource(DataSourceType.MASTER)
    public UserExtend insert(UserExtend userExtend) {
        this.userExtendDao.insert(userExtend);
        return userExtend;
    }

    /**
     * 修改数据
     *
     * @param userExtend 实例对象
     * @return 实例对象
     */
    @Override
    @DataSource(DataSourceType.MASTER)
    public UserExtend update(UserExtend userExtend) {
        this.userExtendDao.update(userExtend);
        return this.queryById(userExtend.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    @DataSource(DataSourceType.MASTER)
    @Transactional
    public boolean deleteById(String id) {
        return this.userExtendDao.deleteById(id) > 0;
    }
    /**
     * 大屏用户信息完善
     * @param userExtend
     */
    @Override
    @DataSource(DataSourceType.MASTER)
    @Transactional
    public void insertUserExtend(UserExtendDto userExtend) throws Exception {
        queryByUserIds(userExtend);
    }

    private void queryByUserIds(UserExtendDto userExtend) throws Exception {
        UserExtend userExtend1 = userExtendDao.queryByUserId(userExtend.getUserId());
        UserExtend userExtend2 = ModelMapperUtil.strictMap(userExtend, UserExtend.class);
        if (userExtend1 != null) {
            userExtend2.setId(userExtend1.getId());
            userExtend2 = identicalCode(userExtend, userExtend2);
            userExtendDao.update(userExtend2);
        }else {
            Result result = feginMstscUnique.addApiAccount();
            if (result.getCode() == 200) {
                userExtend2.setId(result.getData().toString());
            }else {
                userExtend2.setId(IdUtil.getStringId());
            }
            userExtend2 = identicalCode(userExtend, userExtend2);
            userExtendDao.insert(userExtend2);
        }
    }

    private UserExtend identicalCode(UserExtendDto userExtend, UserExtend userExtend2) {
        if (userExtend.getTelephone() != null) {
            UserInfo userInfo = new UserInfo();
            userInfo.setId(userExtend.getUserId());
            if (userExtend.getTelephone() != null) {
                userInfo.setTelephone(userExtend.getTelephone());
            }
            userInfoService.update(userInfo);
        }
        String[] city = userExtend.getCity();
        userExtend2.setCity(arrayToString(city));
        return userExtend2;
    }

    private String arrayToString(String[] city){
        StringBuilder sb = new StringBuilder();
        if (city != null && city.length > 0) {
            for (int i = 0; i < city.length; i++) {
                if (i < city.length - 1) {
                    sb.append(city[i] + ",");
                } else {
                    sb.append(city[i]);
                }
            }
        }
        return sb.toString();
    }

    /**
     * msp用户信息完善
     * @param userExtend
     */
    @Override
    @Transactional
    @DataSource(DataSourceType.SLAVE)
    public void insertMspUserExtend(UserExtendDto userExtend) throws Exception {
        queryByUserIds(userExtend);
    }

    @Override
    @Transactional
    @DataSource(DataSourceType.SLAVE)
    public UserExtendDto queryByMspUserId(String userId) {
        return getUserExtendDto(userId);
    }

    private UserExtendDto getUserExtendDto(String userId) {
        UserExtend userExtend = userExtendDao.queryByUserId(userId);
        UserInfo userInfo = userInfoService.queryById(userId);
        if (userInfo == null) {
            return null;
        }
        UserExtendDto userExtendDto = new UserExtendDto();
        if (userExtend != null) {
            userExtendDto = ModelMapperUtil.strictMap(userExtend, UserExtendDto.class);
            if (userExtend.getCity() != null) {
                userExtendDto.setCity(userExtend.getCity().split(","));
            }
            if (userInfo.getTelephone() != null) {
                userExtendDto.setTelephone(userInfo.getTelephone());
            }
            userExtendDto.setUsername(userInfo.getUsername());
            return  userExtendDto;
        }
        userExtendDto.setUsername(userInfo.getUsername());
        if (userInfo.getTelephone() != null) {
            userExtendDto.setTelephone(userInfo.getTelephone());
        }
        return userExtendDto;
    }

    @Override
    @Transactional
    @DataSource(DataSourceType.MASTER)
    public UserExtendDto queryByUserId(String userId) {
        return getUserExtendDto(userId);
    }

    @Override
    public void sendMail(MailBean mailBean) {
        redisTemplate.convertAndSend("sendEmail",mailBean); // 发送邮件
    }
}
