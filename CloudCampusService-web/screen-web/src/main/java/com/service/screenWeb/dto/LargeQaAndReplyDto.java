package com.service.screenWeb.dto;

import com.service.screenWeb.entity.ReplyInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 意见、建议、问题反馈表(LargeQa)实体类
 *
 * @author makejava
 * @since 2021-05-07 15:38:12
 */
public class LargeQaAndReplyDto implements Serializable {
    private static final long serialVersionUID = -21202144294894204L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 意见、建议、问题反馈
     */
    private String questionFeedback;
    /**
     * 初次提交时间
     */
    private Date commitDate;
    /**
     * 状态 0--false（还未处理） 1--true（已处理）
     */
    private Boolean status;
    /**
     * 文本类型 0--意见 1--建议 2--问题反馈
     */
    private String type;
    /**
     * 内容变更时间
     */
    private Date updateTime;
    /**
     * 问题编号
     */
    /**
     * 用户id
     */
    private String username;
    private String orderNo;
    private List<ReplyInfoDto> replyInfoList;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQuestionFeedback() {
        return questionFeedback;
    }

    public void setQuestionFeedback(String questionFeedback) {
        this.questionFeedback = questionFeedback;
    }

    public Date getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(Date commitDate) {
        this.commitDate = commitDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public List<ReplyInfoDto> getReplyInfoList() {
        return replyInfoList;
    }

    public void setReplyInfoList(List<ReplyInfoDto> replyInfoList) {
        this.replyInfoList = replyInfoList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
