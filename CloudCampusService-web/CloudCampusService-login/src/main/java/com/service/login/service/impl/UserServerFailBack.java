package com.service.login.service.impl;

import com.service.config.utils.Result;
import com.service.login.service.FeginMstscBsAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @version 1.0
 * @date 2020/6/18 11:36
 */
@Component
public class UserServerFailBack implements FeginMstscBsAdmin {
    @Autowired
    DiscoveryClient discoveryClient;
    @Override
    public Result addApiAccount(String userId) {
        return Result.failure(10000,"调用失败");
    }
}
