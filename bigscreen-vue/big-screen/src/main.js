import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
import './assets/iconfont/iconfont.css';
import './components/directives';
import 'babel-polyfill';
import less from 'less'
Vue.use(less)
Vue.config.productionTip = false;
Vue.use(ElementUI, {
    size: 'small'
});

import axios from 'axios'
Vue.prototype.$axios = axios
axios.defaults.baseURL = '/flag' //地址替换


import echarts from 'echarts'

import 'echarts/map/js/china.js';

import Vcomp from './components/index'
Vue.use(Vcomp)

import publicMess from './utils/publicMess.js'



// return obj;

//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | 大屏`;
    var addrstr = location.href;
    var str = addrstr.substring(addrstr.lastIndexOf('?') + 1, addrstr.length); 
    var arr = str.split('&');
    var obj = new Object();
    for (var i = 0; i < arr.length; i++) {
        var tmp_arr = arr[i].split('=');
        obj[decodeURIComponent(tmp_arr[0])] = decodeURIComponent(tmp_arr[1]);
    }
    if(obj.token){localStorage.setItem('cloud-Auth-Token', obj.token);}

    const role = localStorage.getItem('cloud-Auth-Token');


    if (!role ) {
       window.location.href = publicMess.url + "/bs_login/index.html";

    } else  {
        next()
    } 
});


new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
