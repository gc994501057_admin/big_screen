package com.service.config.utils;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

import java.util.ArrayList;

/**
 * @author
 * @version 1.0
 * @date 2020/12/2 15:14
 */
@Data
public class Tree {

    private int id;

    private int pId;

    private String name;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ArrayList<Tree> children;
    public void addChildren(Tree menu) {
        this.children.add(menu);
    }
    public Tree() {
        super();
        this.children = new ArrayList<>();
    }

}
