package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * (ProgrammeInfoView)实体类
 *
 * @author makejava
 * @since 2021-05-19 09:46:51
 */
public class ProgrammeInfoView implements Serializable {
    private static final long serialVersionUID = 696721447192441226L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 方案名称
     */
    private String programmeName;
    /**
     * 是否条幅 0--false 1--true
     */
    private Boolean isBanner;
    /**
     * 大屏条幅标题
     */
    private String name;
    /**
     * 上传文件id
     */
    private String fileId;
    /**
     * 布局信息（json数据）
     */
    private String layoutInfo;
    /**
     * 是否当前 0--false 1--true
     */
    private Boolean status;
    /**
     * 主键id
     */
    private String layoutId;
    /**
     * 组件x
     */
    private Double assemblyX;
    /**
     * 组件y
     */
    private Double assemblyY;
    /**
     * 高
     */
    private Double height;
    /**
     * 宽
     */
    private Double width;
    /**
     * id
     */
    private String assemblyId;
    /**
     * 组件引用的图片地址
     */
    private String assemblyImg;
    /**
     * 组件名称
     */
    private String assemblyName;
    /**
     * vue路由组件
     */
    private String component;
    /**
     * 序号
     */
    private Integer number;
    /**
     * 主键id
     */
    private String assemblyConfigureId;
    /**
     * 站点id--根据站点id进行站点拓扑
     */
    private String siteId;
    /**
     * url昵称
     */
    private String authUrl;
    /**
     * 接口url
     */
    private String apiUrl;
    /**
     * 认证密码
     */
    private String authPassword;
    /**
     * 认证账号
     */
    private String authUsername;
    /**
     * 颜色配置
     */
    private String backgroundColor;
    /**
     * 组件自定义刷新时间- 单位分钟
     */
    private Integer schedule;
    private String programmeImg;
    private String type;
    public String getProgrammeImg() {
        return programmeImg;
    }

    public void setProgrammeImg(String programmeImg) {
        this.programmeImg = programmeImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProgrammeName() {
        return programmeName;
    }

    public void setProgrammeName(String programmeName) {
        this.programmeName = programmeName;
    }

    public Boolean getIsBanner() {
        return isBanner;
    }

    public void setIsBanner(Boolean isBanner) {
        this.isBanner = isBanner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getLayoutInfo() {
        return layoutInfo;
    }

    public void setLayoutInfo(String layoutInfo) {
        this.layoutInfo = layoutInfo;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }

    public Double getAssemblyX() {
        return assemblyX;
    }

    public void setAssemblyX(Double assemblyX) {
        this.assemblyX = assemblyX;
    }

    public Double getAssemblyY() {
        return assemblyY;
    }

    public void setAssemblyY(Double assemblyY) {
        this.assemblyY = assemblyY;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public String getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(String assemblyId) {
        this.assemblyId = assemblyId;
    }

    public String getAssemblyImg() {
        return assemblyImg;
    }

    public void setAssemblyImg(String assemblyImg) {
        this.assemblyImg = assemblyImg;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public void setAssemblyName(String assemblyName) {
        this.assemblyName = assemblyName;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getAssemblyConfigureId() {
        return assemblyConfigureId;
    }

    public void setAssemblyConfigureId(String assemblyConfigureId) {
        this.assemblyConfigureId = assemblyConfigureId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getAuthPassword() {
        return authPassword;
    }

    public void setAuthPassword(String authPassword) {
        this.authPassword = authPassword;
    }

    public String getAuthUsername() {
        return authUsername;
    }

    public void setAuthUsername(String authUsername) {
        this.authUsername = authUsername;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Integer getSchedule() {
        return schedule;
    }

    public void setSchedule(Integer schedule) {
        this.schedule = schedule;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
