package com.service.screenWeb.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表(UserInfo)实体类
 *
 * @author makejava
 * @since 2021-03-30 11:37:21
 */
@Data
public class UserDto implements Serializable {
    private static final long serialVersionUID = -52503473282382160L;
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private String id;
    /**
     * 账号
     */
    @ApiModelProperty("账号")
    private String username;
    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String password;
    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    private String telephone;
    /**
     * 来源ip
     */
    @ApiModelProperty("用户ip")
    private String ip;
    /**
     * 登陆时间
     */
    @ApiModelProperty("登陆时间")
    private Date date;

    @ApiModelProperty("在线时长")
    private Long onlineTime;
    @ApiModelProperty("注销时间")
    private  Date  offTime;

}
