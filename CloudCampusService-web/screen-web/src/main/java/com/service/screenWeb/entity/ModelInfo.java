package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * (ModelInfo)实体类
 *
 * @author makejava
 * @since 2021-05-10 14:56:07
 */
public class ModelInfo implements Serializable {
    private static final long serialVersionUID = -75463877624739802L;
    /**
     * 组件描述
     */
    private Double assemblyX;
    /**
     * 组件标题
     */
    private Double assemblyY;
    /**
     * 宽
     */
    private Double width;
    /**
     * 高
     */
    private Double height;
    /**
     * 模板id
     */
    private String modelId;
    /**
     * 组件信息id
     */
    private String assemblyId;


    public Double getAssemblyX() {
        return assemblyX;
    }

    public void setAssemblyX(Double assemblyX) {
        this.assemblyX = assemblyX;
    }

    public Double getAssemblyY() {
        return assemblyY;
    }

    public void setAssemblyY(Double assemblyY) {
        this.assemblyY = assemblyY;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(String assemblyId) {
        this.assemblyId = assemblyId;
    }

}
