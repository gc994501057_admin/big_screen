package com.service.config.utils;


/**
 * 维护用户token
 */
public class UserTokenManager {
	public static String generateToken(String id) {
        JwtHelper jwtHelper = new JwtHelper();
        return jwtHelper.createToken(id);
    }
    public static String getUserId(String token) {
    	JwtHelper jwtHelper = new JwtHelper();
        String userId = jwtHelper.verifyTokenAndGetUserId(token);
    	if(userId == null || userId.equals("0")){
    		return null;
    	}
        return userId;
    }
}
