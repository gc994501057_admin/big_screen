package com.service.login.service.impl;

import com.service.config.utils.ModelMapperUtil;
import com.service.login.entity.IndustryInfo;
import com.service.login.dao.IndustryInfoDao;
import com.service.login.service.IndustryInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * L2行业信息表(IndustryInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-05-09 11:34:26
 */
@Service("industryInfoService")
public class IndustryInfoServiceImpl implements IndustryInfoService {
    @Resource
    private IndustryInfoDao industryInfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public IndustryInfo queryById(String id) {
        return this.industryInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<IndustryInfo> queryAllByLimit(int offset, int limit) {
        return this.industryInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param industryInfo 实例对象
     * @return 实例对象
     */
    @Override
    public IndustryInfo insert(IndustryInfo industryInfo) {
        this.industryInfoDao.insert(industryInfo);
        return industryInfo;
    }

    /**
     * 修改数据
     *
     * @param industryInfo 实例对象
     * @return 实例对象
     */
    @Override
    public IndustryInfo update(IndustryInfo industryInfo) {
        this.industryInfoDao.update(industryInfo);
        return this.queryById(industryInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.industryInfoDao.deleteById(id) > 0;
    }

    @Override
    public void queryAll(IndustryInfo industryInfo) {
        List<IndustryInfo> industryInfos = industryInfoDao.queryAll(industryInfo);
        List<IndustryInfo> collect = industryInfos.stream().filter(industryInfo1 -> industryInfo1.getIndustryId().equals("0")).collect(Collectors.toList());
    }
}
