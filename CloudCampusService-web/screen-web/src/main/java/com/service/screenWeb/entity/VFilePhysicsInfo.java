package com.service.screenWeb.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 物理文件消息表(VFilePhysicsInfo)实体类
 *
 * @author makejava
 * @since 2021-05-18 19:56:30
 */
public class VFilePhysicsInfo implements Serializable {
    private static final long serialVersionUID = 209401202119910549L;
    /**
     * 主键
     */
    private String id;
    /**
     * 文件名称（重命名后）
     */
    private String filename;
    /**
     * 文件真实名称
     */
    private String filerealname;
    /**
     * 文件存储服务器IP
     */
    private String fileseverip;
    /**
     * 文件存储服务器域名
     */
    private String fileseverdomain;
    /**
     * 物理文件存储目录
     */
    private String filedic;
    /**
     * 物理文件存储目录对应网络访问路径
     */
    private String filedicdomain;
    /**
     * 文件大小(b)
     */
    private Long filesize;
    /**
     * 文件类型(文件后缀)
     */
    private String filetype;
    /**
     * 文件md5码
     */
    private String filemd5;
    /**
     * 文件来源方式
     */
    private String filefromtype;
    /**
     * 文件备注
     */
    private String filebz;
    /**
     * 文件创建人
     */
    private String fileaddr;
    /**
     * 文件创建人昵称
     */
    private String fileaddrrealname;
    /**
     * 状态 0 停用 1 启用
     */
    private String status;
    /**
     * 删除标记 0 未删除 1 已删除
     */
    private String delflag;
    /**
     * 创建人
     */
    private String addr;
    /**
     * 创建时间
     */
    private Date addtime;
    /**
     * 修改人
     */
    private String updr;
    /**
     * 修改时间
     */
    private Date updtime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilerealname() {
        return filerealname;
    }

    public void setFilerealname(String filerealname) {
        this.filerealname = filerealname;
    }

    public String getFileseverip() {
        return fileseverip;
    }

    public void setFileseverip(String fileseverip) {
        this.fileseverip = fileseverip;
    }

    public String getFileseverdomain() {
        return fileseverdomain;
    }

    public void setFileseverdomain(String fileseverdomain) {
        this.fileseverdomain = fileseverdomain;
    }

    public String getFiledic() {
        return filedic;
    }

    public void setFiledic(String filedic) {
        this.filedic = filedic;
    }

    public String getFiledicdomain() {
        return filedicdomain;
    }

    public void setFiledicdomain(String filedicdomain) {
        this.filedicdomain = filedicdomain;
    }

    public Long getFilesize() {
        return filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getFilemd5() {
        return filemd5;
    }

    public void setFilemd5(String filemd5) {
        this.filemd5 = filemd5;
    }

    public String getFilefromtype() {
        return filefromtype;
    }

    public void setFilefromtype(String filefromtype) {
        this.filefromtype = filefromtype;
    }

    public String getFilebz() {
        return filebz;
    }

    public void setFilebz(String filebz) {
        this.filebz = filebz;
    }

    public String getFileaddr() {
        return fileaddr;
    }

    public void setFileaddr(String fileaddr) {
        this.fileaddr = fileaddr;
    }

    public String getFileaddrrealname() {
        return fileaddrrealname;
    }

    public void setFileaddrrealname(String fileaddrrealname) {
        this.fileaddrrealname = fileaddrrealname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDelflag() {
        return delflag;
    }

    public void setDelflag(String delflag) {
        this.delflag = delflag;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    public String getUpdr() {
        return updr;
    }

    public void setUpdr(String updr) {
        this.updr = updr;
    }

    public Date getUpdtime() {
        return updtime;
    }

    public void setUpdtime(Date updtime) {
        this.updtime = updtime;
    }

}
