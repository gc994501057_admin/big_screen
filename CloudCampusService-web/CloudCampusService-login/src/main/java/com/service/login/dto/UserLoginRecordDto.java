package com.service.login.dto;

import com.service.login.entity.UserLoginRecord;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户表(UserLoginRecord)实体类
 *
 * @author makejava
 * @since 2021-05-11 11:33:23
 */
public class UserLoginRecordDto implements Serializable {
    private static final long serialVersionUID = -43404234085215300L;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 记录集合
     */
    private List<UserLoginRecord> userLoginRecordList;
    /**
     * 历史在线时长
     */
    private long onlineTime;
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public long getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(long onlineTime) {
        this.onlineTime = onlineTime;
    }

    public List<UserLoginRecord> getUserLoginRecordList() {
        return userLoginRecordList;
    }

    public void setUserLoginRecordList(List<UserLoginRecord> userLoginRecordList) {
        this.userLoginRecordList = userLoginRecordList;
    }
}
