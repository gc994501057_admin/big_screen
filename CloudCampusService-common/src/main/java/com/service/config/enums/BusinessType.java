package com.service.config.enums;

/**
 *  * 业务操作类型
 * @ClassName BusinessType
 * @Description TODO
 * @Date 2021-02-03 15:02
 * @Version 1.0
 **/
public enum BusinessType
{
    /**
     * 其它
     */
    OTHER,

    /**
     * 新增
     */
    INSERT,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 授权
     */
    GRANT,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 导入
     */
    IMPORT,

    /**
     * 强退
     */
    FORCE,

    /**
     * 生成代码
     */
    GEN_CODE,

    /**
     * 清空
     */
    CLEAN,

    /**
     * 查询
     */
    QUERY,
}
