package com.service.login.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class MailBean implements Serializable {
    private static final long serialVersionUID = -2116367492649751914L;
    @ApiModelProperty("邮件接收人")
    private String recipient;//邮件接收人

    @ApiModelProperty("邮件主题")
    private String subject; //邮件主题

    @ApiModelProperty("邮件内容")
    private String content; //邮件内容

    @ApiModelProperty("附件路径")
    private String enclosurePath; // 附件路径
}
