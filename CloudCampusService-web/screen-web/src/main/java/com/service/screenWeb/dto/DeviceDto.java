package com.service.screenWeb.dto;

import lombok.Data;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-03-31 16:09
 **/
@Data
public class DeviceDto {
    private String id;
    private String name;
    private String deviceModel;
    private String deviceType;//设备类型，支持以下几种：“AR”、“AP”、“FW”或者“LSW”。
    private String status;//设备状态，'0'---正常、'1'---告警、'3'---离线、'4'---未注册。iMaster NCE-WAN场景下，AR设备无告警状态。
    private String siteId;
}
