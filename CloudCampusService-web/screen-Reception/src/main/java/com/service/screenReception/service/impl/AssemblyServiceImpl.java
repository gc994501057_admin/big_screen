package com.service.screenReception.service.impl;

import com.service.screenReception.dao.AssemblyDao;
import com.service.screenReception.entity.Assembly;
import com.service.screenReception.service.AssemblyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 组件(Assembly)表服务实现类
 *
 * @author makejava
 * @since 2021-04-14 09:29:34
 */
@Service("assemblyService")
public class AssemblyServiceImpl implements AssemblyService {
    @Resource
    private AssemblyDao assemblyDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Assembly queryById(String id) {
        return this.assemblyDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Assembly> queryAllByLimit(int offset, int limit) {
        return this.assemblyDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assembly 实例对象
     * @return 实例对象
     */
    @Override
    public Assembly insert(Assembly assembly) {
        this.assemblyDao.insert(assembly);
        return assembly;
    }

    /**
     * 修改数据
     *
     * @param assembly 实例对象
     * @return 实例对象
     */
    @Override
    public Assembly update(Assembly assembly) {
        this.assemblyDao.update(assembly);
        return this.queryById(assembly.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.assemblyDao.deleteById(id) > 0;
    }
}
