package com.service.screenWeb.service;

import com.service.screenWeb.entity.AssemblyConfigure;

import java.util.List;

/**
 * 组件单独配置表(AssemblyConfigure)表服务接口
 *
 * @author makejava
 * @since 2021-05-18 19:56:29
 */
public interface AssemblyConfigureService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssemblyConfigure queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<AssemblyConfigure> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param assemblyConfigure 实例对象
     * @return 实例对象
     */
    AssemblyConfigure insert(AssemblyConfigure assemblyConfigure);

    /**
     * 修改数据
     *
     * @param assemblyConfigure 实例对象
     * @return 实例对象
     */
    AssemblyConfigure update(AssemblyConfigure assemblyConfigure);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    List<AssemblyConfigure> queryDefault();
}
