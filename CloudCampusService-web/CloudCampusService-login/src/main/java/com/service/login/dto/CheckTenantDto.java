package com.service.login.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author
 * @version 1.0
 * @date 2021/4/15 16:44
 */
@Data
public class CheckTenantDto {
    @ApiModelProperty("api账号")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    /**
     * 域名 naas naas1 naas2 naas3
     * @return
     */
    @ApiModelProperty("域名 naas naas1 naas2 naas3")
    private String domain;
}
