import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    base: '/bs_admin/',
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: '/preview'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/',
                    component: () => import(/* webpackChunkName: "preview" */ '../components/page/Preview.vue'),
                    meta: { title: '效果预览' }
                },
                {
                    path: '/workbench',
                    component: () => import(/* webpackChunkName: "workbench" */ '../components/page/Workbench.vue'),
                    meta: { title: '定制大屏显示方案' }
                },
                {
                    path: '/chart',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/page/Chart.vue'),
                    meta: { title: '组件列表' }
                },
                {
                    path: '/account',
                    component: () => import(/* webpackChunkName: "account" */ '../components/page/Account.vue'),
                    meta: { title: '关联华为云管理账号' }
                },
                {
                    path: '/preview',
                    component: () => import(/* webpackChunkName: "preview" */ '../components/page/Preview.vue'),
                    meta: { title: '效果预览' }
                },
                {
                    path: '/thirdPart',
                    component: () => import(/* webpackChunkName: "thirdPart" */ '../components/page/ThirdPart.vue'),
                    meta: { title: '第三方引用' }
                },
                {
                    path: '/person',
                    component: () => import(/* webpackChunkName: "person" */ '../components/page/Person.vue'),
                    meta: { title: '用户信息' }
                },
                {
                    path: '/feedback',
                    component: () => import(/* webpackChunkName: "feedpack" */ '../components/page/Feedback.vue'),
                    meta: { title: '建议、意见、问题反馈' }
                },
                {
                    path: '/helpTips',
                    component: () => import(/* webpackChunkName: "helpTips" */ '../components/page/HelpTips.vue'),
                    meta: { title: '提示与帮助' }
                },
                {
                    path: '/help',
                    component: () => import(/* webpackChunkName: "help" */ '../components/page/Help.vue'),
                    meta: { title: '提示与帮助' }
                },
                
                
            ]
        },
        {
            path: '*',
            redirect: '/404'
        }


        
    ]

    
});
