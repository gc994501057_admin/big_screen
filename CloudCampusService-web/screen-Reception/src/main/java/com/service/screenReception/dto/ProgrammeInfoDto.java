package com.service.screenReception.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 方案信息表(ProgrammeInfo)实体类
 *
 * @author makejava
 * @since 2021-05-18 19:56:24
 */
public class ProgrammeInfoDto implements Serializable {
    private static final long serialVersionUID = -21051524092232774L;
    /**
     * 是否当前 0--false 1--true
     */
    @ApiModelProperty("是否当前 0--false 1--true")
    private Boolean status;
    /**
     * 主键id
     */
    @ApiModelProperty("方案表id")
    private String id;
    /**
     * 方案名称
     */
    @ApiModelProperty("方案名称")
    private String programmeName;
    /**
     * 是否条幅 0--false 1--true
     */
    @ApiModelProperty("是否条幅 0--false 1--true")
    private Boolean isBanner;
    /**
     * 大屏条幅标题
     */
    @ApiModelProperty("大屏条幅标题")
    private String name;
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private String userId;

    /**
     * logo路径
     */
    @ApiModelProperty(" logo路径")
    private String fileUrl;
    /**
     * 方案布局信息
     */
    @ApiModelProperty("方案布局信息")
    private List<ProgrammeInfoViewDto> programmeInfoViewList;
    private Boolean type;
    @ApiModelProperty("方案类型")
    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }
    @ApiModelProperty("预览图片")
    private String programmeImg;

    public String getProgrammeImg() {
        return programmeImg;
    }

    public void setProgrammeImg(String programmeImg) {
        this.programmeImg = programmeImg;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProgrammeName() {
        return programmeName;
    }

    public void setProgrammeName(String programmeName) {
        this.programmeName = programmeName;
    }

    public Boolean getIsBanner() {
        return isBanner;
    }

    public void setIsBanner(Boolean isBanner) {
        this.isBanner = isBanner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public List<ProgrammeInfoViewDto> getProgrammeInfoViewList() {
        return programmeInfoViewList;
    }

    public void setProgrammeInfoViewList(List<ProgrammeInfoViewDto> programmeInfoViewList) {
        this.programmeInfoViewList = programmeInfoViewList;
    }
}
