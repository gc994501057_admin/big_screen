package com.service.screenWeb.dto;


import com.service.screenWeb.entity.ScreenUser;
import lombok.Data;
import org.springframework.http.HttpHeaders;

import java.util.List;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-04-21 19:23
 **/
@Data
public class ApiAccountDto {
    private HttpHeaders requestHeaders;
    private List<SiteDto> sites;
    private ScreenUser screenUser;
}
