package com.service.config.aspect;

import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @author
 * @version 1.0
 * @date 2020/6/18 14:58
 */
@Configuration
public class Configurations {
    @Bean
    public Retryer feintRetry(){
        return new Retryer.Default(100,SECONDS.toMillis(1),5);
    }
}
