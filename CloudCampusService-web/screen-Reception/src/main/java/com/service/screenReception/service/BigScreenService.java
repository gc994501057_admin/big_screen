package com.service.screenReception.service;

import com.service.screenReception.dto.TerminalUserDto;
import com.service.screenReception.dto.TopNSsidTrafficDto;

import java.util.List;
import java.util.Map;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-04-01 15:25
 **/
public interface BigScreenService {

    Map queryDeviceCounts(String user_id);

    List queryDeviceType(String user_id);

    Map queryOnlineUserCount(String user_idn);

    Map querySiteTrafficByDay(String user_id);

    List querySsidTraffic(String user_id);

    Map queryTerminalUser(String userId,String sign) throws Exception;

    List queryDeviceStatus(String userId);

    List queryByAccessType(String userId);

    Map querySiteTrafficByMonth(String userId);

    Map querySiteTrafficByYear(String userId);

    Map queryHealth(String userId,String sign);

//    Map querySiteTrafficByHistory(String userId);
}
