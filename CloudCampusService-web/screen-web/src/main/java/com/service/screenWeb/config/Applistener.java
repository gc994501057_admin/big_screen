package com.service.screenWeb.config;


import com.service.config.utils.JsonXMLUtils;
import com.service.config.utils.ModelMapperUtil;
import com.service.config.utils.RedisUtil;
import com.service.screenWeb.dao.ProgrammeInfoDao;
import com.service.screenWeb.dao.ProgrammeInfoViewDao;
import com.service.screenWeb.dto.ProgrammeInfoViewDto;
import com.service.screenWeb.entity.*;
import com.service.screenWeb.service.AssemblyConfigureService;
import com.service.screenWeb.service.AssemblyInfoService;
import com.service.screenWeb.service.LayoutService;
import com.service.screenWeb.service.ProgrammeInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.service.config.constant.Constant.*;

@Service
@Scope("singleton")
@Component
public class Applistener implements ApplicationRunner {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    RedisUtil redisUtil;
    @Resource
    private ProgrammeInfoDao programmeInfoDao;
    @Resource
    private ProgrammeInfoViewDao programmeInfoViewDao;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        while (true) {
            while (true) { // 默认模板信息
                if (!redisUtil.exist(USER_PROGRAMME_INFO)) {
                    try {
                        List<ProgrammeInfoView> programmeInfoViewList =  programmeInfoViewDao.queryDefault();
                        //根据方案id进行分组统计，并取其中一个塞进缓存和数据库
                        List<ProgrammeInfo> programmeInfoList = new ArrayList<>();
                        Map<String, List<ProgrammeInfoView>> collect1 = programmeInfoViewList.stream().collect(Collectors.groupingBy(ProgrammeInfoView::getId));//根据mac分组
                        collect1.entrySet().forEach(stringListEntry -> {
                            List<ProgrammeInfoView> value = stringListEntry.getValue();
                            List<ProgrammeInfoViewDto> programmeInfoViewDtos = ModelMapperUtil.strictMapList(value, ProgrammeInfoViewDto.class);
                            try {
                                value.get(0).setLayoutInfo(JsonXMLUtils.obj2json(programmeInfoViewDtos));
                                programmeInfoList.add(ModelMapperUtil.strictMap(value.get(0),ProgrammeInfo.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        });
                        programmeInfoDao.insertOrUpdateBatch(programmeInfoList);
                        programmeInfoList.stream().forEach(programmeInfoView -> {
                            try {
                                redisUtil.leftPush(USER_PROGRAMME_INFO, JsonXMLUtils.obj2json(programmeInfoView));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    break;
                }
            }
        }
    }

}
