package com.service.login.controller;



import com.service.config.utils.*;

import com.service.login.dto.UserExtendDto;
import com.service.login.entity.UserExtend;

import com.service.login.entity.UserInfo;
import com.service.login.service.UserExtendService;
import com.service.login.service.UserInfoService;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.tomcat.util.http.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static com.service.config.constant.Constant.*;


/**
 * 用户信息扩展(UserExtend)表控制层
 *
 * @author makejava
 * @since 2021-03-30 11:39:11
 */
@CrossOrigin
@RestController
@RequestMapping("userExtend")
@Api(value = "dmeo")
public class UserExtendController {
    /**
     * 服务对象
     */
    @Resource
    private UserExtendService userExtendService;
    @Resource
    private RedisUtil redisUtil;
    @Autowired
    private UserInfoService userInfoService;
    /**
     * 用户完善信息
     *
     * @param
     * @return
     */
    @PostMapping(value = "perfectUserInfo",produces="application/json")
    @ApiOperation(value = "用户完善信息")
    @ResponseBody
    public Result perfectUserInfo(@RequestHeader(value = "cloud-Auth-Token") String cloudAuthToken,
                                  @RequestBody UserExtendDto userExtendDto ,
                                  @RequestParam("serviceId") String serviceId) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (!RegexUtil.isMobileSimple(userExtendDto.getTelephone())) {
                return Result.failure("手机号格式错误，请重新校验");
            }
            if (redisUtil.exist(cloudAuthToken+userId)) {
                userExtendDto.setUserId(userId);
                if (serviceId.equals("2")) {
                    userExtendService.insertMspUserExtend(userExtendDto);
                }else {
                    userExtendService.insertUserExtend(userExtendDto);
                }
                return Result.ok();
            }else {
                return Result.failure(USERNAME_Off_SITE,usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN,"用户未登录");
    }
    /**
     * 获取用户信息
     *
     * @param
     * @return
     */
    @GetMapping(value = "getUserInfo",produces="application/json")
    @ApiOperation(value = "获取用户信息")
    @ResponseBody
    public Result getUserInfo(@RequestHeader(value = "cloud-Auth-Token") String cloudAuthToken,
                                  @RequestParam("serviceId") String serviceId) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken+userId)) {
                if (serviceId.equals("2")) {
                    UserExtendDto userExtendDto  = userExtendService.queryByMspUserId(userId);
                    return Result.ok().setData(userExtendDto);
                }else {
                    UserExtendDto userExtendDto = userExtendService.queryByUserId(userId);
                    return Result.ok().setData(userExtendDto);
                }

            }else {
                return Result.failure(USERNAME_Off_SITE,usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN,"用户未登录");
    }
}
