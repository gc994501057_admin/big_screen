package com.service.login.dto;

import com.service.login.entity.IndustryInfo;

import java.io.Serializable;
import java.util.List;

/**
 * L2行业信息表(IndustryInfo)实体类
 *
 * @author makejava
 * @since 2021-05-09 11:34:24
 */
public class IndustryInfoDto implements Serializable {
    private static final long serialVersionUID = 587009126669784577L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 行业名称
     */
    private String industryName;
    /**
     * 行业代码
     */
    private String industryNo;
    /**
     * 行业范围简要说明
     */
    private String industryDesc;
    /**
     * 上级行业id
     */
    private String industryId;
    private List<IndustryInfo> industryInfoList;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public String getIndustryNo() {
        return industryNo;
    }

    public void setIndustryNo(String industryNo) {
        this.industryNo = industryNo;
    }

    public String getIndustryDesc() {
        return industryDesc;
    }

    public void setIndustryDesc(String industryDesc) {
        this.industryDesc = industryDesc;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public List<IndustryInfo> getIndustryInfoList() {
        return industryInfoList;
    }

    public void setIndustryInfoList(List<IndustryInfo> industryInfoList) {
        this.industryInfoList = industryInfoList;
    }
}
