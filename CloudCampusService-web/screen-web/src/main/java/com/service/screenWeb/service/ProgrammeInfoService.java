package com.service.screenWeb.service;

import com.service.screenWeb.dto.ProgrammeInfoDto;
import com.service.screenWeb.entity.ProgrammeInfo;

import java.util.HashMap;
import java.util.List;

/**
 * 方案信息表(ProgrammeInfo)表服务接口
 *
 * @author makejava
 * @since 2021-05-18 19:56:24
 */
public interface ProgrammeInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ProgrammeInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ProgrammeInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param programmeInfo 实例对象
     * @return 实例对象
     */
    ProgrammeInfo insert(ProgrammeInfo programmeInfo) throws Exception;

    /**
     * 修改数据
     *
     * @param programmeInfo 实例对象
     * @return 实例对象
     */
    ProgrammeInfo update(ProgrammeInfo programmeInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    HashMap deleteById(String id);

    List<ProgrammeInfoDto> queryAll(ProgrammeInfo programmeInfo, String userId) throws Exception;

    List<ProgrammeInfoDto> queryDefault();

    HashMap updateStatus(ProgrammeInfo strictMap);
}
