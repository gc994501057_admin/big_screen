package com.service.screenReception.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.service.config.utils.ModelMapperUtil;

import com.service.screenReception.dao.AssemblyDao;
import com.service.screenReception.dao.ScreenUserDao;
import com.service.screenReception.dao.UserServiceInfoDao;
import com.service.screenReception.dto.UserServiceInfoDto;
import com.service.screenReception.entity.Assembly;
import com.service.screenReception.entity.ScreenUser;
import com.service.screenReception.entity.UserServiceInfo;
import com.service.screenReception.service.UserServiceInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户与服务关联表(UserServiceInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-04-22 09:50:42
 */
@Service("userServiceInfoService")
public class UserServiceInfoServiceImpl implements UserServiceInfoService {
    @Resource
    private UserServiceInfoDao userServiceInfoDao;
    @Resource
    private AssemblyDao assemblyDao;
    @Resource
    private ScreenUserDao screenUserDao;
    /**
     * 通过ID查询单条数据
     *
     * @param id
     * @return 实例对象
     */


    @Override
    public UserServiceInfo queryById(String id) {
        return userServiceInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<UserServiceInfo> queryAllByLimit(int offset, int limit) {
        return this.userServiceInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param userServiceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public UserServiceInfo insert(UserServiceInfo userServiceInfo) {
        this.userServiceInfoDao.insert(userServiceInfo);
        return userServiceInfo;
    }

    /**
     * 修改数据
     *
     * @param userServiceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public UserServiceInfo update(UserServiceInfo userServiceInfo) {
        this.userServiceInfoDao.update(userServiceInfo);
        return this.queryById(userServiceInfo.getServiceId());
    }

    @Override
    public boolean deleteById(String id) {
        return false;
    }

    @Override
    public UserServiceInfoDto queryAll(String serviceId, String userId) throws Exception {
        ScreenUser screenUser = new ScreenUser();
        screenUser.setUserId(userId);
        UserServiceInfo userServiceInfo = userServiceInfoDao.queryById(serviceId);
        UserServiceInfoDto userServiceInfoDto = ModelMapperUtil.strictMap(userServiceInfo, UserServiceInfoDto.class);
        if (userServiceInfo.getAssemblyInfo() != null && !userServiceInfo.getAssemblyInfo().equals("")) {
            userServiceInfoDto.setAssemblyInfo(JSONArray.parseArray(userServiceInfo.getAssemblyInfo(),Assembly.class));
            return userServiceInfoDto;
        }else {
            List<Assembly> assemblies = assemblyDao.queryAll(new Assembly());
            userServiceInfoDto.setAssemblyInfo(assemblies);
            return userServiceInfoDto;
        }
    }
}
