package com.service.screenReception.dto;

import com.service.screenReception.entity.ScreenUser;
import lombok.Data;
import org.springframework.http.HttpHeaders;

import java.util.List;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-04-21 19:23
 **/
@Data
public class ApiAccountDto {
    private HttpHeaders requestHeaders;
    private List<SiteDto> sites;
    private ScreenUser screenUser;
}
