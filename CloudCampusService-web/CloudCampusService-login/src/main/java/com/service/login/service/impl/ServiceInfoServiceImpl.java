package com.service.login.service.impl;

import com.service.login.entity.ServiceInfo;
import com.service.login.dao.ServiceInfoDao;
import com.service.login.service.ServiceInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ServiceInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-03-31 14:37:11
 */
@Service("serviceInfoService")
public class ServiceInfoServiceImpl implements ServiceInfoService {
    @Resource
    private ServiceInfoDao serviceInfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ServiceInfo queryById(String id) {
        return this.serviceInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ServiceInfo> queryAllByLimit(int offset, int limit) {
        return this.serviceInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param serviceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public ServiceInfo insert(ServiceInfo serviceInfo) {
        this.serviceInfoDao.insert(serviceInfo);
        return serviceInfo;
    }

    /**
     * 修改数据
     *
     * @param serviceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public ServiceInfo update(ServiceInfo serviceInfo) {
        this.serviceInfoDao.update(serviceInfo);
        return this.queryById(serviceInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.serviceInfoDao.deleteById(id) > 0;
    }

    @Override
    public List<ServiceInfo> queryAll() {
        return serviceInfoDao.queryAll(new ServiceInfo());
    }
}
