package com.service.login.dto;

import com.service.login.entity.ServiceInfo;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户与服务关联表(UserServiceInfo)实体类
 *
 * @author makejava
 * @since 2021-03-31 14:38:20
 */
@Data
public class UserServiceInfoDto implements Serializable {

    /**
     * 云服务id
     */
    private String id;
    /**
     * 云服务id
     */
    private String serviceId;
    /**
     * 用户id
     */
    private String userId;
    private ServiceInfo serviceInfo;


}
