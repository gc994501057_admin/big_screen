package com.service.config.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {


    /**
     * 时间戳转换成日期格式字符串
     *
     * @param seconds 精确到秒的字符串
     * @param format
     * @return
     */
    public static String timeStamp2Date(String seconds, String format) {
        if (seconds == null || seconds.isEmpty() || seconds.equals("null")) {
            return "";
        }
        if (format == null || format.isEmpty()) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(Long.valueOf(seconds + "000")));
    }

    public static String timeStamp3Date(Integer seconds, String format) {

        if (format == null || format.equals("")) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        Long lo = (long) seconds * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(lo));
    }

    public static String timeStamp4Date(String seconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        Date date1 = new Date();
        try {
            date1 = sdf.parse(timeStamp2Date(seconds, ""));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date1);
    }

    /**
     * 日期格式字符串转换成时间戳
     *
     * @param date_str 字符串日期
     * @param format   如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String date2TimeStamp(String date_str, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(date_str).getTime() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 取得当前时间戳（精确到秒）
     *
     * @return
     */
    public static String timeStamp() {
        long time = System.currentTimeMillis();
        String t = String.valueOf(time / 1000);
        return t;
    }

    /**
     * 获得当天零时零分零秒
     *
     * @return
     */
    public static Date initDateByDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取当月零时零分零秒
     *
     * @return
     */
    public static Date initDateByMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取当年零时零分零秒
     *
     * @return
     */
    public static Date initDateByYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取零时零分零秒
     *
     * @return
     */
    public static Date initDateByHistory() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    public static long getDatePoor(Date endDate, Date nowDate) {

        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;

        return min;
    }

    public static long getDateDay(Date endDate, Date nowDate) {

        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;

        return day;
    }
    /**
     * 计算时间差
     * @return
     */
    public static String getDatePoor(Integer diff) {

        long nd = 24 * 60 * 60;
        long nh = 60 * 60;
        long nm = 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;

        return day+"天"+hour+"小时"+min +"分钟";
    }
    /**
     * @param fromDate3
     * @param toDate3
     * @return
     * @throws ParseException
     */
    public static long getDateMin(String fromDate3, String toDate3) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fromDate = sdf.parse(fromDate3);
        Date toDate = sdf.parse(toDate3);
        long from3 = fromDate.getTime();
        long to3 = toDate.getTime();
        int minutes = (int) ((to3 - from3) / (1000 * 60));

        return minutes;
    }

    /**
     * 计算时间的差值：秒级
     * @param fromDate3
     * @return
     * @throws ParseException
     */
    public static long getHMS(String fromDate3, String toDate3) throws ParseException {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fromDate = sdf.parse(fromDate3);
        long ns = 1000;
        Date toDate = sdf.parse(toDate3);
        long from3 = fromDate.getTime();
        long to3 = toDate.getTime();
        int sec = (int) ((to3 - from3) / (ns));

        return sec;
    }

    /**
     * 日期转星期
     *
     * @param datetime
     * @return
     */
    public static String dateToWeek(String datetime) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance(); // 获得一个日历
        Date date = null;
        try {
            date = f.parse(datetime);
            cal.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1; // 指示一个星期中的某天。
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * java 获取月周
     */
    public static Map<String, String> getWeek(String date) {

        Integer week = null;
        Map<String, String> map = new HashMap<String, String>();

        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //设置时间格式
            Calendar cal = Calendar.getInstance();
            Date time = sdf.parse(date);
            cal.setTime(time);

            //判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
            int dayWeek = cal.get(Calendar.DAY_OF_WEEK);//获得当前日期是一个星期的第几天
            if (1 == dayWeek) {
                cal.add(Calendar.DAY_OF_MONTH, -1);
            }
            cal.setFirstDayOfWeek(Calendar.MONDAY);//设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
            int day = cal.get(Calendar.DAY_OF_WEEK);//获得当前日期是一个星期的第几天
            cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);//根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
            //System.out.println("所在周星期一的日期："+sdf.format(cal.getTime()));

            String weekFirstDay = sdf.format(cal.getTime()).toString();
            cal.add(Calendar.DATE, cal.getFirstDayOfWeek() + 1);
            String weekTuesdayDay = sdf.format(cal.getFirstDayOfWeek() + 1).toString();//周二
            String weekWednesdayDay = sdf.format(cal.getFirstDayOfWeek() + 1).toString();//周三
            String weekThursdayDay = sdf.format(cal.getFirstDayOfWeek() + 1).toString();//周四
            String weekFridayDay = sdf.format(cal.getFirstDayOfWeek() + 1).toString();//周五
            String weekSaturdayDay = sdf.format(cal.getFirstDayOfWeek() + 1).toString();//周六
            map.put("weeekTuesdayDay", weekTuesdayDay);
            map.put("weeekFirstDay", weekFirstDay);
            String date1 = sdf.format(cal.getTime());
            Date time1 = sdf.parse(date1);
            calendar.setTime(time1);
            int week1 = calendar.get(Calendar.WEEK_OF_YEAR);

            // System.out.println(cal.getFirstDayOfWeek()+"-"+day+"+6="+(cal.getFirstDayOfWeek()-day+6));

            cal.add(Calendar.DATE, 6);
            //System.out.println("所在周星期日的日期："+sdf.format(cal.getTime()));
            String weeekEndDay = sdf.format(cal.getTime());
            map.put("weeekEndDay", weeekEndDay);

            String date2 = sdf.format(cal.getTime());
            Date time2 = sdf.parse(date2);
            calendar.setTime(time2);
            int week2 = calendar.get(Calendar.WEEK_OF_YEAR);
            week = (week1 >= week2 ? week2 : week1);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }


        return map;
    }
    /**
     * 获取当天的23:59:59
     * @return
     */
    public static Date dateFour(String date){
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Date date1= null;
        try {
            date1 = new Date(simpleDateFormat.parse(date).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date1!= null){
            //C获取当天23点59分59秒Date
            Calendar cal = new GregorianCalendar();
            cal.setTime(date1);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 59);
            return cal.getTime();
        }else {
            return null;
        }
    }
    /**
     * 获取当天的23:59:59
     * @return
     */
    public static Date date(String date){
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Date date1= null;
        try {
            date1 = new Date(simpleDateFormat.parse(date).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date1!= null){
            //C获取当天23点59分59秒Date
            Calendar cal = new GregorianCalendar();
            cal.setTime(date1);
            cal.set(Calendar.HOUR_OF_DAY, 00);
            cal.set(Calendar.MINUTE, 00);
            cal.set(Calendar.SECOND, 00);
            cal.set(Calendar.MILLISECOND, 00);
            datetoString(cal.getTime());
            return cal.getTime();
        }else {
            return null;
        }
    }
    public static String datetoString(Date date){
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }


    /**
     * 获取指定某一天的开始时间戳
     *
     * @param timeStamp 毫秒级时间戳
     * @return
     */
    public static Long getDailyStartTime(String timeStamp) {
        String timeZone = "GMT+8:00";
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone(timeZone));
        calendar.setTimeInMillis(Long.parseLong(timeStamp));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }


    public static void main(String[] args) throws ParseException {
        String timeStamp = timeStamp();
        System.out.println("timeStamp=" + timeStamp); //运行输出:timeStamp=1470278082
        System.out.println(System.currentTimeMillis());//运行输出:1470278082980
        //该方法的作用是返回当前的计算机时间，时间的表达格式为当前计算机时间和GMT时间(格林威治时间)1970年1月1号0时0分0秒所差的毫秒数
        Map week = getWeek("2020-04-10");
        System.out.println("s" + week.get("weekTuesdayDay"));
        String date = timeStamp2Date(timeStamp, "yyyy-MM-dd HH:mm:ss");
        System.out.println("date=" + date);//运行输出:date=2016-08-04 10:34:42
        String timeStamp2 = date2TimeStamp(date, "yyyy-MM-dd HH:mm:ss");
        System.out.println(dateToWeek("2020-4-13"));
        System.out.println("de=" + timeStamp4Date(timeStamp));
        System.out.println(timeStamp2Date("1589784322",""));System.out.println(timeStamp2Date("1589784363",""));
        System.out.println("de21=="+getHMS(timeStamp2Date("1589784322",""),timeStamp2Date("1589784363","")));
        System.out.println(timeStamp2);  //运行输出:1470278082
        System.out.println(date("2020-05-25"));
        System.out.println(date("2020-05-26"));
    }

}

