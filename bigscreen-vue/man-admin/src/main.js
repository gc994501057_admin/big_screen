import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
import { messages } from './components/common/i18n';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
import './assets/css/icon.css';
import './assets/iconfont/iconfont.css';
import './components/common/directives';
import 'babel-polyfill';
import less from 'less'
Vue.use(less)
// 炫酷的粒子动效
import  VueParticles  from   'vue-particles';
Vue.use(VueParticles);
Vue.config.productionTip = false;
Vue.use(VueI18n);
Vue.use(ElementUI, {
    size: 'small'
});
const i18n = new VueI18n({
    locale: 'zh',
    messages
});



import axios from 'axios'
Vue.prototype.$axios = axios
axios.defaults.baseURL = '/flag' //地址替换

// axios.interceptors.request.use(function (config) {
//     // Do something before request is sent
//     let token = window.localStorage.getItem("ms_token")
//     if (token && config.method == "delete") {
//         config.headers['X-cloud-Token'] = token;    //将token放到请求头发送给服务器
//         return config;
//         //这里经常搭配token使用，将token值配置到tokenkey中，将tokenkey放在请求头中
//         // config.headers['accessToken'] = Token;
//     }
//     // console.log(config)
//     return config;
// }, function (error) {
//     // Do something with request error
//     return Promise.reject(error);
// });



//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | 大屏管理员运维系统`;
    const role = localStorage.getItem('manage_token');
    if (!role && to.path !== '/login') {
        next('/login');
    } else if (to.meta.permission) {
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        role === 'admin' ? next() : next('/403');
    } else {
        // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
        if (navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor') {
            Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
                confirmButtonText: '确定'
            });
        } else {
            next();
        }
    }
});

new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app');
