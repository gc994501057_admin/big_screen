# bigscreen

#### 介绍
大屏，基于华为云管理网络的北向API,构建的网络大屏应用，是网络的数据可视化平台，可协助网络运维和监控。具有多用户saas化能力，可以集成第三方内容，也可以被第三方集成。

#### 软件架构
微服务架构，使用springCloud的原生组件（eureka，gateway（Zuul 网关），fegin，hystrix，rabbion），前后端完全分离 (互不依赖 开发效率高)

#### 使用说明
项目结构：  
	wifilogin  
	    &emsp;&emsp;bigscreen-vue &emsp;前端页面  
		&emsp;&emsp;CloudCampusService-common &emsp;公共包  
		&emsp;&emsp;CloudCampusService-Eureka &emsp;注册中心  
		&emsp;&emsp;CloudCampusService-gateway &emsp;网关  
		&emsp;&emsp;CloudCampusService-web &emsp;服务类  
			&emsp;&emsp;CloudCampusService-uniqueSequence &emsp;唯一id服务  
			&emsp;&emsp;CloudCampusService-login &emsp;登录 
			&emsp;&emsp;screen-web &emsp;大屏后台 
			&emsp;&emsp;screen-Reception &emsp;大屏前台 
		&emsp;&emsp;sql &emsp;数据库结构  


环境配置  
    &emsp;&emsp;node.js  
    &emsp;&emsp;vue 2.6.10  
    &emsp;&emsp;vue-cli-service 3.9.0  
    &emsp;&emsp;windows server 服务器  
    &emsp;&emsp;Nginx 1.16.1+  
    &emsp;&emsp;java8+  
    &emsp;&emsp;MySQL 8.0.20+  
    &emsp;&emsp;redis 5.0.9+  
# 版权须知
&emsp;&emsp;按照Apache 2.0开源协议使用即可。本demo源代码仅供参考，我们不对本demo源代码承担任何责任。
