package com.service.screenWeb.service;

import com.service.screenWeb.entity.ReplyInfo;

import java.util.List;

/**
 * 回复表(ReplyInfo)表服务接口
 *
 * @author makejava
 * @since 2021-05-07 15:00:53
 */
public interface ReplyInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ReplyInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ReplyInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param replyInfo 实例对象
     * @return 实例对象
     */
    ReplyInfo insert(ReplyInfo replyInfo);

    /**
     * 修改数据
     *
     * @param replyInfo 实例对象
     * @return 实例对象
     */
    ReplyInfo update(ReplyInfo replyInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}
