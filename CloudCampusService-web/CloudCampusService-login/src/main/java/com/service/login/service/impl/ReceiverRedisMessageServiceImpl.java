package com.service.login.service.impl;

/**
 * @author
 * @version 1.0
 * @date 2020/11/11 11:11
 */


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.service.config.utils.JsonXMLUtils;
import com.service.login.bean.MailBean;
import com.service.login.config.MailUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

/**
 * Created by GK_LZS  ON 2019/4/5
 * 注入消息接受类
 *
 * @author 林泽思
 */

@Service
public class ReceiverRedisMessageServiceImpl {
    private static final Logger log = LoggerFactory.getLogger(ReceiverRedisMessageServiceImpl.class);

    @Autowired
    MailUtil mailUtil;





    /**
     * 队列消息接收方法
     *
     * @param jsonMsg
     */
    public void receiveMessage(String jsonMsg) throws Exception {
        log.info("[ begin consumer REDIS  sendEmail data ...]");
        //序列化对象（特别注意：发布的时候需要设置序列化；订阅方也需要设置序列化）
        MailBean mailBean = JsonXMLUtils.json2obj(jsonMsg, MailBean.class);
        try {
            mailUtil.sendSimpleMail(mailBean);
            log.info("[consumer REDIS  sendEmail success ]");
        } catch (Exception e) {
            log.error("[consumer REDIS  sendEmail error ，errorMsg:{}]", e.getMessage());
        }
    }
//    /**
//     * 队列消息接收方法
//     *
//     * @param jsonMsg
//     */
//    public void receiveMessage2(String jsonMsg) throws UnsupportedEncodingException {
//        log.info("[ begin consumer REDIS  crateReport data ...]");
//        //序列化对象（特别注意：发布的时候需要设置序列化；订阅方也需要设置序列化）
//        Jackson2JsonRedisSerializer<ScheduledTimeDto> seria = new Jackson2JsonRedisSerializer<>(ScheduledTimeDto.class);
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
//        seria.setObjectMapper(objectMapper);
//        ScheduledTimeDto scheduledTimeDto = seria.deserialize(jsonMsg.getBytes("utf-8"));
//        try {
//            if (scheduledTimeDto != null) {
//                scheduledTimeService.sendMail(scheduledTimeDto.getReport_user_id(),scheduledTimeDto.getMode_id(),scheduledTimeDto.getMspMail(),scheduledTimeDto.getMsp_id());
//                log.info("[consumer REDIS  crateReport success ]");
//            }
//        } catch (Exception e) {
//            log.error("[consumer REDIS  crateReport error ，errorMsg:{}]", e.getMessage());
//        }
//    }
//    /**
//     * 队列消息接收方法
//     *
//     * @param jsonMsg
//     */
//    public void receiveMessage3(String jsonMsg) throws UnsupportedEncodingException {
//        log.info("[ begin consumer REDIS  checkTenants data ...]");
//        //序列化对象（特别注意：发布的时候需要设置序列化；订阅方也需要设置序列化）
////        Jackson2JsonRedisSerializer<ReportUser> seria = new Jackson2JsonRedisSerializer<>(ReportUser.class);
////        ObjectMapper objectMapper = new ObjectMapper();
////        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
////        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
////        seria.setObjectMapper(objectMapper);
//        //List<ReportUser> scheduledTimeDto = (List<ReportUser>) seria.deserialize(jsonMsg.getBytes("utf-8"));
//        try {
//
//            String jsonMsg1 = jsonMsg.substring(1,jsonMsg.length()-1).replaceAll("\\\\","");
//            JSONArray jsonArray = JSONArray.parseArray(jsonMsg1);
//            List<ReportUser> list = jsonArray.toJavaList(ReportUser.class);
//            if (list != null) {
//                excelService.checkTenants(list);
//                log.info("[consumer REDIS  checkTenants success ]");
//            }
//        } catch (Exception e) {
//            log.error("[consumer REDIS  checkTenants error ，errorMsg:{}]", e.getMessage());
//        }
//    }
}
