package com.service.screenWeb.service;

import com.service.screenWeb.entity.ProgrammeInfoView;

import java.util.List;

/**
 * (ProgrammeInfoView)表服务接口
 *
 * @author makejava
 * @since 2021-05-19 09:46:53
 */
public interface ProgrammeInfoViewService {

    /**
     * 通过ID查询单条数据
     *
     * @param id
     * @return 实例对象
     */
    ProgrammeInfoView queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ProgrammeInfoView> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param programmeInfoView 实例对象
     * @return 实例对象
     */
    ProgrammeInfoView insert(ProgrammeInfoView programmeInfoView);

    /**
     * 修改数据
     *
     * @param programmeInfoView 实例对象
     * @return 实例对象
     */
    ProgrammeInfoView update(ProgrammeInfoView programmeInfoView);

    /**
     * 通过主键删除数据
     *
     * @param id id
     * @return 是否成功
     */
    boolean deleteById(String id);

}
