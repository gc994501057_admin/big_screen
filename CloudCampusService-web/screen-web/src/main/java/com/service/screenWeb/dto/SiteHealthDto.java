package com.service.screenWeb.dto;

import lombok.Data;

import java.math.BigInteger;

/**
 * @program: CloudCampusService
 * @description:站点健康度查询
 * @author: rui
 * @create: 2021-06-09 16:17
 **/
@Data
public class SiteHealthDto {
    private String siteId;
    private String siteName;
    private Integer deviceHealth;
    private Integer radioHealth;
    private Integer siteHealth;

    private BigInteger timestamp;
    private Integer deviceHealthPoint;
}
