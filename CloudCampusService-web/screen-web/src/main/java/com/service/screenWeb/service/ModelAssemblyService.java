package com.service.screenWeb.service;

import com.service.screenWeb.entity.ModelAssembly;

import java.util.List;

/**
 * (ModelAssembly)表服务接口
 *
 * @author makejava
 * @since 2021-04-30 09:39:22
 */
public interface ModelAssemblyService {

    /**
     * 通过ID查询单条数据
     *
     * @param modelId 主键
     * @return 实例对象
     */
    ModelAssembly queryById(String modelId);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ModelAssembly> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param modelAssembly 实例对象
     * @return 实例对象
     */
    ModelAssembly insert(ModelAssembly modelAssembly);

    /**
     * 修改数据
     *
     * @param modelAssembly 实例对象
     * @return 实例对象
     */
    ModelAssembly update(ModelAssembly modelAssembly);

    /**
     * 通过主键删除数据
     *
     * @param modelId 主键
     * @return 是否成功
     */
    boolean deleteById(String modelId);

}
