package com.service.login.annotation;

/**
 * @author
 * @version 1.0
 * @date 2021/4/28 14:28
 */

import com.service.login.enums.DataSourceType;

import java.lang.annotation.*;

/**
 * 数据源自定义注解
 */

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {

    /**
     * 没有设置数据源名称,默认取default名称数据源
     */

    DataSourceType value();

}
