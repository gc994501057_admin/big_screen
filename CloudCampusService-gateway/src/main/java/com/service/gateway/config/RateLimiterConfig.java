//package com.service.gateway.config;
//
//import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import reactor.core.publisher.Mono;
//
///**
// * @author
// * @version 1.0
// * @date 2021/4/6 17:43
// */
//@Configuration
//public class RateLimiterConfig {
//    @Bean(value = "remoteAddrKeyResolver")
//    public KeyResolver remoteAddrKeyResolver() {
//        return exchange -> Mono.just(exchange.getRequest().getRemoteAddress().getAddress().getHostAddress());
//    }
//
//}
