package com.service.login.service.impl;

import com.service.login.annotation.DataSource;
import com.service.login.entity.UserLoginRecord;
import com.service.login.dao.UserLoginRecordDao;
import com.service.login.enums.DataSourceType;
import com.service.login.service.UserLoginRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户表(UserLoginRecord)表服务实现类
 *
 * @author makejava
 * @since 2021-05-11 11:34:15
 */
@Service("userLoginRecordService")
@Slf4j
public class UserLoginRecordServiceImpl implements UserLoginRecordService {
    @Resource
    private UserLoginRecordDao userLoginRecordDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public UserLoginRecord queryById(String id) {
        return this.userLoginRecordDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<UserLoginRecord> queryAllByLimit(int offset, int limit) {
        return this.userLoginRecordDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param userLoginRecord 实例对象
     * @return 实例对象
     */
    @Override
    public UserLoginRecord insert(UserLoginRecord userLoginRecord) {
        this.userLoginRecordDao.insert(userLoginRecord);
        return userLoginRecord;
    }

    /**
     * 修改数据
     *
     * @param userLoginRecord 实例对象
     * @return 实例对象
     */
    @Override
    public UserLoginRecord update(UserLoginRecord userLoginRecord) {
        this.userLoginRecordDao.update(userLoginRecord);
        return this.queryById(userLoginRecord.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.userLoginRecordDao.deleteById(id) > 0;
    }

    @Override
    @DataSource(DataSourceType.MASTER)
    public void insertBatch(List<UserLoginRecord> userLoginRecordList) {
        userLoginRecordDao.insertBatch(userLoginRecordList);
    }

    @Override
    @DataSource(DataSourceType.MASTER)
    public List<UserLoginRecord> queryAll(UserLoginRecord userLoginRecord) {
        return userLoginRecordDao.queryAllByUserId(userLoginRecord.getUserId());
    }

    @Override
    @DataSource(DataSourceType.SLAVE)
    public List<UserLoginRecord> queryAllMsp(UserLoginRecord userLoginRecord) {
        return userLoginRecordDao.queryAllByUserId(userLoginRecord.getUserId());
    }

    @Override
    @DataSource(DataSourceType.MASTER)
    public void insertOrUpdateBatch(List<UserLoginRecord> userLoginRecordList) {
        try{
            userLoginRecordDao.insertOrUpdateBatch(userLoginRecordList);
        }catch (Exception e) {
            log.info(""+e.getMessage());
        }

    }
}
