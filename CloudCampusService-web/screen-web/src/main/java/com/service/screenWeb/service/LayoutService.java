package com.service.screenWeb.service;

import com.service.screenWeb.entity.Layout;

import java.util.List;

/**
 * 组件布局表(Layout)表服务接口
 *
 * @author makejava
 * @since 2021-05-18 19:56:25
 */
public interface LayoutService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Layout queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Layout> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param layout 实例对象
     * @return 实例对象
     */
    Layout insert(Layout layout);

    /**
     * 修改数据
     *
     * @param layout 实例对象
     * @return 实例对象
     */
    Layout update(Layout layout);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    List<Layout> queryDefault();
}
