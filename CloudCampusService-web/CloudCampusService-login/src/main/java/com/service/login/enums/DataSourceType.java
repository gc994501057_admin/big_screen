package com.service.login.enums;

/**
 * @author
 * @version 1.0
 * @date 2021/4/28 14:30
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER("master"),

    /**
     * 从库
     */


    SLAVE("slave");

    private String value;

    DataSourceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
