package com.service.login.entity;

import java.io.Serializable;

/**
 * (ServiceInfo)实体类
 *
 * @author makejava
 * @since 2021-04-22 09:50:49
 */
public class ServiceInfo implements Serializable {
    private static final long serialVersionUID = 851942008043745457L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 服务名字
     */
    private String serviceName;
    /**
     * 0--已禁用 1--正常
     */
    private Boolean status;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
