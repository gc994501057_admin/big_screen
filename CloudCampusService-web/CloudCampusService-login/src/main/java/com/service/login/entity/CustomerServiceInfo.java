package com.service.login.entity;

import java.io.Serializable;

/**
 * 系统运维人员信息表(CustomerServiceInfo)实体类
 *
 * @author makejava
 * @since 2021-05-08 17:36:15
 */
public class CustomerServiceInfo implements Serializable {
    private static final long serialVersionUID = 802526173710213149L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
