package com.service.login.service;

import com.service.login.entity.ServiceInfo;

import java.util.List;

/**
 * (ServiceInfo)表服务接口
 *
 * @author makejava
 * @since 2021-03-31 14:37:11
 */
public interface ServiceInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ServiceInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ServiceInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param serviceInfo 实例对象
     * @return 实例对象
     */
    ServiceInfo insert(ServiceInfo serviceInfo);

    /**
     * 修改数据
     *
     * @param serviceInfo 实例对象
     * @return 实例对象
     */
    ServiceInfo update(ServiceInfo serviceInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    List<ServiceInfo> queryAll();
}
