package com.service.screenWeb.service.impl;

import com.service.screenWeb.entity.ModelAssembly;
import com.service.screenWeb.dao.ModelAssemblyDao;
import com.service.screenWeb.service.ModelAssemblyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ModelAssembly)表服务实现类
 *
 * @author makejava
 * @since 2021-04-30 09:39:22
 */
@Service("modelAssemblyService")
public class ModelAssemblyServiceImpl implements ModelAssemblyService {
    @Resource
    private ModelAssemblyDao modelAssemblyDao;

    /**
     * 通过ID查询单条数据
     *
     * @param modelId 主键
     * @return 实例对象
     */
    @Override
    public ModelAssembly queryById(String modelId) {
        return this.modelAssemblyDao.queryById(modelId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ModelAssembly> queryAllByLimit(int offset, int limit) {
        return this.modelAssemblyDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param modelAssembly 实例对象
     * @return 实例对象
     */
    @Override
    public ModelAssembly insert(ModelAssembly modelAssembly) {
        this.modelAssemblyDao.insert(modelAssembly);
        return modelAssembly;
    }

    /**
     * 修改数据
     *
     * @param modelAssembly 实例对象
     * @return 实例对象
     */
    @Override
    public ModelAssembly update(ModelAssembly modelAssembly) {
        this.modelAssemblyDao.update(modelAssembly);
        return this.queryById(modelAssembly.getModelId());
    }

    /**
     * 通过主键删除数据
     *
     * @param modelId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String modelId) {
        return this.modelAssemblyDao.deleteById(modelId) > 0;
    }
}
