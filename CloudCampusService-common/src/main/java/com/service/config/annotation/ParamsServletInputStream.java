package com.service.config.annotation;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * @author
 * @version 1.0
 * @date 2021/5/7 9:09
 */
public class ParamsServletInputStream extends ServletInputStream {

    private ByteArrayInputStream bis;

    public ParamsServletInputStream(ByteArrayInputStream bis) {
        this.bis = bis;
    }

    //@Override
    public boolean isFinished() {
        return true;
    }

   // @Override
    public boolean isReady() {
        return true;
    }

    //@Override
    public void setReadListener(ReadListener readListener) {

    }

    @Override
    public int read() throws IOException {
        return bis.read();
    }

}
