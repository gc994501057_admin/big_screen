package com.service.screenReception;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-03-30 16:46
 **/

@CrossOrigin
@SpringBootApplication
@EnableDiscoveryClient
@EnableAsync
@EnableScheduling
@EnableTransactionManagement
@EnableFeignClients
@ComponentScan(basePackages = {"com.service"})
@MapperScan("com.service.screenReception.dao")
public class ScreenReceptionApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScreenReceptionApplication.class, args);
    }

}
