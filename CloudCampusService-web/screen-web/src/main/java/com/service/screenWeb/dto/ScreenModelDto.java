package com.service.screenWeb.dto;

import com.service.screenWeb.entity.Assembly;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 模板（视图）(ScreenModel)实体类
 *
 * @author makejava
 * @since 2021-04-14 09:29:38
 */
@Data
public class ScreenModelDto implements Serializable {
    private static final long serialVersionUID = 748783059353382915L;
    /**
     * id
     */
    private String id;
    /**
     * 模板Id
     */
    private String modelId;
    /**
     * 模板名称
     */
    private String modelName;
    /**
     * 模板描述
     */
    private String modelDesc;
    private List<Assembly> assemblyInfo;
    private String name;

}
