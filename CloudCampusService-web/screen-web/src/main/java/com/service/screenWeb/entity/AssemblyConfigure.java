package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * 组件单独配置表(AssemblyConfigure)实体类
 *
 * @author makejava
 * @since 2021-05-18 19:56:29
 */
public class AssemblyConfigure implements Serializable {
    private static final long serialVersionUID = 892755304514921513L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 组件id
     */
    private String assemblyId;
    /**
     * 组件自定义刷新时间- 单位分钟
     */
    private Integer schedule;
    /**
     * 颜色配置
     */
    private String backgroundColor;
    /**
     * 站点id--根据站点id进行站点拓扑
     */
    private String siteId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 方案表id
     */
    private String programmeId;
    /**
     * url昵称
     */
    private String authUrl;
    /**
     * 接口url
     */
    private String apiUrl;
    /**
     * 认证账号
     */
    private String authUsername;
    /**
     * 认证密码
     */
    private String authPassword;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(String assemblyId) {
        this.assemblyId = assemblyId;
    }

    public Integer getSchedule() {
        return schedule;
    }

    public void setSchedule(Integer schedule) {
        this.schedule = schedule;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProgrammeId() {
        return programmeId;
    }

    public void setProgrammeId(String programmeId) {
        this.programmeId = programmeId;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getAuthUsername() {
        return authUsername;
    }

    public void setAuthUsername(String authUsername) {
        this.authUsername = authUsername;
    }

    public String getAuthPassword() {
        return authPassword;
    }

    public void setAuthPassword(String authPassword) {
        this.authPassword = authPassword;
    }

}
