package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * 方案信息表(ProgrammeInfo)实体类
 *
 * @author makejava
 * @since 2021-05-18 19:56:24
 */
public class ProgrammeInfo implements Serializable {
    private static final long serialVersionUID = -21051524092232774L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 方案名称
     */
    private String programmeName;
    /**
     * 是否条幅 0--false 1--true
     */
    private Boolean isBanner;
    /**
     * 大屏条幅标题
     */
    private String name;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 布局信息（json数据）
     */
    private String layoutInfo;
    /**
     * 是否当前 0--false 1--true
     */
    private Boolean status;
    /**
     * 上传文件id
     */
    private String fileId;
    private Boolean type;

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }
    private String programmeImg;

    public String getProgrammeImg() {
        return programmeImg;
    }

    public void setProgrammeImg(String programmeImg) {
        this.programmeImg = programmeImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProgrammeName() {
        return programmeName;
    }

    public void setProgrammeName(String programmeName) {
        this.programmeName = programmeName;
    }

    public Boolean getIsBanner() {
        return isBanner;
    }

    public void setIsBanner(Boolean isBanner) {
        this.isBanner = isBanner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLayoutInfo() {
        return layoutInfo;
    }

    public void setLayoutInfo(String layoutInfo) {
        this.layoutInfo = layoutInfo;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

}
