package com.service.config.utils;

interface IdGenerator {
	public Long nextId();
}

