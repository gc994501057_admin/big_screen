package com.service.screenWeb.service.impl;


import com.alibaba.fastjson.JSONArray;
import com.service.config.utils.JsonXMLUtils;
import com.service.screenWeb.dao.AssemblyDao;
import com.service.screenWeb.dao.ModelAssemblyDao;
import com.service.screenWeb.dao.ModelInfoDao;
import com.service.screenWeb.dao.ScreenModelDao;
import com.service.screenWeb.dto.ScreenModelDto;
import com.service.screenWeb.dto.ScreenUserDto;
import com.service.screenWeb.entity.Assembly;
import com.service.screenWeb.entity.ModelAssembly;
import com.service.screenWeb.entity.ModelInfo;
import com.service.screenWeb.entity.ScreenModel;
import com.service.screenWeb.service.ScreenModelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 模板（视图）(ScreenModel)表服务实现类
 *
 * @author makejava
 * @since 2021-03-29 15:42:44
 */
@Service("screenModelService")
public class ScreenModelServiceImpl implements ScreenModelService {
    @Resource
    private ScreenModelDao screenModelDao;
    @Resource
    private ModelAssemblyDao modelAssemblyDao;
    @Resource
    private AssemblyDao assemblyDao;
    @Resource
    private ModelInfoDao modelInfoDao;
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ScreenModel queryById(String id) {
        return this.screenModelDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ScreenModel> queryAllByLimit(int offset, int limit) {
        return this.screenModelDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param screenModel 实例对象
     * @return 实例对象
     */
    @Override
    public ScreenModel insert(ScreenModel screenModel) {
        this.screenModelDao.insert(screenModel);
        return screenModel;
    }

    /**
     * 修改数据
     *
     * @param screenModel 实例对象
     * @return 实例对象
     */
    @Override
    public ScreenModel update(ScreenModel screenModel) {
        this.screenModelDao.update(screenModel);
        return this.queryById(screenModel.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.screenModelDao.deleteById(id) > 0;
    }

    @Override
    public List<ScreenModelDto> queryModelList() throws Exception {
        List<ModelAssembly> modelAssemblies = modelAssemblyDao.queryAll(new ModelAssembly());
        List<Assembly> assemblies = assemblyDao.queryAll(new Assembly());
        List<ScreenModel> screenModels = screenModelDao.queryAll(new ScreenModel());
        List<ScreenModelDto> screenUserDtoList = JSONArray.parseArray(JsonXMLUtils.obj2json(screenModels), ScreenModelDto.class);
        screenUserDtoList.stream().forEach(screenModelDto -> {
            List<ModelAssembly> collect = modelAssemblies.stream().filter(modelAssembly -> modelAssembly.getModelId().equals(screenModelDto.getId())).collect(Collectors.toList());
            List<Assembly> assemblyList = new ArrayList<>();
            collect.stream().forEach(modelAssembly -> {
                assemblies.stream().forEach(assembly -> {
                    if (modelAssembly.getAssemblyId().equals(assembly.getId())){
                        assemblyList.add(assembly);
                    };
                });
            });
            screenModelDto.setAssemblyInfo(assemblyList.stream().sorted(Comparator.comparing(Assembly::getId)).collect(Collectors.toList()));
        });
        return screenUserDtoList;
    }
}
