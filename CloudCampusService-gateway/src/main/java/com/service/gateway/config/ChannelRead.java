package com.service.gateway.config;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufHolder;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;

/**
 * @author
 * @version 1.0
 * @date 2021/4/6 16:09
 */
public class ChannelRead extends SimpleChannelInboundHandler {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        try {
            // Do something with msg
        } finally {
            ReferenceCountUtil.release(o);
        }
    }
}
