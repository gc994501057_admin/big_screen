package com.service.screenWeb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.service.config.utils.JsonXMLUtils;
import com.service.config.utils.ModelMapperUtil;
import com.service.screenWeb.dao.AssemblyDao;
import com.service.screenWeb.dao.ModelAssemblyDao;
import com.service.screenWeb.dao.ScreenUserDao;
import com.service.screenWeb.dao.UserServiceInfoDao;
import com.service.screenWeb.dto.AssemblyDto;
import com.service.screenWeb.dto.UserServiceInfoDto;
import com.service.screenWeb.entity.Assembly;
import com.service.screenWeb.entity.ModelAssembly;
import com.service.screenWeb.entity.ScreenUser;
import com.service.screenWeb.entity.UserServiceInfo;

import com.service.screenWeb.service.UserServiceInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.ref.PhantomReference;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户与服务关联表(UserServiceInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-04-22 09:50:42
 */
@Service("userServiceInfoService")
public class UserServiceInfoServiceImpl implements UserServiceInfoService {
    @Resource
    private UserServiceInfoDao userServiceInfoDao;
    @Resource
    private AssemblyDao assemblyDao;
    @Resource
    private ScreenUserDao screenUserDao;
    @Resource
    ModelAssemblyDao modelAssemblyDao;
    /**
     * 通过ID查询单条数据
     *
     * @param id
     * @return 实例对象
     */


    @Override
    public UserServiceInfo queryById(String id) {
        return null;
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<UserServiceInfo> queryAllByLimit(int offset, int limit) {
        return this.userServiceInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param userServiceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public UserServiceInfo insert(UserServiceInfo userServiceInfo) {
        this.userServiceInfoDao.insert(userServiceInfo);
        return userServiceInfo;
    }

    /**
     * 修改数据
     *
     * @param userServiceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public UserServiceInfo update(UserServiceInfo userServiceInfo) {
        UserServiceInfo userServiceInfo1 = userServiceInfoDao.queryById(userServiceInfo.getServiceId(),userServiceInfo.getUserId());
        if (userServiceInfo1 == null) {
            userServiceInfoDao.insert(userServiceInfo) ;
        }else {
            this.userServiceInfoDao.update(userServiceInfo);
        }
        return userServiceInfo;
    }

    @Override
    public boolean deleteById(String id) {
        return false;
    }

    @Override
    public UserServiceInfoDto queryAll(String serviceId, String userId) throws Exception {
        ScreenUser screenUser = new ScreenUser();
        screenUser.setUserId(userId);
        UserServiceInfo userServiceInfo = userServiceInfoDao.queryById(serviceId,userId);
        UserServiceInfoDto userServiceInfoDto = new UserServiceInfoDto();
        if (userServiceInfo != null) {
            userServiceInfoDto = ModelMapperUtil.strictMap(userServiceInfo, UserServiceInfoDto.class);
            if (userServiceInfo.getAssemblyInfo() != null && !userServiceInfo.getAssemblyInfo().equals("")) {
                userServiceInfoDto.setAssemblyInfo(JSONArray.parseArray(userServiceInfo.getAssemblyInfo(), AssemblyDto.class));
                return userServiceInfoDto;
            }else {
                return getUserServiceInfoDto(screenUser, userServiceInfoDto);
            }
        }else {
            return getUserServiceInfoDto(screenUser, userServiceInfoDto);
        }

    }

    private UserServiceInfoDto getUserServiceInfoDto(ScreenUser screenUser, UserServiceInfoDto userServiceInfoDto) {
        List<ModelAssembly> modelAssemblies = new ArrayList<>();
        List<ScreenUser> screenUserList = screenUserDao.queryAll(screenUser);
        if (screenUserList.size() > 0) {
            ModelAssembly modelAssembly = new ModelAssembly();
            modelAssembly.setModelId(screenUserList.get(0).getModelId());
            modelAssemblies = modelAssemblyDao.queryAll(modelAssembly);
        }
        List<Assembly> assemblyList = new ArrayList<>();
        List<Assembly> assemblies = assemblyDao.queryAll(new Assembly());
        modelAssemblies.forEach(modelAssembly -> {
            List<Assembly> collect = assemblies.stream().filter(assembly -> assembly.getId().equals(modelAssembly.getAssemblyId())).collect(Collectors.toList());
            Assembly assembly = collect.get(0);
            assemblyList.add(assembly);
        });
        List<AssemblyDto> assemblyDtoList = ModelMapperUtil.strictMapList(assemblyList, AssemblyDto.class);
        userServiceInfoDto.setAssemblyInfo(assemblyDtoList);
        return userServiceInfoDto;
    }
}
