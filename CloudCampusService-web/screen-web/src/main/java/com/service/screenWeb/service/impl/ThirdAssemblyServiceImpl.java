package com.service.screenWeb.service.impl;

import com.service.screenWeb.entity.ThirdAssembly;
import com.service.screenWeb.dao.ThirdAssemblyDao;
import com.service.screenWeb.service.ThirdAssemblyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 第三方组件(ThirdAssembly)表服务实现类
 *
 * @author makejava
 * @since 2021-05-10 11:36:10
 */
@Service("thirdAssemblyService")
public class ThirdAssemblyServiceImpl implements ThirdAssemblyService {
    @Resource
    private ThirdAssemblyDao thirdAssemblyDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ThirdAssembly queryById(String id) {
        return this.thirdAssemblyDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ThirdAssembly> queryAllByLimit(int offset, int limit) {
        return this.thirdAssemblyDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param thirdAssembly 实例对象
     * @return 实例对象
     */
    @Override
    public ThirdAssembly insert(ThirdAssembly thirdAssembly) {
        this.thirdAssemblyDao.insert(thirdAssembly);
        return thirdAssembly;
    }

    /**
     * 修改数据
     *
     * @param thirdAssembly 实例对象
     * @return 实例对象
     */
    @Override
    public ThirdAssembly update(ThirdAssembly thirdAssembly) {
        this.thirdAssemblyDao.update(thirdAssembly);
        return this.queryById(thirdAssembly.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.thirdAssemblyDao.deleteById(id) > 0;
    }

    @Override
    public void insertAssembly(ThirdAssembly thirdAssembly) {
        thirdAssemblyDao.insert(thirdAssembly);
    }

    @Override
    public List<ThirdAssembly> queryAlByUserId(String userId) {
        ThirdAssembly thirdAssembly = new ThirdAssembly();
        thirdAssembly.setUserId(userId);
        return thirdAssemblyDao.queryAll(thirdAssembly);
    }
}
