package com.service.screenWeb.controller;

import com.service.config.utils.*;
import com.service.screenWeb.dto.UserDto;
import com.service.screenWeb.entity.Assembly;
import com.service.screenWeb.entity.AssemblyInfo;
import com.service.screenWeb.entity.ThirdAssembly;
import com.service.screenWeb.service.AssemblyInfoService;
import com.service.screenWeb.service.AssemblyService;
import com.service.screenWeb.service.ThirdAssemblyService;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.service.config.constant.Constant.*;

/**
 * 组件(Assembly)表控制层
 *
 * @author makejava
 * @since 2021-03-29 15:42:29
 */
@RestController
@RequestMapping("assembly")
@Api(value = "组件管理",description = "组件管理")
public class AssemblyController {

    @Resource
    RedisUtil redisUtil;
    /**
     * 服务对象
     */
    @Resource
    private AssemblyService assemblyService;
    @Resource
    private ThirdAssemblyService thirdAssemblyService;
    @Resource
    private AssemblyInfoService assemblyInfoService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Assembly selectOne(String id) {
        return this.assemblyService.queryById(id);
    }


    @SuppressWarnings("AliEqualsAvoidNull")
    @GetMapping("queryAssemblyList")
    @ApiOperation(value = "查询组件列表")
    @ResponseBody
    public Result queryAssemblyList(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                    @RequestParam(value = "noThird",required = false) @ApiParam("  true  没有第三方组件") Boolean hasThird) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                List<AssemblyInfo> assemblyList = assemblyInfoService.queryAllNoThirdAssembly();
                if (hasThird!=null) {
                    if (hasThird) {
                        return Result.ok().setData(assemblyList);
                    }
                }
                return Result.ok().setData(assemblyList);
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
    @SuppressWarnings("AliEqualsAvoidNull")
    @GetMapping("queryAssemblyListInfo")
    @ApiOperation(value = "查询组件列表-2")
    @ResponseBody
    public Result queryAssemblyListInfo(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                    @RequestParam(value = "noThird",required = false) @ApiParam("  true  没有第三方组件") Boolean hasThird) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                List<ThirdAssembly> assemblyList = assemblyInfoService.queryAll(new AssemblyInfo());
                if (hasThird!=null) {
                    if (hasThird) {
                        return Result.ok().setData(assemblyList.stream().filter(assemblyInfo -> assemblyInfo.getType()!=null && assemblyInfo.getType()==0).collect(Collectors.toList()));
                    }
                    List<ThirdAssembly> thirdAssemblies = ModelMapperUtil.strictMapList(assemblyList, ThirdAssembly.class);
//                    List<ThirdAssembly> thirdAssemblyList = thirdAssemblyService.queryAllByLimit(0,99999999);
////                    thirdAssemblies.addAll(thirdAssemblyList);
                    return Result.ok().setData(thirdAssemblies);
                }
                List<ThirdAssembly> thirdAssemblies = ModelMapperUtil.strictMapList(assemblyList, ThirdAssembly.class);
                //List<ThirdAssembly> thirdAssemblyList = thirdAssemblyService.queryAllByLimit(0,99999999);
                //thirdAssemblies.addAll(thirdAssemblyList);
                return Result.ok().setData(thirdAssemblies);
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
    @PostMapping("insertAssembly")
    @ApiOperation(value = "新增组件")
    @ResponseBody
    public Result insertAssembly(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,Assembly assembly) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                assemblyService.insertAssembly(assembly);
                return Result.ok();
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

    @PostMapping("addThirdAssembly")
    @ApiOperation(value = "用户新增第三方组件")
    @ResponseBody
    public Result addThirdAssembly(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                   @RequestBody ThirdAssembly thirdAssembly) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                thirdAssembly.setId(IdUtil.getStringId());
                thirdAssembly.setUserId(userId);
                thirdAssemblyService.insertAssembly(thirdAssembly);
                return Result.ok();
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
    @PostMapping("updateThirdAssembly")
    @ApiOperation(value = "更新第三方组件")
    @ResponseBody
    public Result updateThirdAssembly(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                      @RequestBody ThirdAssembly thirdAssembly) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                thirdAssemblyService.update(thirdAssembly);
                return Result.ok();
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
    @PostMapping("updateAssembly")
    @ApiOperation(value = "更新组件")
    @ResponseBody
    public Result updateAssembly(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,Assembly assembly) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                assemblyService.updateAssembly(assembly);
                return Result.ok();
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }


    @GetMapping("deleteAssembly")
    @ApiOperation(value = "删除组件")
    @ResponseBody
    public Result deleteAssembly(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,String assemblyId) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                assemblyService.deleteAssembly(assemblyId);
                return Result.ok();
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
}
