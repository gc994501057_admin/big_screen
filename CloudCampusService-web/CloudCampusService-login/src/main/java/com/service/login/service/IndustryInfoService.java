package com.service.login.service;

import com.service.login.entity.IndustryInfo;

import java.util.List;

/**
 * L2行业信息表(IndustryInfo)表服务接口
 *
 * @author makejava
 * @since 2021-05-09 11:34:26
 */
public interface IndustryInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    IndustryInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<IndustryInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param industryInfo 实例对象
     * @return 实例对象
     */
    IndustryInfo insert(IndustryInfo industryInfo);

    /**
     * 修改数据
     *
     * @param industryInfo 实例对象
     * @return 实例对象
     */
    IndustryInfo update(IndustryInfo industryInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    void queryAll(IndustryInfo industryInfo);
}
