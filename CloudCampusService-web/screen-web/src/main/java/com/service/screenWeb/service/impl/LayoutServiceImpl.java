package com.service.screenWeb.service.impl;

import com.service.screenWeb.entity.Layout;
import com.service.screenWeb.dao.LayoutDao;
import com.service.screenWeb.service.LayoutService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 组件布局表(Layout)表服务实现类
 *
 * @author makejava
 * @since 2021-05-18 19:56:25
 */
@Service("layoutService")
public class LayoutServiceImpl implements LayoutService {
    @Resource
    private LayoutDao layoutDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Layout queryById(String id) {
        return this.layoutDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Layout> queryAllByLimit(int offset, int limit) {
        return this.layoutDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param layout 实例对象
     * @return 实例对象
     */
    @Override
    public Layout insert(Layout layout) {
        this.layoutDao.insert(layout);
        return layout;
    }

    /**
     * 修改数据
     *
     * @param layout 实例对象
     * @return 实例对象
     */
    @Override
    public Layout update(Layout layout) {
        this.layoutDao.update(layout);
        return this.queryById(layout.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.layoutDao.deleteById(id) > 0;
    }

    @Override
    public List<Layout> queryDefault() {
        return layoutDao.queryDefault();
    }
}
