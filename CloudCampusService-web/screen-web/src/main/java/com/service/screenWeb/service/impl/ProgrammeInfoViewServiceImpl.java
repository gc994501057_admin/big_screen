package com.service.screenWeb.service.impl;

import com.service.screenWeb.entity.ProgrammeInfoView;
import com.service.screenWeb.dao.ProgrammeInfoViewDao;
import com.service.screenWeb.service.ProgrammeInfoViewService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ProgrammeInfoView)表服务实现类
 *
 * @author makejava
 * @since 2021-05-19 09:46:53
 */
@Service("programmeInfoViewService")
public class ProgrammeInfoViewServiceImpl implements ProgrammeInfoViewService {
    @Resource
    private ProgrammeInfoViewDao programmeInfoViewDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id
     * @return 实例对象
     */
    @Override
    public ProgrammeInfoView queryById(String id) {
        return this.programmeInfoViewDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ProgrammeInfoView> queryAllByLimit(int offset, int limit) {
        return this.programmeInfoViewDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param programmeInfoView 实例对象
     * @return 实例对象
     */
    @Override
    public ProgrammeInfoView insert(ProgrammeInfoView programmeInfoView) {
        this.programmeInfoViewDao.insert(programmeInfoView);
        return programmeInfoView;
    }

    /**
     * 修改数据
     *
     * @param programmeInfoView 实例对象
     * @return 实例对象
     */
    @Override
    public ProgrammeInfoView update(ProgrammeInfoView programmeInfoView) {
        this.programmeInfoViewDao.update(programmeInfoView);
        return this.queryById(programmeInfoView.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.programmeInfoViewDao.deleteById(id) > 0;
    }
}
