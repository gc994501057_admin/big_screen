package com.service.screenReception.entity;

import java.io.Serializable;

/**
 * 用户表(UserInfo)实体类
 *
 * @author makejava
 * @since 2021-03-31 14:36:12
 */
public class UserInfo implements Serializable {
    private static final long serialVersionUID = -90801048760342804L;
    /**
     * 用户id
     */
    private String id;
    /**
     * 账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 手机号码
     */
    private String telephone;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

}
