package com.service.screenWeb.dto;

import lombok.Data;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-04-14 14:55
 **/
@Data
public class TerminalUserDto {
    private String account;
    private Integer onlineTime;
    private String onlineStatus;
    private String accessTime;
    private String accessType;
    private String terminalMac;
    private String siteId;
    private String siteName;
    private Double cumulativeTraffic;

}
