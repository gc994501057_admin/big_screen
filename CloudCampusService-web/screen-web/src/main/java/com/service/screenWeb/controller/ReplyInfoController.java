package com.service.screenWeb.controller;

import com.service.config.utils.*;
import com.service.screenWeb.dto.LargeQaStatusDto;
import com.service.screenWeb.dto.ReplyInfoDto;
import com.service.screenWeb.dto.UserDto;
import com.service.screenWeb.entity.LargeQa;
import com.service.screenWeb.entity.ReplyInfo;
import com.service.screenWeb.service.ReplyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.Date;

import static com.service.config.constant.Constant.*;

/**
 * 回复表(ReplyInfo)表控制层
 *
 * @author makejava
 * @since 2021-05-07 15:00:54
 */
@RestController
@RequestMapping("replyInfo")
@Api(value = "回复意见、建议和问题反馈",description = "回复意见、建议和问题反馈")
public class ReplyInfoController {
    /**
     * 服务对象
     */
    @Resource
    private ReplyInfoService replyInfoService;
    @Resource
    private RedisUtil redisUtil;
    /**
     * 回复用户意见、建议和问题反馈的回复记录
     * @param replyInfoDto
     */
    @PostMapping("updateLargeQaStatus")
    @ApiOperation(value = "回复用户意见、建议和问题反馈")
    @ResponseBody
    public Result updateLargeQaStatus(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                      @RequestBody ReplyInfoDto replyInfoDto,
                                      @RequestParam(value = "isKeFu") @ApiParam("是否客服身份") Boolean isKeFu) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (isKeFu != null) {
                if (isKeFu) { //如果isKeFu为true， 则用户名设置为客服
                    if (redisUtil.exist(cloudAuthToken)) {
                        ReplyInfo replyInfo = ModelMapperUtil.strictMap(replyInfoDto, ReplyInfo.class);
                        replyInfo.setId(IdUtil.getStringId());
                        replyInfo.setDate(new Date());
                        replyInfo.setUsername("客服");
                        replyInfoService.insert(replyInfo);
                        return Result.ok();
                    }else {
                        // token缓存不存在
                        return Result.failure(USERNAME_Off_SITE, usernameOffSite);
                    }
                }else { // 否则为客户
                    if (redisUtil.exist(cloudAuthToken + userId)) {
                        ReplyInfo replyInfo = ModelMapperUtil.strictMap(replyInfoDto, ReplyInfo.class);
                        replyInfo.setId(IdUtil.getStringId());
                        replyInfo.setDate(new Date());
                        if (replyInfo.getRole().equals("0")) {
                            String value = redisUtil.getValue(cloudAuthToken + userId);
                            UserDto userDto = JsonXMLUtils.json2obj(value, UserDto.class);
                            replyInfo.setUserId(userId);
                            replyInfo.setUsername(userDto.getUsername());
                        }
                        replyInfoService.insert(replyInfo);
                        return Result.ok();
                    } else {
                        // token缓存不存在
                        return Result.failure(USERNAME_Off_SITE, usernameOffSite);
                    }
                }
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

}
