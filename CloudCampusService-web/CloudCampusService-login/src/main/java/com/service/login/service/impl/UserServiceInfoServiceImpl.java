package com.service.login.service.impl;


import com.service.login.dao.UserServiceInfoDao;
import com.service.login.dao.UserServiceInfoViewDao;
import com.service.login.entity.UserServiceInfo;
import com.service.login.entity.UserServiceInfoView;
import com.service.login.service.UserServiceInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户与服务关联表(UserServiceInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-04-22 09:50:42
 */
@Service("userServiceInfoService")
public class UserServiceInfoServiceImpl implements UserServiceInfoService {
    @Resource
    private UserServiceInfoDao userServiceInfoDao;
    @Resource
    private UserServiceInfoViewDao userServiceInfoViewDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id
     * @return 实例对象
     */
    @Override
    public UserServiceInfo queryById(String id) {
        return this.userServiceInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<UserServiceInfo> queryAllByLimit(int offset, int limit) {
        return this.userServiceInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param userServiceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public UserServiceInfo insert(UserServiceInfo userServiceInfo) {
        this.userServiceInfoDao.insert(userServiceInfo);
        return userServiceInfo;
    }

    /**
     * 修改数据
     *
     * @param userServiceInfo 实例对象
     * @return 实例对象
     */
    @Override
    public UserServiceInfo update(UserServiceInfo userServiceInfo) {
        this.userServiceInfoDao.update(userServiceInfo);
        return this.queryById(userServiceInfo.getServiceId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.userServiceInfoDao.deleteById(id) > 0;
    }

    @Override
    public List<UserServiceInfoView> queryAll(String userId) {
        UserServiceInfoView userServiceInfoView = new UserServiceInfoView();
        userServiceInfoView.setId(userId);
        return userServiceInfoViewDao.queryAll(userServiceInfoView);
    }
}
