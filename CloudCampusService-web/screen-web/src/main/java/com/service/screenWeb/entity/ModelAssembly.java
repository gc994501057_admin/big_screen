package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * (ModelAssembly)实体类
 *
 * @author makejava
 * @since 2021-04-30 09:39:21
 */
public class ModelAssembly implements Serializable {
    private static final long serialVersionUID = 259736813771831802L;
    /**
     * 模板id
     */
    private String modelId;
    /**
     * 组件id
     */
    private String assemblyId;


    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(String assemblyId) {
        this.assemblyId = assemblyId;
    }

}
