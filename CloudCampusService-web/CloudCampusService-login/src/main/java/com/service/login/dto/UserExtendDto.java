package com.service.login.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

/**
 * @author
 * @version 1.0
 * @date 2021/5/6 17:09
 */

public class UserExtendDto {
    /**
     * 用户信息扩展表
     */@ApiModelProperty("用户信息扩展表id")
    private String id;
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private String userId;
    /**
     * 区域（地址）
     */
    @ApiModelProperty("区域（地址）")
    private String address;
    /**
     * 职业
     */
    @ApiModelProperty("行业")
    private String occupation;
    /**
     * 是否接受营销信息
     */
    @ApiModelProperty("是否接受营销信息")
    private Boolean sendAllow;
    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    private String email;
    /**
     * 公司名称
     */
    @ApiModelProperty("公司名称")
    private String companyName;
    /**
     * 城市
     */
    @ApiModelProperty("城市")
    private String[] city;
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;
    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    private String telephone;
    /**
     * 母公司/集团名称
     */
    private String parentCompanyName;
    /**
     * 母公司/集团网址
     */
    private String parentCompanyUrl;
    /**
     * 公司/企业/学校网址
     */
    private String companyUrl;
    /**
     * 公司/企业/学校地址
     */
    private String companyAddress;
    /**
     * 行业
     */
    private String industry;
    /**
     * 行业
     */
    private String name;
    public UserExtendDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Boolean getSendAllow() {
        return sendAllow;
    }

    public void setSendAllow(Boolean sendAllow) {
        this.sendAllow = sendAllow;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String[] getCity() {
        return city;
    }

    public void setCity(String[] city) {
        this.city = city;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getParentCompanyName() {
        return parentCompanyName;
    }

    public void setParentCompanyName(String parentCompanyName) {
        this.parentCompanyName = parentCompanyName;
    }

    public String getParentCompanyUrl() {
        return parentCompanyUrl;
    }

    public void setParentCompanyUrl(String parentCompanyUrl) {
        this.parentCompanyUrl = parentCompanyUrl;
    }

    public String getCompanyUrl() {
        return companyUrl;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }
    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
