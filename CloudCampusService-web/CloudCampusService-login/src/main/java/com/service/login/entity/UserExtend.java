package com.service.login.entity;

import java.io.Serializable;

/**
 * 用户信息扩展(UserExtend)实体类
 *
 * @author makejava
 * @since 2021-05-09 11:25:51
 */
public class UserExtend implements Serializable {
    private static final long serialVersionUID = 611060833213240979L;
    /**
     * id
     */
    private String id;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 邮箱 （必填）
     */
    private String email;
    /**
     * 公司/企业/学校名称
     */
    private String companyName;
    /**
     * 所属行业
     */
    private String occupation;
    /**
     * 公司/企业/学校网址
     */
    private String companyUrl;
    /**
     * 公司/企业/学校地址
     */
    private String companyAddress;
    /**
     * 城市
     */
    private String city;
    /**
     * 区域（地址）
     */
    private String address;
    /**
     * 是否接受营销信息
     */
    private Boolean sendAllow;
    /**
     * 母公司/集团名称
     */
    private String parentCompanyName;
    /**
     * 母公司/集团网址
     */
    private String parentCompanyUrl;
    /**
     * 行业
     */
    private String industry;
    /**
     * 联系人
     */
    private String name;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCompanyUrl() {
        return companyUrl;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getSendAllow() {
        return sendAllow;
    }

    public void setSendAllow(Boolean sendAllow) {
        this.sendAllow = sendAllow;
    }

    public String getParentCompanyName() {
        return parentCompanyName;
    }

    public void setParentCompanyName(String parentCompanyName) {
        this.parentCompanyName = parentCompanyName;
    }

    public String getParentCompanyUrl() {
        return parentCompanyUrl;
    }

    public void setParentCompanyUrl(String parentCompanyUrl) {
        this.parentCompanyUrl = parentCompanyUrl;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
