package com.service.login.service.impl;

import com.service.config.utils.*;
import com.service.login.dao.UserInfoDao;
import com.service.login.dto.ScreenUser;
import com.service.login.dto.ScreenUserDto;
import com.service.login.dto.UserDto;
import com.service.login.entity.UserInfo;
import com.service.login.service.FeginMstsc;
import com.service.login.service.FeginMstscBsAdmin;
import com.service.login.service.UserInfoService;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author
 * @version 1.0
 * @date 2021/6/1 14:19
 */
@Component
public class AsyncService {
    public static ExecutorService product = Executors.newFixedThreadPool(30);

    @Resource
    private FeginMstscBsAdmin feginMstscBsAdmin;
    @Resource
    private FeginMstsc feginMstsc;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private UserInfoService userInfoService;
    @Async
    public void asyncMethod(String token,UserInfo userInfo2, ScreenUserDto screenUser){
        try {
            product.submit(()->{
                feginMstscBsAdmin.addApiAccount(userInfo2.getId());
                feginMstsc.addApiAccount(screenUser, userInfo2.getId()); //远程校验api账号是否重复以及新增关联华为云管理api账号

            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
