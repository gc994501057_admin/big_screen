package com.service.config.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.filechooser.FileSystemView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class UuIDUtil {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static void main(String[] args) throws IOException {
        //  List<StringBuffer> list1 = new ArrayList<StringBuffer>();
        // String files="ct";
        // list1 = Random1(10000, 1000);
        //WriterFun(files);
        //WriterFun(files);
        String line="-";
        String d= Random2("-78");
        System.out.println(line.concat(d));


    }

    /**
     * 测试生成随机数
     * @param rssi
     * @return
     */
    public static String Random2(String rssi) {
        rssi =rssi.substring(1);
        Integer integer = Integer.valueOf(rssi);
        if (integer > 65 && integer < 70) {
            integer+= (int)(Math.random()*7);  //Generate random integers;
            return String.valueOf(integer);
        }else if (integer >= 70) {
            integer += (int)(Math.random()*10);
        }else{
            integer += (int)(Math.random()*10);
            return String.valueOf(integer);
        }
        if (integer > 78) {
            integer = integer-10;
            Random2(String.valueOf(integer));
        }else {
            return String.valueOf(integer);
        }
        return String.valueOf(integer);
    }

    /**
     * 获取编号
     * @throws IOException
     */

    public static void WriterFun (String files,Integer numbers) throws IOException {
        try {

            FileSystemView fsv = FileSystemView.getFileSystemView();
            File com=fsv.getHomeDirectory(); //这便是读取桌面路径的方法了
            List<StringBuffer> list = new ArrayList<StringBuffer>();
            //获得路径
            String filepath = "C:\\Users\\ADMIN\\Desktop\\file.txt";
            File file = new File(com+"\\"+files+".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter bw = null;
            bw = new BufferedWriter(new FileWriter(file));
            char[] letters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
                    'W', 'X', 'Y', 'Z'};
            for (int j = 0; j < numbers; j++) {
                for (int y = 0; y < letters.length; y++) {
                    StringBuffer pwd = new StringBuffer("");
                    pwd.append(letters[y]);
                    double code = Math.random() * (10000 - 1000) + 1000;
                    Integer x = (int) code;
                    pwd.append(x);
                    list.add(pwd);
                }
            }
            Set set1 = new HashSet();
            List newList1 = new ArrayList();
            for (StringBuffer integer : list) {
                if (set1.add(integer)) {
                    newList1.add(integer);
                }
            }
            for (Object stringBuffer : newList1) {
                bw.write(stringBuffer.toString());
                bw.write("\r\n");
            }
            bw.newLine();
            bw.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取uuid
     * @throws IOException
     */
    public static void writeUuid (String files,Integer numbers) throws IOException {
        try {
            FileSystemView fsv = FileSystemView.getFileSystemView();
            File com=fsv.getHomeDirectory(); //这便是读取桌面路径的方法了
            List<String> list = new ArrayList<String>();
            //获得路径
            String filepath ="C:\\Users\\admin\\Desktop\\uuid.txt";
            File file = new File(com+"\\"+files+"UUID.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter bw = null;
            bw = new BufferedWriter(new FileWriter(file));
            int number=numbers*26;
            for (int x = 0; x < number; x++) {
                String uuid = UUID.randomUUID().toString();
                //System.out.println(uuid);
                list.add(uuid);
            }
            Set set1 = new HashSet();
            List newList1 = new ArrayList();
            for (String integer : list) {
                if(set1.add(integer)) {
                    newList1.add(integer);
                }
            }
            for (Object stringBuffer:newList1) {
                bw.write(stringBuffer.toString());
                bw.write("\r\n");
            }
            bw.newLine();
            bw.close();
        }catch (IOException e) {
            e.printStackTrace();}
    }
}
