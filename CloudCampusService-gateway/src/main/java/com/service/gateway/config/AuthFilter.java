package com.service.gateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 网关鉴权
 */
@Slf4j
@Component
public class AuthFilter implements GlobalFilter, Ordered {

    private static final String[] whiteList = {"/auth/login", "/user/register",

            "/dakar/news/getList", "/dakar/v2/api-docs", "/wifi/news/getList", "/wifi/v2/api-docs"};

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String url = exchange.getRequest().getURI().getPath();
//        log.info("url:{}", url);
        // 跳过不需要验证的路径
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return -200;
    }
}
