package com.service.login.service;

import com.service.login.bean.MailBean;
import com.service.login.dto.UserExtendDto;
import com.service.login.entity.UserExtend;

import java.util.List;

/**
 * 用户信息扩展(UserExtend)表服务接口
 *
 * @author makejava
 * @since 2021-03-30 11:39:11
 */
public interface UserExtendService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserExtend queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserExtend> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param userExtend 实例对象
     * @return 实例对象
     */
    UserExtend insert(UserExtend userExtend);

    /**
     * 修改数据
     *
     * @param userExtend 实例对象
     * @return 实例对象
     */
    UserExtend update(UserExtend userExtend);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    void insertUserExtend(UserExtendDto userExtend) throws Exception;

    void insertMspUserExtend(UserExtendDto userExtend) throws Exception;

    UserExtendDto queryByMspUserId(String userId);

    UserExtendDto queryByUserId(String userId);

    void sendMail(MailBean mailBean);
}
