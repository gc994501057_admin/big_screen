package com.service.config.constant;

/**
 * @author
 * @version 1.0
 * @date 2021/3/31 10:02
 */
public class Constant {
    public static final int  USER_NOT_FOUND = 10001; // 用户不存在
    public static final String userNotFound = "用户不存在";
    public static final int  USER_NOT_LOGIN = 10002; // 用户未登录
    public static final String userNotLogin = "用户未登录";
    public static final int  USERNAME_OR_NOTFOUND = 10003; // 用户名或密码为空,请重新输入
    public static final String usernameOrNotFound = "用户名或密码为空,请重新输入";
    public static final int  USERNAME_REPEAT = 10004; // 用户名已存在，请重新输入
    public static final String usernameRepeat = "用户名已存在，请重新输入";
    public static final int  USERNAME_Off_SITE = 10005; // 登录信息已失效
    public static final String usernameOffSite = "登录信息已失效";
    public static final int  USER_INFO_IMPERFECT = 10006; // 用户信息不完善请联系管理人员进行修正
    public static final String userInfoImperfect = "用户信息不完善请联系管理人员进行修正";
    public static final int  CLOUD_CAMPUS_ACCOUNT_ERROR = 10007;//云管理账号有误
    public static final String cloudCampusAccountError = "云管理账号有误";
    public static final int  CODE_INVALID = 10008;// 验证码失效
    public static final String codeInvalid = "验证码失效";
    public static final int  USERNAME_DIFFERENT = 10009;// 用户名不正确，请重新填写
    public static final String usernameDifferent = "用户名不正确，请重新填写";
    public static final int  PARAMETER_NOT_FOUND = 10010;// 参数不存在，请重新校验
    public static final String parameterNotFound = "参数不存在，请重新校验";
    public static final int  CLOUD_CAMPUS_ACCOUNT_SAME = 10011;// 云管理账号同域名已存在，是否覆盖
    public static final String cloudCampusAccountSame = "云管理账号同域名已存在，是否覆盖";
    public static final int  USER_IS_TOKEN = 10012; // 用户重复登陆
    public static final String userIsToken = "用户重复登陆";
    public static final String  LARGE_SCREEN_NOTICE = "service:large_screen:notice"; // 公共接口公告
    public static final String  SERVICE_INFO = "service:info"; // 开通的服务信息
    public static final String  USER_INFO = "user:info"; // 用户信息
    public static final String  SEND_RELIVE_INFO = "大屏系统账号解封提示：\r\n" +
            "您的账号已解封。\r\n" +
            "请登录系统：\r\n" +
            "https://cloudcampusapps.top/bs"; // 账号解封邮件提醒
    public static final String  IS_LOGOUT = "大屏系统封号提示：\r\n" +
            "1、疑似黑客攻击。\r\n" +
            "2、账号登录异常。\r\n" +
            "3、大屏系统链接：\r\n"+"https://cloudcampusapps.top/bs \r\n"+
            "申请解封请联系 :\r\n"+"     xxxxxx"; // 账号封号邮件提醒
    public static final String  USER_PROGRAMME_INFO = "user:programme:info"; // 方案信息
    public static final String  USER_ASSEMBLY_INFO = "user:assembly:info"; // 组件信息
    public static final String  USER_LAYOUT_INFO = "user:layout:info"; // 布局信息
    public static final String  USER_CONFIG_INFO = "user:config:info"; // 组件单独配置信息
    public static final int  USER_PHONE_UNAUTHORIZED_CODE = 10013; // 用户手机号未授权
    public static final String  USER_PHONE_UNAUTHORIZED_INFO = "用户手机号未授权"; // 用户手机号未授权
    public static final int  CODE_EXPIRE = 10014; //临时code过期请重新获取code
    public static final String  CODE_DESCRIBS = "临时code过期请重新获取code"; // 临时code过期请重新获取code
    public static final int  REVIEW_WAIT = 0; // 待审核
    public static final int  REVIEW_TIME = 1; // 审核中
    public static final int  REVIEW_FAIL = 2; // 审核失败
    public static final int  REVIEW_SUCCESS = 3; // 审核成功
    public static final int  REVIEW_ISSUE = 4; // 下发成功
    public static final int  REVIEW_CANCEL = 5; // 已回退
}
