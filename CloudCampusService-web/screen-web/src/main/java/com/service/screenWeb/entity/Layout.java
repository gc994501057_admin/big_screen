package com.service.screenWeb.entity;

import java.io.Serializable;

/**
 * 组件布局表(Layout)实体类
 *
 * @author makejava
 * @since 2021-05-18 19:56:25
 */
public class Layout implements Serializable {
    private static final long serialVersionUID = 176872169607492972L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 组件x
     */
    private Double assemblyX;
    /**
     * 组件y
     */
    private Double assemblyY;
    /**
     * 宽
     */
    private Double width;
    /**
     * 高
     */
    private Double height;
    /**
     * 组件信息id
     */
    private String assemblyId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 方案表id
     */
    private String programmeId;
    /**
     * 组件序号
     */
    private Integer number;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getAssemblyX() {
        return assemblyX;
    }

    public void setAssemblyX(Double assemblyX) {
        this.assemblyX = assemblyX;
    }

    public Double getAssemblyY() {
        return assemblyY;
    }

    public void setAssemblyY(Double assemblyY) {
        this.assemblyY = assemblyY;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(String assemblyId) {
        this.assemblyId = assemblyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProgrammeId() {
        return programmeId;
    }

    public void setProgrammeId(String programmeId) {
        this.programmeId = programmeId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

}
