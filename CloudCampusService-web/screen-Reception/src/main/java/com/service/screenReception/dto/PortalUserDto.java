package com.service.screenReception.dto;

import lombok.Data;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-04-01 10:05
 **/
@Data
public class PortalUserDto {
    private String deviceId;
    private String siteId;
    private String ssid;
    private String terminalMac;
    private String userName;
}
