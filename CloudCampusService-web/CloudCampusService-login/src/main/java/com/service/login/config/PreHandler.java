package com.service.login.config;


import com.service.config.annotation.Distinct;
import com.service.config.annotation.ParamsRequestWrapper;
import com.service.config.utils.JsonXMLUtils;
import com.service.config.utils.RedisUtil;
import com.service.login.dto.UserInfoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public class PreHandler implements HandlerInterceptor {
    private static final Logger LOG = LoggerFactory.getLogger(PreHandler.class);
    private
    @Autowired
    RedisUtil redisUtil;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.判断是否存在注解
        if (!(handler instanceof HandlerMethod)) {
            LOG.info("不是HandlerMethod类型，则无需检查");
            return true;
        }
        HandlerMethod method = (HandlerMethod) handler;
        boolean flag = method.getMethod().isAnnotationPresent(Distinct.class);
        if (!flag) {
            //不存在Distinct注解，则直接通过
            LOG.info("不存在Distinct注解，则直接通过");
            return true;
        }



//        RequestWrapper requestWrapper = new RequestWrapper(request);
//        String body =requestWrapper.getBody() ;
        ParamsRequestWrapper paramsRequestWrapper = new ParamsRequestWrapper(request);
        Map<String, String[]> parameterMap = paramsRequestWrapper.getParameterMap();
       // String body = HttpHpler.getBodyString(request);
        UserInfoDto requestBodEntity = JsonXMLUtils.json2obj(JsonXMLUtils.obj2json(parameterMap),UserInfoDto.class);
        if (redisUtil.exists(requestBodEntity.getUsername()+requestBodEntity.getPassword())){
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "该用户的上次请求还未处理完成");
            return false;
        }else {
            redisUtil.set(requestBodEntity.getUsername()+requestBodEntity.getPassword(),1,JsonXMLUtils.obj2json(parameterMap),10);
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception { }



    public String getRealIp(HttpServletRequest request) {
        return getString(request);
    }
    public static String getString(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        ip = getString(request, ip);
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    public static String getString(HttpServletRequest request, String ip) {
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        return ip;
    }
}
