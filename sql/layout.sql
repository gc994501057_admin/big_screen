/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50637
 Source Host           : localhost:3306
 Source Schema         : large_screen

 Target Server Type    : MySQL
 Target Server Version : 50637
 File Encoding         : 65001

 Date: 19/08/2021 16:33:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for layout
-- ----------------------------
DROP TABLE IF EXISTS `layout`;
CREATE TABLE `layout`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `assembly_x` double(25, 0) NULL DEFAULT NULL COMMENT '组件x',
  `assembly_y` double(25, 0) NULL DEFAULT NULL COMMENT '组件y',
  `width` double NULL DEFAULT NULL COMMENT '宽',
  `height` double NULL DEFAULT NULL COMMENT '高',
  `assembly_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件信息id',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `programme_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方案表id',
  `number` int(11) NULL DEFAULT NULL COMMENT '组件序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组件布局表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of layout
-- ----------------------------
INSERT INTO `layout` VALUES ('1', 50, 110, 1800, 900, '6', NULL, '1', 1);
INSERT INTO `layout` VALUES ('10', 50, 110, 390, 925, '1', NULL, '4', 1);
INSERT INTO `layout` VALUES ('100', 50, 341, 290, 500, '1', NULL, '14', 1);
INSERT INTO `layout` VALUES ('101', 1570, 100, 290, 225, '2', NULL, '14', 2);
INSERT INTO `layout` VALUES ('102', 50, 855, 290, 190, '3', NULL, '14', 3);
INSERT INTO `layout` VALUES ('103', 355, 855, 590, 195, '4', NULL, '14', 4);
INSERT INTO `layout` VALUES ('104', 965, 855, 590, 195, '5', NULL, '14', 5);
INSERT INTO `layout` VALUES ('105', 660, 100, 585, 360, '6', NULL, '14', 6);
INSERT INTO `layout` VALUES ('11', 50, 110, 390, 600, '1', NULL, '5', 1);
INSERT INTO `layout` VALUES ('12', 1480, 110, 390, 600, '7', NULL, '5', 7);
INSERT INTO `layout` VALUES ('13', 470, 110, 980, 600, '6', NULL, '5', 6);
INSERT INTO `layout` VALUES ('14', 50, 740, 885, 295, '4', NULL, '5', 4);
INSERT INTO `layout` VALUES ('15', 985, 740, 885, 295, '5', NULL, '5', 5);
INSERT INTO `layout` VALUES ('16', 50, 110, 390, 600, '1', NULL, '6', 1);
INSERT INTO `layout` VALUES ('17', 1480, 110, 390, 285, '8', NULL, '6', 8);
INSERT INTO `layout` VALUES ('18', 1480, 425, 389, 285, '3', NULL, '6', 3);
INSERT INTO `layout` VALUES ('19', 470, 110, 980, 600, '6', NULL, '6', 6);
INSERT INTO `layout` VALUES ('2', 1350, 110, 520, 900, '7', NULL, '2', 1);
INSERT INTO `layout` VALUES ('20', 50, 740, 885, 295, '4', NULL, '6', 4);
INSERT INTO `layout` VALUES ('21', 985, 740, 885, 295, '5', NULL, '6', 5);
INSERT INTO `layout` VALUES ('22', 50, 425, 390, 285, '2', NULL, '7', 2);
INSERT INTO `layout` VALUES ('23', 50, 110, 390, 285, '9', NULL, '7', 9);
INSERT INTO `layout` VALUES ('24', 1480, 110, 390, 285, '8', NULL, '7', 8);
INSERT INTO `layout` VALUES ('25', 1480, 425, 390, 285, '3', NULL, '7', 3);
INSERT INTO `layout` VALUES ('26', 470, 110, 980, 600, '6', NULL, '7', 6);
INSERT INTO `layout` VALUES ('27', 50, 740, 885, 295, '4', NULL, '7', 4);
INSERT INTO `layout` VALUES ('28', 985, 740, 885, 295, '5', NULL, '7', 5);
INSERT INTO `layout` VALUES ('29', 50, 425, 290, 285, '2', NULL, '8', 2);
INSERT INTO `layout` VALUES ('3', 50, 110, 1270, 900, '6', NULL, '2', 2);
INSERT INTO `layout` VALUES ('30', 1580, 110, 290, 285, '9', NULL, '8', 9);
INSERT INTO `layout` VALUES ('31', 50, 740, 290, 285, '8', NULL, '8', 8);
INSERT INTO `layout` VALUES ('32', 1580, 425, 290, 600, '7', NULL, '8', 7);
INSERT INTO `layout` VALUES ('33', 50, 110, 290, 285, '3', NULL, '8', 3);
INSERT INTO `layout` VALUES ('34', 370, 110, 1180, 600, '6', NULL, '8', 6);
INSERT INTO `layout` VALUES ('35', 370, 740, 575, 285, '4', NULL, '8', 4);
INSERT INTO `layout` VALUES ('36', 975, 740, 575, 285, '5', NULL, '8', 5);
INSERT INTO `layout` VALUES ('37', 50, 340, 400, 470, '1', NULL, '9', 1);
INSERT INTO `layout` VALUES ('38', 1470, 825, 400, 225, '2', NULL, '9', 2);
INSERT INTO `layout` VALUES ('39', 50, 825, 400, 225, '3', NULL, '9', 3);
INSERT INTO `layout` VALUES ('4', 50, 110, 390, 900, '1', NULL, '3', 1);
INSERT INTO `layout` VALUES ('40', 465, 755, 490, 295, '4', NULL, '9', 4);
INSERT INTO `layout` VALUES ('41', 965, 755, 490, 295, '5', NULL, '9', 5);
INSERT INTO `layout` VALUES ('42', 465, 100, 990, 640, '6', NULL, '9', 6);
INSERT INTO `layout` VALUES ('43', 1470, 340, 400, 470, '7', NULL, '9', 7);
INSERT INTO `layout` VALUES ('44', 1470, 100, 400, 225, '8', NULL, '9', 8);
INSERT INTO `layout` VALUES ('45', 50, 100, 400, 225, '9', NULL, '9', 9);
INSERT INTO `layout` VALUES ('46', 50, 341, 390, 500, '1', NULL, '10', 1);
INSERT INTO `layout` VALUES ('47', 1470, 100, 390, 225, '2', NULL, '10', 2);
INSERT INTO `layout` VALUES ('48', 50, 855, 390, 190, '3', NULL, '10', 3);
INSERT INTO `layout` VALUES ('49', 455, 855, 490, 195, '4', NULL, '10', 4);
INSERT INTO `layout` VALUES ('5', 1480, 110, 390, 900, '7', NULL, '3', 7);
INSERT INTO `layout` VALUES ('50', 965, 855, 490, 195, '5', NULL, '10', 5);
INSERT INTO `layout` VALUES ('51', 455, 100, 1000, 450, '6', NULL, '10', 6);
INSERT INTO `layout` VALUES ('52', 1470, 339, 390, 500, '7', NULL, '10', 7);
INSERT INTO `layout` VALUES ('53', 1470, 855, 390, 195, '8', NULL, '10', 8);
INSERT INTO `layout` VALUES ('54', 50, 100, 390, 225, '9', NULL, '10', 9);
INSERT INTO `layout` VALUES ('55', 455, 570, 1000, 270, '10', NULL, '10', 10);
INSERT INTO `layout` VALUES ('56', 50, 341, 390, 500, '1', NULL, '11', 1);
INSERT INTO `layout` VALUES ('57', 1470, 100, 390, 225, '2', NULL, '11', 2);
INSERT INTO `layout` VALUES ('58', 50, 855, 390, 190, '3', NULL, '11', 3);
INSERT INTO `layout` VALUES ('59', 455, 855, 490, 195, '4', NULL, '11', 4);
INSERT INTO `layout` VALUES ('6', 470, 110, 980, 900, '6', NULL, '3', 6);
INSERT INTO `layout` VALUES ('60', 965, 855, 490, 195, '5', NULL, '11', 5);
INSERT INTO `layout` VALUES ('61', 455, 100, 1000, 450, '6', NULL, '11', 6);
INSERT INTO `layout` VALUES ('62', 1470, 339, 390, 500, '7', NULL, '11', 7);
INSERT INTO `layout` VALUES ('63', 1470, 855, 390, 195, '8', NULL, '11', 8);
INSERT INTO `layout` VALUES ('64', 50, 100, 390, 225, '9', NULL, '11', 9);
INSERT INTO `layout` VALUES ('65', 965, 570, 490, 270, '10', NULL, '11', 10);
INSERT INTO `layout` VALUES ('66', 455, 570, 490, 270, '11', NULL, '11', 11);
INSERT INTO `layout` VALUES ('67', 50, 341, 390, 500, '1', NULL, '12', 1);
INSERT INTO `layout` VALUES ('68', 1470, 100, 390, 225, '2', NULL, '12', 2);
INSERT INTO `layout` VALUES ('69', 50, 855, 390, 190, '3', NULL, '12', 3);
INSERT INTO `layout` VALUES ('7', 1480, 110, 390, 925, '7', NULL, '4', 7);
INSERT INTO `layout` VALUES ('70', 455, 855, 490, 195, '4', NULL, '12', 4);
INSERT INTO `layout` VALUES ('71', 965, 855, 490, 195, '5', NULL, '12', 5);
INSERT INTO `layout` VALUES ('72', 965, 570, 490, 270, '10', NULL, '12', 10);
INSERT INTO `layout` VALUES ('73', 1470, 339, 390, 500, '7', NULL, '12', 7);
INSERT INTO `layout` VALUES ('74', 1470, 855, 390, 195, '8', NULL, '12', 8);
INSERT INTO `layout` VALUES ('75', 50, 100, 390, 225, '9', NULL, '12', 9);
INSERT INTO `layout` VALUES ('76', 455, 100, 490, 450, '12', NULL, '12', 12);
INSERT INTO `layout` VALUES ('77', 455, 570, 490, 270, '11', NULL, '12', 11);
INSERT INTO `layout` VALUES ('78', 965, 100, 490, 450, '13', NULL, '12', 13);
INSERT INTO `layout` VALUES ('79', 50, 341, 290, 500, '1', NULL, '13', 1);
INSERT INTO `layout` VALUES ('8', 470, 110, 980, 600, '6', NULL, '4', 6);
INSERT INTO `layout` VALUES ('80', 1570, 100, 290, 225, '2', NULL, '13', 2);
INSERT INTO `layout` VALUES ('81', 50, 855, 290, 190, '3', NULL, '13', 3);
INSERT INTO `layout` VALUES ('82', 355, 855, 590, 195, '4', NULL, '13', 4);
INSERT INTO `layout` VALUES ('83', 965, 855, 590, 195, '5', NULL, '13', 5);
INSERT INTO `layout` VALUES ('84', 660, 100, 585, 740, '6', NULL, '13', 6);
INSERT INTO `layout` VALUES ('85', 1570, 339, 290, 500, '7', NULL, '13', 7);
INSERT INTO `layout` VALUES ('86', 1570, 855, 290, 195, '8', NULL, '13', 8);
INSERT INTO `layout` VALUES ('87', 50, 100, 290, 225, '9', NULL, '13', 9);
INSERT INTO `layout` VALUES ('88', 1265, 480, 290, 360, '10', NULL, '13', 10);
INSERT INTO `layout` VALUES ('89', 355, 480, 290, 360, '11', NULL, '13', 11);
INSERT INTO `layout` VALUES ('9', 470, 740, 980, 295, '4', NULL, '4', 4);
INSERT INTO `layout` VALUES ('90', 355, 100, 290, 360, '12', NULL, '13', 12);
INSERT INTO `layout` VALUES ('91', 1265, 100, 290, 360, '13', NULL, '13', 13);
INSERT INTO `layout` VALUES ('92', 1570, 339, 290, 500, '7', NULL, '14', 7);
INSERT INTO `layout` VALUES ('93', 1570, 855, 290, 195, '8', NULL, '14', 8);
INSERT INTO `layout` VALUES ('94', 50, 100, 290, 225, '9', NULL, '14', 9);
INSERT INTO `layout` VALUES ('95', 1265, 480, 290, 360, '10', NULL, '14', 10);
INSERT INTO `layout` VALUES ('96', 355, 480, 290, 360, '11', NULL, '14', 11);
INSERT INTO `layout` VALUES ('97', 355, 100, 290, 360, '12', NULL, '14', 12);
INSERT INTO `layout` VALUES ('98', 1265, 100, 290, 360, '13', NULL, '14', 13);
INSERT INTO `layout` VALUES ('99', 660, 480, 585, 360, '14', NULL, '14', 14);

SET FOREIGN_KEY_CHECKS = 1;
