package com.service.login.controller;

import com.service.config.utils.ModelMapperUtil;
import com.service.config.utils.RedisUtil;
import com.service.config.utils.Result;
import com.service.login.entity.ServiceInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

import static com.service.config.constant.Constant.*;

/**
 * @author
 * @version 1.0
 * @date 2021/4/14 11:26
 */
@Api(value = "公共服务接口",produces = "公共服务接口")
@RestController
@RequestMapping("/CommonService")
public class CommonServiceController {

    @Autowired
    private RedisUtil redisUtil;
    /**
     * 读取公告
     * @param
     * @return
     */
    @GetMapping(value = "Notice",produces="application/json")
    @ApiOperation(value = "读取公告")
    @ResponseBody
    public Result getNotice() throws Exception {
        if (redisUtil.exist(LARGE_SCREEN_NOTICE)) {
            String value = redisUtil.getValue(LARGE_SCREEN_NOTICE);
            return Result.ok().setData(value);
        }
        return Result.ok();
    }
    /**
     * 配置公告
     *
     * @param
     * @return
     */
    @GetMapping(value = "getAllServices",produces="application/json")
    @ApiOperation(value = "配置公告")
    @ResponseBody
    public Result getAllServices(@RequestParam String notice) throws Exception {
        if (notice !=null && !notice.equals("")) {
            redisUtil.set(LARGE_SCREEN_NOTICE,5,notice,3);
            return Result.ok();
        }
        return Result.failure(PARAMETER_NOT_FOUND,"参数不存在，请重新校验");
    }
}
