import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    base: '/bs_login/',
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Login',
            component: () => import('../views/Login.vue'),
            meta: {title: '登录界面'}
        },
        {
            path: '/forgot',
            name: 'Forgot',
            component: () => import('../views/Forgot.vue'),
            meta: {title: '忘记密码'}
        },
        {
            path: '*',
            name: 'Login',
            component: () => import('../views/Login.vue'),
            meta: {title: '登录界面'}
        }
    ]
});
