package com.service.screenWeb.dao;

import com.service.screenWeb.entity.ProgrammeInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 方案信息表(ProgrammeInfo)表数据库访问层
 *
 * @author makejava
 * @since 2021-05-18 19:56:24
 */
public interface ProgrammeInfoDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ProgrammeInfo queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ProgrammeInfo> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param programmeInfo 实例对象
     * @return 对象列表
     */
    List<ProgrammeInfo> queryAll(ProgrammeInfo programmeInfo);

    /**
     * 新增数据
     *
     * @param programmeInfo 实例对象
     * @return 影响行数
     */
    int insert(ProgrammeInfo programmeInfo);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<ProgrammeInfo> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<ProgrammeInfo> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<ProgrammeInfo> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<ProgrammeInfo> entities);

    /**
     * 修改数据
     *
     * @param programmeInfo 实例对象
     * @return 影响行数
     */
    int update(ProgrammeInfo programmeInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    List<ProgrammeInfo> queryDefault();

    List<ProgrammeInfo> queryByUserId(String userId);

    void updateStatus(@Param("entities")List<ProgrammeInfo> programmeInfoList);

    void updateFileId(ProgrammeInfo programmeInfo);
}

