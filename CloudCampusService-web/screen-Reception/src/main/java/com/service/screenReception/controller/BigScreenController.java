package com.service.screenReception.controller;

import com.service.config.utils.*;
import com.service.screenReception.dto.SiteDto;
import com.service.screenReception.service.BigScreenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static com.service.config.constant.Constant.*;

/**
 * @program: CloudCampusService
 * @description:站点分布GIS图
 * @author: rui
 * @create: 2021-04-01 14:48
 **/
@CrossOrigin
@Api(value = "站点分布GIS图", description = "查询站点分布")
@RestController
@RequestMapping("/bigScreen")
public class BigScreenController {

    @Resource
    RedisUtil redisUtil;

    @Resource
    BigScreenService bigScreenService;

    @GetMapping("querySiteGis")
    @ApiOperation(value = "站点的地图分布")
    @ResponseBody
    public Result queryServiceList(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {

        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);

            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(ModelMapperUtil.strictMapList(redisUtil.getList1(userId.concat("_siteData")), SiteDto.class));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }


    @GetMapping("queryDeviceCounts")
    @ApiOperation(value = "设备总数柱状图")
    @ResponseBody
    public Result queryDeviceCounts(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryDeviceCounts(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }


    @GetMapping("queryDeviceType")
    @ApiOperation(value = "分类型的设备数量（饼状图）")
    @ResponseBody
    public Result queryDeviceType(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryDeviceType(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");

    }


    @GetMapping("queryOnlineUserCount")
    @ApiOperation(value = "在线总人数（柱状图）：总数+各个站点的人数")
    @ResponseBody
    public Result queryOnlineUserCount(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryOnlineUserCount(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");


    }


    @GetMapping("querySiteTraffic")
    @ApiOperation(value = "当日网络流量")
    @ResponseBody
    public Result querySiteTraffic(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {

                return Result.ok().setData(bigScreenService.querySiteTrafficByDay(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");

    }

    @GetMapping("querySiteTrafficByMonth")
    @ApiOperation(value = "当月网络流量")
    @ResponseBody
    public Result querySiteTrafficByMonth(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {

                return Result.ok().setData(bigScreenService.querySiteTrafficByMonth(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");

    }

    @GetMapping("querySiteTrafficByYear")
    @ApiOperation(value = "当年网络流量")
    @ResponseBody
    public Result querySiteTrafficByYear(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {

                return Result.ok().setData(bigScreenService.querySiteTrafficByYear(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");

    }

//    @GetMapping("querySiteTrafficByHistory")
//    @ApiOperation(value = "历史网络流量")
//    @ResponseBody
//    public Result querySiteTrafficByHistory(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
//        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
//            String userId = UserTokenManager.getUserId(cloudAuthToken);
//            if (redisUtil.exist(cloudAuthToken + userId)) {
//
//                return Result.ok().setData(bigScreenService.querySiteTrafficByHistory(userId));
//            } else {
//                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
//            }
//        }
//        return Result.failure(USER_NOT_LOGIN, "用户未登录");
//    }


    @GetMapping("querySsidTraffic")
    @ApiOperation(value = "Top流量应用")
    @ResponseBody
    public Result querySsidTraffic(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.querySsidTraffic(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }


    @GetMapping("queryTerminalUser")
    @ApiOperation(value = "终端用户在线时长列表")
    @ResponseBody
    public Result queryTerminalUser(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryTerminalUser(userId,"onlineTime"));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

    @GetMapping("queryTerminalTraffic")
    @ApiOperation(value = "终端用户在线流量列表")
    @ResponseBody
    public Result queryTerminalTraffic(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryTerminalUser(userId,"traffic"));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

    @GetMapping("queryDeviceStatus")
    @ApiOperation(value = "设备状态总览")
    @ResponseBody
    public Result queryDeviceStatus(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryDeviceStatus(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }




    @GetMapping("queryByAccessType")
    @ApiOperation(value = "查询在线/离线接入用户人数")
    @ResponseBody
    public Result queryByAccessType(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryByAccessType(userId));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

    @GetMapping("queryDeviceHealth")
    @ApiOperation(value = "Health-查询设备健康度")
    @ResponseBody
    public Result queryDeviceHealth(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryHealth(userId,"deviceHealth"));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }


    @GetMapping("queryRadioHealth")
    @ApiOperation(value = "Health-查询射频健康度")
    @ResponseBody
    public Result queryRadioHealth(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryHealth(userId,"radioHealth"));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

    @GetMapping("querySiteHealth")
    @ApiOperation(value = "Health-查询站点健康度")
    @ResponseBody
    public Result querySiteHealth(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryHealth(userId,"siteHealth"));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }


    @GetMapping("queryDeviceHealthPoint")
    @ApiOperation(value = "Health-AP设备健康度")
    @ResponseBody
    public Result queryDeviceHealthPoint(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken) {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                return Result.ok().setData(bigScreenService.queryHealth(userId,"deviceHealthPoint"));
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
}
