package com.service.screenWeb.dto;

import lombok.Data;

import java.util.List;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-03-30 16:38
 **/
@Data
public class SiteDto {
    private String id;
    private String tenantId;
    private String name;
    private String latitude;//维度
    private String longtitude;//经度
    private Integer deviceCounts;
    private List<DeviceDto> device;

}
