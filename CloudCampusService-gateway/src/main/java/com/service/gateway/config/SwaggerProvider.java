package com.service.gateway.config;


import com.service.config.utils.JsonXMLUtils;
import com.service.config.utils.RedisUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import org.springframework.cloud.gateway.config.GatewayProperties;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.support.NameUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName SwaggerProvider
 * @Description
 * @Date 2020/04/19 10:04
 * @Version 1.0
 */
@Component
@Primary
@AllArgsConstructor
public class SwaggerProvider implements SwaggerResourcesProvider {
    public static final String API_URI = "/v2/api-docs";
    public static final String PATTERN = "pattern";


    private final RouteLocator routeLocator;

    private final GatewayProperties gatewayProperties;
    @Resource
    RedisUtil redisUtil;

    @SneakyThrows
    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        List<SwaggerResource> resources1 = new ArrayList<>();
        List<String> routes = new ArrayList<>();
        // 取出gateway的route
        routeLocator.getRoutes().subscribe(route -> routes.add(route.getId()));
        gatewayProperties.getRoutes().stream()
                .forEach(routeDefinition -> routeDefinition.getPredicates().stream()
                        .filter(predicateDefinition -> ("Path").equalsIgnoreCase(predicateDefinition.getName()))
                        .forEach(predicateDefinition -> resources
                                .add(swaggerResource(routeDefinition.getId(), predicateDefinition.getArgs()
                                        .get(NameUtils.GENERATED_NAME_PREFIX + "0").replace("/**", API_URI)))));
        Set<String> values = redisUtil.values("service");
        if (values != null) {
            values.forEach(s -> {
                try {
                    RouteDefinition demo = JsonXMLUtils.json2obj(s, RouteDefinition.class);
                    SwaggerResource swaggerResource = swaggerResource(demo.getId(), demo.getPredicates().get(0).getArgs().get(PATTERN).replace("/**", API_URI));
                    resources.add(swaggerResource);
                }catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            });
        }
        // 去重
        Map<String, List<SwaggerResource>> collect = resources.stream().collect(Collectors.groupingBy(SwaggerResource::getName));//根据mac分组
        //内部遍历 比较，获取最早的打卡时间
        collect.entrySet().forEach(entry ->
        {
            List<SwaggerResource> swaggerResourceList = entry.getValue();
            Optional<SwaggerResource> swaggerResource = swaggerResourceList.stream().findFirst();
            resources1.add(swaggerResource.get()); //放入集合中
        });
        return resources1;
    }

    private SwaggerResource swaggerResource(String name, String location) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion("2.0");
        return swaggerResource;
    }
}
