package com.service.login.service;


import com.service.config.utils.Result;
import com.service.login.dto.ScreenUserDto;
import com.service.login.service.impl.UserServerFailBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author
 * @version 1.0
 * @date 2020/6/18 14:54
 */
@FeignClient(value = "service-unique",configuration = UserServerFailBack.class)
public interface FeginMstscUnique {
    /**
     * 配置账号远程接口
     * @return
     */
    @RequestMapping(value = "/getUniqueSequence",method = RequestMethod.GET)
    public Result addApiAccount();
}
