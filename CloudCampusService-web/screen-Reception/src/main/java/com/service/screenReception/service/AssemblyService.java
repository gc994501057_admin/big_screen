package com.service.screenReception.service;

import com.service.screenReception.entity.Assembly;

import java.util.List;

/**
 * 组件(Assembly)表服务接口
 *
 * @author makejava
 * @since 2021-04-14 09:29:33
 */
public interface AssemblyService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Assembly queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Assembly> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param assembly 实例对象
     * @return 实例对象
     */
    Assembly insert(Assembly assembly);

    /**
     * 修改数据
     *
     * @param assembly 实例对象
     * @return 实例对象
     */
    Assembly update(Assembly assembly);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}
