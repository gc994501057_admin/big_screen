package com.service.screenWeb.service;

import com.service.screenWeb.dto.LargeQaAndReplyDto;
import com.service.screenWeb.entity.LargeQa;

import java.util.List;

/**
 * 意见、建议、问题反馈表(LargeQa)表服务接口
 *
 * @author makejava
 * @since 2021-05-07 15:00:53
 */
public interface LargeQaService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    LargeQa queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<LargeQa> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param largeQa 实例对象
     * @return 实例对象
     */
    LargeQa insert(LargeQa largeQa);

    /**
     * 修改数据
     *
     * @param largeQa 实例对象
     * @return 实例对象
     */
    LargeQa update(LargeQa largeQa);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    void addAndUpdateLargeQa(LargeQa strictMap);

    List<LargeQaAndReplyDto> queryAll(LargeQa largeQa, Boolean getHistory);
}
