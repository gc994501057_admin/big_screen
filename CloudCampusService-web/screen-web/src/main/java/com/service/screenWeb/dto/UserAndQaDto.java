package com.service.screenWeb.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户表(UserInfo)实体类
 *
 * @author makejava
 * @since 2021-03-30 11:37:21
 */
@Data
public class UserAndQaDto implements Serializable {
    private static final long serialVersionUID = -52503473282382160L;
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private String id;
    /**
     * 账号
     */
    @ApiModelProperty("账号")
    private String username;
    List<LargeQaAndReplyDto> largeQaAndReplyDtoList;

}
