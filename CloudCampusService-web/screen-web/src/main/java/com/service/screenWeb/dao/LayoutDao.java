package com.service.screenWeb.dao;

import com.service.screenWeb.entity.Layout;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 组件布局表(Layout)表数据库访问层
 *
 * @author makejava
 * @since 2021-05-18 19:56:25
 */
public interface LayoutDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Layout queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Layout> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param layout 实例对象
     * @return 对象列表
     */
    List<Layout> queryAll(Layout layout);

    /**
     * 新增数据
     *
     * @param layout 实例对象
     * @return 影响行数
     */
    int insert(Layout layout);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Layout> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Layout> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Layout> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Layout> entities);

    /**
     * 修改数据
     *
     * @param layout 实例对象
     * @return 影响行数
     */
    int update(Layout layout);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    List<Layout> queryDefault();

    void deleteByProgrammeId(String id);
}

