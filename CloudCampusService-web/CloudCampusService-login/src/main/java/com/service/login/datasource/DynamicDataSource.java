package com.service.login.datasource;

/**
 * @author
 * @version 1.0
 * @date 2021/4/28 14:47
 */

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取数据源（依赖于 spring）  定义一个类继承AbstractRoutingDataSource实现determineCurrentLookupKey方法，该方法可以实现数据库的动态切换
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    //缓存添加的数据源
    private Map<Object, Object> dynamicTargetDataSources = new HashMap<>();
    //缓存默认数据源
    private Object dynamicDefaultTargetDataSource = null;

    /**
     * 重新加载数据源
     */
    @Override
    public void setTargetDataSources(Map<Object, Object> targetDataSources) {
        super.setTargetDataSources(targetDataSources);
        dynamicTargetDataSources.putAll(targetDataSources);
        super.afterPropertiesSet();// 必须添加该句，否则新添加数据源无法识别到
    }

    /**
     * 设置默认数据源
     */
    @Override
    public void setDefaultTargetDataSource(Object defaultTargetDataSource) {
        super.setDefaultTargetDataSource(defaultTargetDataSource);
        this.dynamicDefaultTargetDataSource = defaultTargetDataSource;
        super.afterPropertiesSet();// 必须添加该句，否则新添加数据源无法识别到
    }


    /**
     * 获取默认数据源
     */
    public Object getDynamicDefaultTargetDataSource() {
        return dynamicDefaultTargetDataSource;
    }

    /**
     * 获取已加载的数据源
     */
    public Map<Object, Object> getDynamicTargetDataSources() {
        return dynamicTargetDataSources;
    }

    /**
     * 重写至AbstractRoutingDataSource,运行时就是在这里根据名称动态切换数据源的
     */
    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.get();
    }
}
