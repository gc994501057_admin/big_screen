package com.service.screenWeb.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 回复表(ReplyInfo)实体类
 *
 * @author makejava
 * @since 2021-05-07 15:00:53
 */
public class ReplyInfo implements Serializable {
    private static final long serialVersionUID = 557720066062805535L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 意见、建议和问题反馈表id
     */
    private String qaId;
    /**
     * 客服id
     */
    private String customerServiceId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 回复内容
     */
    private String comment;
    /**
     * 回复时间
     */
    private Date date;
    /**
     * 用户名
     */
    private String username;
    /**
     * 回复id
     */
    private String replyId;
    /**
     * 回复者身份
     */
    private String role;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQaId() {
        return qaId;
    }

    public void setQaId(String qaId) {
        this.qaId = qaId;
    }

    public String getCustomerServiceId() {
        return customerServiceId;
    }

    public void setCustomerServiceId(String customerServiceId) {
        this.customerServiceId = customerServiceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
