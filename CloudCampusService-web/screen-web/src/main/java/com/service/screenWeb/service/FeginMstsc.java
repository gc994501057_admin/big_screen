package com.service.screenWeb.service;


import com.service.config.utils.Result;
import com.service.screenWeb.service.impl.UserServerFailBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author
 * @version 1.0
 * @date 2020/6/18 14:54
 */
@FeignClient(value = "service-signin",configuration = UserServerFailBack.class)
public interface FeginMstsc {
    @RequestMapping(value = "/userInfo/queryAllUser",method = RequestMethod.GET)
    public Result s();
}
