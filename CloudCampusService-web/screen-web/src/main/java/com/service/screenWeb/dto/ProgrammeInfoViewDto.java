package com.service.screenWeb.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * (ProgrammeInfoView)实体类
 *
 * @author makejava
 * @since 2021-05-19 09:46:51
 */
public class ProgrammeInfoViewDto implements Serializable {
    private static final long serialVersionUID = 696721447192441226L;

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private String userId;

    /**
     * 布局表id
     */
    @ApiModelProperty("布局表id")
    private String layoutId;
    /**
     * 组件x
     */
    @ApiModelProperty("组件x")
    private Double assemblyX;
    /**
     * 组件y
     */
    @ApiModelProperty("组件y")
    private Double assemblyY;
    /**
     * 高
     */
    @ApiModelProperty("高")
    private Double height;
    /**
     * 宽
     */
    @ApiModelProperty("宽")
    private Double width;
    /**
     * 组件id
     */
    @ApiModelProperty("组件id")
    private String assemblyId;
    /**
     * 组件引用的图片地址
     */
    @ApiModelProperty("组件引用的图片地址")
    private String assemblyImg;
    /**
     * 组件名称
     */
    @ApiModelProperty("组件名称")
    private String assemblyName;
    /**
     * vue路由组件
     */
    @ApiModelProperty("vue路由组件")
    private String component;
    /**
     * 序号
     */
    @ApiModelProperty("组件序号")
    private Integer number;
    /**
     * 组件单独配置表id
     */
    @ApiModelProperty("组件单独配置表id")
    private String assemblyConfigureId;
    /**
     * 站点id--根据站点id进行站点拓扑
     */
    @ApiModelProperty(" 站点id--根据站点id进行站点拓扑")
    private String siteId;
    /**
     * 接口url
     */
    @ApiModelProperty("接口url")
    private String apiUrl;
    /**
     * 认证密码
     */
    @ApiModelProperty("认证密码")
    private String authPassword;
    /**
     * 认证账号
     */
    @ApiModelProperty("")
    private String authUsername;
    /**
     * 颜色
     */
    @ApiModelProperty("颜色")
    private String backgroundColor;
    /**
     * 组件自定义刷新时间- 单位分钟
     */
    @ApiModelProperty("组件自定义刷新时间- 单位分钟")
    private Integer schedule;

    /**
     *客户起的名字
     */
    @ApiModelProperty("客户起的名字")
    private String authUrl;

    /**
     * 组件类型 0--内置组件 1--第三方组件
     */
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }

    public Double getAssemblyX() {
        return assemblyX;
    }

    public void setAssemblyX(Double assemblyX) {
        this.assemblyX = assemblyX;
    }

    public Double getAssemblyY() {
        return assemblyY;
    }

    public void setAssemblyY(Double assemblyY) {
        this.assemblyY = assemblyY;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public String getAssemblyId() {
        return assemblyId;
    }

    public void setAssemblyId(String assemblyId) {
        this.assemblyId = assemblyId;
    }

    public String getAssemblyImg() {
        return assemblyImg;
    }

    public void setAssemblyImg(String assemblyImg) {
        this.assemblyImg = assemblyImg;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public void setAssemblyName(String assemblyName) {
        this.assemblyName = assemblyName;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getAssemblyConfigureId() {
        return assemblyConfigureId;
    }

    public void setAssemblyConfigureId(String assemblyConfigureId) {
        this.assemblyConfigureId = assemblyConfigureId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getAuthPassword() {
        return authPassword;
    }

    public void setAuthPassword(String authPassword) {
        this.authPassword = authPassword;
    }

    public String getAuthUsername() {
        return authUsername;
    }

    public void setAuthUsername(String authUsername) {
        this.authUsername = authUsername;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Integer getSchedule() {
        return schedule;
    }

    public void setSchedule(Integer schedule) {
        this.schedule = schedule;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }
}
