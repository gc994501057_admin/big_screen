package com.service.screenWeb.service;

import com.service.screenWeb.entity.VFilePhysicsInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 物理文件消息表(VFilePhysicsInfo)表服务接口
 *
 * @author makejava
 * @since 2021-05-18 19:56:30
 */
public interface VFilePhysicsInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    VFilePhysicsInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<VFilePhysicsInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param vFilePhysicsInfo 实例对象
     * @return 实例对象
     */
    VFilePhysicsInfo insert(VFilePhysicsInfo vFilePhysicsInfo);

    /**
     * 修改数据
     *
     * @param vFilePhysicsInfo 实例对象
     * @return 实例对象
     */
    VFilePhysicsInfo update(VFilePhysicsInfo vFilePhysicsInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    void uploadServiceLogo(MultipartFile file, String programmeId) throws Exception;
}
