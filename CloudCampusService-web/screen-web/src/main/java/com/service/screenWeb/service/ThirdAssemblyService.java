package com.service.screenWeb.service;

import com.service.screenWeb.entity.ThirdAssembly;

import java.util.List;

/**
 * 第三方组件(ThirdAssembly)表服务接口
 *
 * @author makejava
 * @since 2021-05-10 11:36:10
 */
public interface ThirdAssemblyService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ThirdAssembly queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ThirdAssembly> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param thirdAssembly 实例对象
     * @return 实例对象
     */
    ThirdAssembly insert(ThirdAssembly thirdAssembly);

    /**
     * 修改数据
     *
     * @param thirdAssembly 实例对象
     * @return 实例对象
     */
    ThirdAssembly update(ThirdAssembly thirdAssembly);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    void insertAssembly(ThirdAssembly thirdAssembly);

    List<ThirdAssembly> queryAlByUserId(String userId);
}
