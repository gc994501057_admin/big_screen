//package com.service.gateway.config;
//
//
//import org.apache.catalina.connector.Connector;
//
//import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
//import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
//import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.server.ServerHttpRequest;
//import reactor.core.publisher.Mono;
//
//import java.util.List;
//
//
///**
// * @author
// * @version 1.0
// * @date 2020/12/3 16:48
// */
//@Configuration
//public class GateWayConfig {
//
//    @Bean
//    public ServletWebServerFactory serverFactory() {
//        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
//        tomcat.addAdditionalTomcatConnectors(createStandardConnector());
//        return tomcat;
//    }
//    /**
//     * 配置http
//     * @return
//     */
//    private Connector createStandardConnector() {
//        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
//        connector.setPort(8091);
//        return connector;
//    }
//}
