package com.service.screenWeb.controller;


import com.service.config.utils.ModelMapperUtil;
import com.service.config.utils.RedisUtil;
import com.service.config.utils.Result;
import com.service.config.utils.UserTokenManager;
import com.service.screenWeb.dto.*;
import com.service.screenWeb.entity.ProgrammeInfo;
import com.service.screenWeb.entity.ScreenModel;
import com.service.screenWeb.entity.UserServiceInfo;
import com.service.screenWeb.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.service.config.constant.Constant.*;

/**
 * 模板（视图）(ScreenModel)表控制层
 *
 * @author makejava
 * @since 2021-03-29 15:32:44
 */
@RestController
@Api(value = "模板信息管理",description = "模板信息管理")
@RequestMapping("screenModel")
public class ScreenModelController {
    /**
     * 服务对象
     */

    @Resource
    RedisUtil redisUtil;

    @Resource
    private ScreenModelService screenModelService;
    @Resource
    private UserServiceInfoService userServiceInfoService;
    @Resource
    private ScreenUserService screenUserService;

    @Resource
    private ProgrammeInfoService programmeInfoService;


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public ScreenModel selectOne(String id) {
        return this.screenModelService.queryById(id);
    }



    @GetMapping("queryModelList")
    @ApiOperation(value = "查询模板列表")
    @ResponseBody
    public Result queryModelList(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,@RequestParam(value = "serviceId", required = false) @ApiParam("服务id存在则去查询用户自定义否则获取内置的模板列表")String serviceId) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                if (serviceId != null && !serviceId.equals("")) { //服务id存在则去查询用户自定义否则获取内置的模板列表
                    ProgrammeInfo programmeInfo = new ProgrammeInfo();
                    programmeInfo.setUserId(userId);
                    List<ProgrammeInfoDto> programmeInfoList = programmeInfoService.queryAll(programmeInfo,userId); //个人方案
                    List<ScreenUserDto> screenUserDtoList = screenUserService.queryAll(userId);
                    if (screenUserDtoList == null || screenUserDtoList.size()==0 ) {
                        return Result.failure(10013,"您的账号还未关联华为云管理账号，请进入大屏后台进行关联");
                    }
                    return Result.ok().setData(programmeInfoList);
                }
                return Result.ok().setData(programmeInfoService.queryDefault()); // 默认方案
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

    /**
     * fegin远程调用接口
     * @param userId
     * @return
     * @throws Exception
     */
    @GetMapping("queryInsideModelList")
    @ApiOperation(value = "查询内置模板列表")
    @ResponseBody
    public Result queryInsideModelList(@RequestParam(value = "userId", required = false) @ApiParam("服务id存在则去查询用户自定义否则获取内置的模板列表")String userId) throws Exception {
        ProgrammeInfo programmeInfo = new ProgrammeInfo();
        programmeInfo.setUserId(userId);
        List<ProgrammeInfoDto> programmeInfoList = programmeInfoService.queryAll(programmeInfo,userId); //个人方案
        return Result.ok().setData(programmeInfoList);
    }
    @PostMapping("setApplication")
    @ApiOperation(value = "应用该大屏方案")
    @ResponseBody
    public Result setApplication(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                 @RequestBody ProgrammeInfoStatusDto programmeInfoStatusDto) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                HashMap map = programmeInfoService.updateStatus(ModelMapperUtil.strictMap(programmeInfoStatusDto, ProgrammeInfo.class));
                if (map.containsKey("errorMsg")) { // 是否存在错误信息
                    return Result.ok().setData(map);
                }
                return Result.ok(); // 默认方案
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

    @GetMapping("deleteProgramme")
    @ApiOperation(value = "删除该方案")
    @ResponseBody
    public Result deleteProgramme(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                 @RequestParam String id) throws Exception {
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                HashMap map = programmeInfoService.deleteById(id);
                if (map != null && !map.isEmpty() && map.containsKey("errorMsg")) {
                    return Result.ok().setData(map);
                }
                return Result.ok(); // 默认方案
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }
}
