package com.service.screenWeb.service;



import com.service.screenWeb.entity.PurchaseCharge;

import java.util.List;

/**
 * 用户购买计费表（字段待补充）(PurchaseCharge)表服务接口
 *
 * @author makejava
 * @since 2021-03-29 15:42:41
 */
public interface PurchaseChargeService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    PurchaseCharge queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<PurchaseCharge> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param purchaseCharge 实例对象
     * @return 实例对象
     */
    PurchaseCharge insert(PurchaseCharge purchaseCharge);

    /**
     * 修改数据
     *
     * @param purchaseCharge 实例对象
     * @return 实例对象
     */
    PurchaseCharge update(PurchaseCharge purchaseCharge);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}
