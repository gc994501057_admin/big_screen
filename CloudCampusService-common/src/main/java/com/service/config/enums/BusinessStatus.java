package com.service.config.enums;

/**
 *  * 操作状态
 * @ClassName BusinessStatus
 * @Description TODO
 * @Date 2021-02-03 15:02
 * @Version 1.0
 **/
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
