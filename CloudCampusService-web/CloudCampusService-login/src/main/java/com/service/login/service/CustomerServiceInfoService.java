package com.service.login.service;

import com.service.login.entity.CustomerServiceInfo;

import java.util.List;

/**
 * 系统运维人员信息表(CustomerServiceInfo)表服务接口
 *
 * @author makejava
 * @since 2021-05-08 17:36:15
 */
public interface CustomerServiceInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CustomerServiceInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<CustomerServiceInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param customerServiceInfo 实例对象
     * @return 实例对象
     */
    CustomerServiceInfo insert(CustomerServiceInfo customerServiceInfo);

    /**
     * 修改数据
     *
     * @param customerServiceInfo 实例对象
     * @return 实例对象
     */
    CustomerServiceInfo update(CustomerServiceInfo customerServiceInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    CustomerServiceInfo queryAllByNameAndPassword(CustomerServiceInfo customerServiceInfo);
}
