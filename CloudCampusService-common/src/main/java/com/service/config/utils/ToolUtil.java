package com.service.config.utils;

import java.io.File;

public class ToolUtil {
    /**
     * 获取临时目录
     *
     * @author mmc
     */
    public static String getTempPath() {
        return System.getProperty("java.io.tmpdir");
    }

    /**
     * 获取当前项目工作目录
     *
     * @return
     * @author mmc
     */
    public static String getUserDir() {
        return System.getProperty("user.dir");
    }

    /**
     * 获取临时下载目录
     *
     * @return
     * @author mmc
     */
    public static String getDownloadPath() {
        return getTempPath() + File.separator + "download" + File.separator;
    }
}
