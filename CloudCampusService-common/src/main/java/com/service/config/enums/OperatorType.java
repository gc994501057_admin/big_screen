package com.service.config.enums;

/**
 *  * 操作人类别
 * @ClassName OperatorType
 * @Description TODO
 * @Date 2021-02-03 15:02
 * @Version 1.0
 **/
public enum OperatorType
{
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
