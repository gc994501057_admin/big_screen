package com.service.screenWeb.service.impl;


import com.service.screenWeb.dao.PurchaseChargeDao;
import com.service.screenWeb.entity.PurchaseCharge;
import com.service.screenWeb.service.PurchaseChargeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户购买计费表（字段待补充）(PurchaseCharge)表服务实现类
 *
 * @author makejava
 * @since 2021-03-29 15:42:41
 */
@Service("purchaseChargeService")
public class PurchaseChargeServiceImpl implements PurchaseChargeService {
    @Resource
    private PurchaseChargeDao purchaseChargeDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public PurchaseCharge queryById(String id) {
        return this.purchaseChargeDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<PurchaseCharge> queryAllByLimit(int offset, int limit) {
        return this.purchaseChargeDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param purchaseCharge 实例对象
     * @return 实例对象
     */
    @Override
    public PurchaseCharge insert(PurchaseCharge purchaseCharge) {
        this.purchaseChargeDao.insert(purchaseCharge);
        return purchaseCharge;
    }

    /**
     * 修改数据
     *
     * @param purchaseCharge 实例对象
     * @return 实例对象
     */
    @Override
    public PurchaseCharge update(PurchaseCharge purchaseCharge) {
        this.purchaseChargeDao.update(purchaseCharge);
        return this.queryById(purchaseCharge.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.purchaseChargeDao.deleteById(id) > 0;
    }
}
