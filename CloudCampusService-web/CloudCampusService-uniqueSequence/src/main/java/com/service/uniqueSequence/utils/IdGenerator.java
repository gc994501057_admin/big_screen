package com.service.uniqueSequence.utils;

interface IdGenerator {
	public Long nextId();
}

