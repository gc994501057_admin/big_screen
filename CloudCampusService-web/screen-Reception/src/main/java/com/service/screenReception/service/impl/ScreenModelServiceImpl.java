package com.service.screenReception.service.impl;

import com.service.screenReception.dao.ScreenModelDao;
import com.service.screenReception.entity.ScreenModel;
import com.service.screenReception.service.ScreenModelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 模板（视图）(ScreenModel)表服务实现类
 *
 * @author makejava
 * @since 2021-04-14 09:29:41
 */
@Service("screenModelService")
public class ScreenModelServiceImpl implements ScreenModelService {
    @Resource
    private ScreenModelDao screenModelDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ScreenModel queryById(String id) {
        return this.screenModelDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ScreenModel> queryAllByLimit(int offset, int limit) {
        return this.screenModelDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param screenModel 实例对象
     * @return 实例对象
     */
    @Override
    public ScreenModel insert(ScreenModel screenModel) {
        this.screenModelDao.insert(screenModel);
        return screenModel;
    }

    /**
     * 修改数据
     *
     * @param screenModel 实例对象
     * @return 实例对象
     */
    @Override
    public ScreenModel update(ScreenModel screenModel) {
        this.screenModelDao.update(screenModel);
        return this.queryById(screenModel.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.screenModelDao.deleteById(id) > 0;
    }
}
