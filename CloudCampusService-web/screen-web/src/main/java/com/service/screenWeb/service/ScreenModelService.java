package com.service.screenWeb.service;



import com.service.screenWeb.dto.ScreenModelDto;
import com.service.screenWeb.entity.ScreenModel;

import java.util.List;

/**
 * 模板（视图）(ScreenModel)表服务接口
 *
 * @author makejava
 * @since 2021-03-29 15:42:44
 */
public interface ScreenModelService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ScreenModel queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ScreenModel> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param screenModel 实例对象
     * @return 实例对象
     */
    ScreenModel insert(ScreenModel screenModel);

    /**
     * 修改数据
     *
     * @param screenModel 实例对象
     * @return 实例对象
     */
    ScreenModel update(ScreenModel screenModel);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    List<ScreenModelDto> queryModelList() throws Exception;
}
