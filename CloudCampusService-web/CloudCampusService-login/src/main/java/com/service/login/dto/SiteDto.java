package com.service.login.dto;

import lombok.Data;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-03-30 16:38
 **/
@Data
public class SiteDto {
    private String id;
    private String tenantId;
    private String name;
    private String latitude;//维度
    private String longtitude;//经度
    private Integer deviceCounts;


}
