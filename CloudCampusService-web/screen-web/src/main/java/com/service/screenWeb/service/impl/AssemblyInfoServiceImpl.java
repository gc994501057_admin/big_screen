package com.service.screenWeb.service.impl;

import com.service.config.utils.ModelMapperUtil;
import com.service.screenWeb.dao.AssemblyConfigureDao;
import com.service.screenWeb.entity.AssemblyConfigure;
import com.service.screenWeb.entity.AssemblyInfo;
import com.service.screenWeb.dao.AssemblyInfoDao;
import com.service.screenWeb.entity.ThirdAssembly;
import com.service.screenWeb.service.AssemblyInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 组件信息表(AssemblyInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-05-18 19:56:28
 */
@Service("assemblyInfoService")
public class AssemblyInfoServiceImpl implements AssemblyInfoService {
    @Resource
    private AssemblyInfoDao assemblyInfoDao;
    @Resource
    private AssemblyConfigureDao assemblyConfigureDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AssemblyInfo queryById(String id) {
        return this.assemblyInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<AssemblyInfo> queryAllByLimit(int offset, int limit) {
        return this.assemblyInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assemblyInfo 实例对象
     * @return 实例对象
     */
    @Override
    public AssemblyInfo insert(AssemblyInfo assemblyInfo) {
        this.assemblyInfoDao.insert(assemblyInfo);
        return assemblyInfo;
    }

    /**
     * 修改数据
     *
     * @param assemblyInfo 实例对象
     * @return 实例对象
     */
    @Override
    public AssemblyInfo update(AssemblyInfo assemblyInfo) {
        this.assemblyInfoDao.update(assemblyInfo);
        return this.queryById(assemblyInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.assemblyInfoDao.deleteById(id) > 0;
    }

    @Override
    public List<ThirdAssembly> queryAll(AssemblyInfo assemblyInfo) {
        List<AssemblyInfo> assemblyInfoList = assemblyInfoDao.queryAll(assemblyInfo);
        List<ThirdAssembly> thirdAssemblies = ModelMapperUtil.strictMapList(assemblyInfoList, ThirdAssembly.class);
        List<AssemblyConfigure> assemblyConfigureList = assemblyConfigureDao.queryDefault();
        thirdAssemblies.stream().forEach(thirdAssembly -> {
            List<AssemblyConfigure> collect = assemblyConfigureList.stream().filter(assemblyConfigure -> assemblyConfigure.getAssemblyId().equals(thirdAssembly.getId())).collect(Collectors.toList());
            if (collect.size()>0) {
                AssemblyConfigure assemblyConfigure = collect.get(0);
                if (assemblyConfigure.getApiUrl() !=null) {
                    thirdAssembly.setApiUrl(assemblyConfigure.getApiUrl());
                }
                if (assemblyConfigure.getAuthUrl() !=null) {
                    thirdAssembly.setAuthUrl(assemblyConfigure.getAuthUrl());
                }
            }
        });
        return thirdAssemblies;
    }

    @Override
    public List<AssemblyInfo> queryAllNoThirdAssembly() {
        AssemblyInfo assemblyInfo = new AssemblyInfo();
        assemblyInfo.setType(0);
        List<AssemblyInfo> assemblyInfoList = assemblyInfoDao.queryAll(assemblyInfo);
        return assemblyInfoList;
    }
}
