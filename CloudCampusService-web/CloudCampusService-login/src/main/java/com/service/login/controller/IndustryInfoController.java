package com.service.login.controller;

import com.service.config.utils.Result;
import com.service.login.entity.IndustryInfo;
import com.service.login.service.IndustryInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * L2行业信息表(IndustryInfo)表控制层
 *
 * @author makejava
 * @since 2021-05-09 11:34:27
 */
@RestController
@RequestMapping("industryInfo")
public class IndustryInfoController {
    /**
     * 服务对象
     */
    @Resource
    private IndustryInfoService industryInfoService;

    /**
     * L2行业下拉框
     *
     * @return 单条数据
     */
    @PostMapping(value = "IndustryInfo",produces="application/json")
    @ApiOperation(value = "L2行业下拉框")
    @ResponseBody
    public Result  IndustryInfo() {
        industryInfoService.queryAll(new IndustryInfo());
        return Result.ok();
    }

}
