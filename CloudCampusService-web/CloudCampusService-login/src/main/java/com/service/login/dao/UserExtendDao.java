package com.service.login.dao;

import com.service.login.entity.UserExtend;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户信息扩展(UserExtend)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-30 11:39:11
 */
public interface UserExtendDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserExtend queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserExtend> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param userExtend 实例对象
     * @return 对象列表
     */
    List<UserExtend> queryAll(UserExtend userExtend);

    /**
     * 新增数据
     *
     * @param userExtend 实例对象
     * @return 影响行数
     */
    int insert(UserExtend userExtend);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserExtend> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<UserExtend> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserExtend> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<UserExtend> entities);

    /**
     * 修改数据
     *
     * @param userExtend 实例对象
     * @return 影响行数
     */
    int update(UserExtend userExtend);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    UserExtend queryByUserId(String userId);
}

