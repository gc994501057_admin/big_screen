package com.service.login.dao;

import com.service.login.entity.UserLoginRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表(UserLoginRecord)表数据库访问层
 *
 * @author makejava
 * @since 2021-05-11 11:34:15
 */
public interface UserLoginRecordDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserLoginRecord queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserLoginRecord> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param userLoginRecord 实例对象
     * @return 对象列表
     */
    List<UserLoginRecord> queryAll(UserLoginRecord userLoginRecord);

    /**
     * 新增数据
     *
     * @param userLoginRecord 实例对象
     * @return 影响行数
     */
    int insert(UserLoginRecord userLoginRecord);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserLoginRecord> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<UserLoginRecord> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserLoginRecord> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<UserLoginRecord> entities);

    /**
     * 修改数据
     *
     * @param userLoginRecord 实例对象
     * @return 影响行数
     */
    int update(UserLoginRecord userLoginRecord);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    List<UserLoginRecord> queryAllByUserId(String userId);
}

