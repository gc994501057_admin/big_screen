package com.service.uniqueSequence.controller;

import com.service.uniqueSequence.utils.IdUtil;
import com.service.uniqueSequence.utils.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Set;

/**
 * @author
 * @version 1.0
 * @date 2021/6/7 10:16
 */
@Controller
public class SnowflakeIdGeneratorController {
    /**
     * 用户获取已添加的服务
     *
     * @param
     * @return
     */
    @GetMapping(value = "getUniqueSequence",produces="application/json")
    @ResponseBody
    public Result getAllServices() {
        return Result.ok().setData(IdUtil.getStringId());
    }
}
