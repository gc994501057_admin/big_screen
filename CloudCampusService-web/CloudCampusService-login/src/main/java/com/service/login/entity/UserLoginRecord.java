package com.service.login.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 用户表(UserLoginRecord)实体类
 *
 * @author makejava
 * @since 2021-05-11 11:33:23
 */
public class UserLoginRecord implements Serializable {
    private static final long serialVersionUID = -43404234085215300L;
    /**
     * 记录id
     */
    private String id;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 账号
     */
    private String username;
    /**
     * 手机号码
     */
    private String telephone;
    /**
     * 注册时间
     */
    private Date addTime;
    /**
     * 登录时间
     */
    private Date date;
    /**
     * 第几次登录
     */
    private Integer count;
    /**
     * 下线时间
     */
    private Date offTime;
    /**
     * 登录ip
     */
    private String ip;
    /**
     * 在线时长
     */
    private long onlineTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getOffTime() {
        return offTime;
    }

    public void setOffTime(Date offTime) {
        this.offTime = offTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public long getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(long onlineTime) {
        this.onlineTime = onlineTime;
    }

}
