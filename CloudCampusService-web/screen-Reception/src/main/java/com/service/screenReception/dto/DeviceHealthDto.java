package com.service.screenReception.dto;

import lombok.Data;

import java.math.BigInteger;

/**
 * @program: CloudCampusService
 * @description: 设备健康度
 * @author: rui
 * @create: 2021-06-09 16:36
 **/
@Data
public class DeviceHealthDto {
    private String siteId;
    private BigInteger timestamp;
    private Integer deviceHealthPoint;

}
