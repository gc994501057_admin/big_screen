package com.service.screenWeb.dto;

import lombok.Data;

/**
 * @program: CloudCampusService
 * @description:
 * @author: rui
 * @create: 2021-06-08 15:09
 **/
@Data
public class TopNSsidTrafficDto {
    private String ssidName;
    private String ssidTraffic;
    private String onlineUsers;
    private String siteId;
    private String siteName;
}
