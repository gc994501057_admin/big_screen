package com.service.screenReception.enums;

import org.springframework.util.StringUtils;

public enum AssemblyEnum {

    SITE_DEVICE("1","站点设备数据"),
    APP_TRAFFIC ("2","TopN流量应用"),
    ONLINE("4","实时在线总人数"),
    TRAFFIC("5","网络流量"),
    TERMINAL_USER("7","终端接入用户列表"),
    SITE_HEALTH("18","健康度");

    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private AssemblyEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public static AssemblyEnum get(String code){
        if (StringUtils.hasLength(code)) {
            for (AssemblyEnum element : AssemblyEnum.values()) {
                if (element.getCode().equals(code)) {
                    return element;
                }
            }
        }
        return null;
    }

}
