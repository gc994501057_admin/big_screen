package com.service.screenReception.service.impl;

import com.service.config.utils.IdUtil;
import com.service.config.utils.ModelMapperUtil;
import com.service.screenReception.dao.ScreenUserDao;
import com.service.screenReception.dto.ScreenUserDto;
import com.service.screenReception.entity.ScreenUser;
import com.service.screenReception.service.ScreenUserService;
import com.service.screenReception.task.CloudCampusApi;
import com.service.screenReception.task.ScheduleService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 大屏用户表(ScreenUser)表服务实现类
 *
 * @author makejava
 * @since 2021-04-14 09:29:46
 */
@Service("screenUserService")
public class ScreenUserServiceImpl implements ScreenUserService {
    @Resource
    private ScreenUserDao screenUserDao;


    @Resource
    CloudCampusApi cloudCampusApi;
    @Resource
    ScheduleService scheduleService;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ScreenUser queryById(String id) {
        return this.screenUserDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ScreenUser> queryAllByLimit(int offset, int limit) {
        return this.screenUserDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param screenUser 实例对象
     * @return 实例对象
     */
    @Override
    public ScreenUser insert(ScreenUser screenUser) {
        this.screenUserDao.insert(screenUser);
        return screenUser;
    }

    /**
     * 修改数据
     *
     * @param screenUser 实例对象
     * @return 实例对象
     */
    @Override
    public ScreenUser update(ScreenUser screenUser) {
        this.screenUserDao.update(screenUser);
        return this.queryById(screenUser.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.screenUserDao.deleteById(id) > 0;
    }


    @Override
    public ScreenUser checkTenant(ScreenUserDto screenUserDto, String userId) {
        HttpHeaders requestHeaders = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        requestHeaders.setContentType(type);
        requestHeaders.add("Accept", MediaType.APPLICATION_JSON.toString());
        //获取token 验证api账号是否已经存在或者有误
        String token = cloudCampusApi.getToken(screenUserDto.getTenantName(), screenUserDto.getTenantPwd(), requestHeaders,screenUserDto.getDomain());

        List<ScreenUser> screenUsers = screenUserDao.queryAll(new ScreenUser());

        //该账号是否存在
        if (screenUsers.size() > 0) {
            Boolean flag = screenUsers.stream().anyMatch(task -> task.getTenantName().equals(screenUserDto.getTenantName()));
            if (flag) {
                return null;
            }
        }

        if (token != null) {
            ScreenUser screenUser = ModelMapperUtil.strictMap(screenUserDto, ScreenUser.class);
            screenUser.setId(IdUtil.getStringId());
            screenUser.setUserId(userId);
            screenUser.setStatus(true);
            insert(screenUser);
            scheduleService.getLoginUserDispose(screenUser);
            return screenUser;
        } else {
            return null;
        }
    }

    @Override
    public ScreenUser insideAddApiAccount(ScreenUserDto screenUserDto, String userId) {
        ScreenUser screenUser = ModelMapperUtil.strictMap(screenUserDto, ScreenUser.class);
        scheduleService.getLoginUserDispose(screenUser);
        return screenUser;
    }
}
