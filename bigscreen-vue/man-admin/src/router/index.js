import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    base: '/man_admin/',
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: '/onlineManagement'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/onlineManagement',
                    component: () => import(/* webpackChunkName: "onlineManagement" */ '../components/page/OnlineManagement.vue'),
                    meta: { title: '工在线用户管理' }
                },{
                    path: '/userManagement',
                    component: () => import(/* webpackChunkName: "userManagement" */ '../components/page/UserManagement.vue'),
                    meta: { title: '全部用户管理' }
                },{
                    path: '/feedback',
                    component: () => import(/* webpackChunkName: "feedback" */ '../components/page/Feedback.vue'),
                    meta: { title: '建议与反馈' }
                },
                {
                    path: '/personal',
                    component: () => import(/* webpackChunkName: "personal" */ '../components/page/Personal.vue'),
                    meta: { title: '个人中心' }
                },
                
            ]
        },
        {
            path: '/login',
            component: () => import(/* webpackChunkName: "login" */ '../components/page/Login.vue'),
            meta: { title: '登录' }
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
