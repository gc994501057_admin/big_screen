package com.service.screenReception.controller;

import com.service.config.annotation.LoginUser;
import com.service.config.utils.*;
import com.service.screenReception.dto.ScreenUserDto;
import com.service.screenReception.dto.SiteDto;
import com.service.screenReception.entity.ScreenUser;
import com.service.screenReception.service.ScreenUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static com.service.config.constant.Constant.*;

/**
 * @author
 * @version 1.0
 * @date 2021/4/2 17:16
 */
@CrossOrigin
@Api(value = "大屏用户配置", description = "用户配置保存")
@RestController
@RequestMapping("/configApiAccount")
public class ConfigApiAccountController {


    @Resource
    private RedisUtil redisUtil;
    @Autowired
    private ScreenUserService screenUserService;

    @PostMapping("addApiAccount")
    @ApiOperation(value = "配置api账号和密码")
    @ResponseBody
    public Result queryServiceList(@RequestHeader(value = "cloud-Auth-Token") @ApiParam("token") String cloudAuthToken,
                                   @RequestBody ScreenUserDto screenUserDto) {
        if (screenUserDto.getTenantName() == null || "".equals(screenUserDto.getTenantName())
                || "".equals(screenUserDto.getTenantPwd()) || screenUserDto.getTenantPwd() == null) {
            return Result.failure(10003, "用户名或密码为空");
        }
        if (cloudAuthToken != null && !cloudAuthToken.equals("")) {
            String userId = UserTokenManager.getUserId(cloudAuthToken);
            if (redisUtil.exist(cloudAuthToken + userId)) {
                ScreenUser screenUser = screenUserService.checkTenant(screenUserDto, userId);
                if (screenUser != null) {
                    return Result.ok();
                } else {
                    return Result.failure(CLOUD_CAMPUS_ACCOUNT_ERROR, "云管理api账号有误或已经存在，请检查");
                }
            } else {
                return Result.failure(USERNAME_Off_SITE, usernameOffSite);
            }
        }
        return Result.failure(USER_NOT_LOGIN, "用户未登录");
    }

    /**
     * fegin远程调用接口
     * @param screenUserDto
     * @param userId
     * @return
     */
    @PostMapping("insideAddApiAccount")
    @ApiOperation(value = "内部配置api账号和密码")
    @ResponseBody
    public Result insideAddApiAccount(@RequestBody ScreenUserDto screenUserDto,@RequestParam("userId") String userId) {
        ScreenUser screenUser = screenUserService.insideAddApiAccount(screenUserDto, userId);
        if (screenUser != null) {
            return Result.ok();
        } else {
            return Result.failure(CLOUD_CAMPUS_ACCOUNT_ERROR, "云管理api账号有误或已经存在，请检查");
        }
    }
}
