import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
import { messages } from './components/common/i18n';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
import './assets/css/icon.css';
import './assets/iconfont/iconfont.css';
import './components/common/directives';
import 'babel-polyfill';
import less from 'less'
Vue.use(less)
// 炫酷的粒子动效
import  VueParticles  from   'vue-particles';
Vue.use(VueParticles);
Vue.config.productionTip = false;
Vue.use(VueI18n);
Vue.use(ElementUI, {
    size: 'small'
});
import echarts from 'echarts'

import 'echarts/map/js/china.js';

import publicMess from './utils/publicMess.js'
import Vcomp from './components/common/index'
Vue.use(Vcomp)

const i18n = new VueI18n({
    locale: 'zh',
    messages
});



import axios from 'axios'
Vue.prototype.$axios = axios
axios.defaults.baseURL = '/flag' //地址替换

//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    
    document.title = `${to.meta.title} | 大屏`;
    var addrstr = location.href;
    var str = addrstr.substring(addrstr.lastIndexOf('?') + 1, addrstr.length); 
    var arr = str.split('&');
    var obj = new Object();
    for (var i = 0; i < arr.length; i++) {
        var tmp_arr = arr[i].split('=');
        obj[decodeURIComponent(tmp_arr[0])] = decodeURIComponent(tmp_arr[1]);
    }
    if(obj.token){localStorage.setItem('cloud-Auth-Token', obj.token);}

    const role = localStorage.getItem('cloud-Auth-Token');

    if (!role ) {
       window.location.href = publicMess.url + "/bs_login/index.html";

    } else  {
        next()
    } 
});

new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app');
