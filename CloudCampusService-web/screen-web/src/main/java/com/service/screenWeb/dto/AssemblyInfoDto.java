package com.service.screenWeb.dto;

import java.io.Serializable;

/**
 * 组件信息表(AssemblyInfo)实体类
 *
 * @author makejava
 * @since 2021-05-18 19:56:26
 */
public class AssemblyInfoDto implements Serializable {
    private static final long serialVersionUID = 381543463989349998L;
    /**
     * id
     */
    private String id;
    /**
     * 组件名称
     */
    private String assemblyName;
    /**
     * 序号
     */
    private Integer number;
    /**
     * vue路由组件
     */
    private String component;
    /**
     * 组件引用的图片地址
     */
    private String assemblyImg;
    /**
     * 组件类型 0--内置组件 1--第三方组件
     */
    private String type;
    /**
     * 组件描述
     */
    private Double assemblyX;
    /**
     * 组件标题
     */
    private Double assemblyY;
    /**
     * 宽
     */
    private Double width;
    /**
     * 高
     */
    private Double height;
    private String authUrl;
    private String apiUrl;
    private String authUsername;
    private String authPassword;
    private String userId;
    /**
     * 方案表id
     */
    private String programmeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public void setAssemblyName(String assemblyName) {
        this.assemblyName = assemblyName;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getAssemblyImg() {
        return assemblyImg;
    }

    public void setAssemblyImg(String assemblyImg) {
        this.assemblyImg = assemblyImg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getAssemblyX() {
        return assemblyX;
    }

    public void setAssemblyX(Double assemblyX) {
        this.assemblyX = assemblyX;
    }

    public Double getAssemblyY() {
        return assemblyY;
    }

    public void setAssemblyY(Double assemblyY) {
        this.assemblyY = assemblyY;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getAuthUsername() {
        return authUsername;
    }

    public void setAuthUsername(String authUsername) {
        this.authUsername = authUsername;
    }

    public String getAuthPassword() {
        return authPassword;
    }

    public void setAuthPassword(String authPassword) {
        this.authPassword = authPassword;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProgrammeId() {
        return programmeId;
    }

    public void setProgrammeId(String programmeId) {
        this.programmeId = programmeId;
    }
}
