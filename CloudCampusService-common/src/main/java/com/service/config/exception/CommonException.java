package com.service.config.exception;

/**
 * @author
 * @version 1.0
 * @date 2021/3/29 11:41
 */
public class CommonException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    protected final String message;

    public CommonException(String message) {
        this.message = message;
    }

    public CommonException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
